module.exports = (err, req, res, next) => {
  let status = err.status || "500";
  console.log(typeof status, 'status')
  let message =  {message: err.message} || { message: "Internal Server Error" }
  console.log(err)
  res.status(status).json(message);
}