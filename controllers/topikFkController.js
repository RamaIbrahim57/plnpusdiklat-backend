const TopikFK = require('../models/topik_fk');
const { redis } = require('../helpers/redis');

module.exports = {
    // Read
  findOne: async (req, res, next) => {
    try {
      const foundRedis = await redis.get(req.params.id)
      if (!foundRedis) {
        let found = await TopikFK.findOne({_id: req.params.id})
        if (!found) {
          throw {status: 404, message: 'Data Topik FK Tidak Ditemukan'}
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findByAngkatan: async (req, res, next) => {
    try {
      let found = await TopikFK.find({angkatan_id: req.params.id})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newTopikFK = await TopikFK.create(req.body)
      await redis.set(newTopikFK._id, JSON.stringify(newTopikFK))
      const redisFound = await redis.get("topik_fk")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newTopikFK)
        await redis.set('topik_fk', JSON.stringify(parsed))
      }
      res.status(201).json(newTopikFK)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { topik_name, topik_tanggal, topik_deskripsi } = req.body
      let newTopikFK = await TopikFK.findOneAndUpdate({ _id: id }, { topik_name, topik_tanggal, topik_deskripsi }, { rawResult: true, new: true })
      if(newTopikFK.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      await redis.set(id, JSON.stringify(newTopikFK.value))
      res.status(200).json(newTopikFK)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await TopikFK.findOneAndDelete({ _id: id }, { rawResult: true })
      if(target.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      const foundRedis = await redis.get(id)
      if (foundRedis) {
        await redis.del(id)
      }
      await redis.del('topik_fk')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
}
