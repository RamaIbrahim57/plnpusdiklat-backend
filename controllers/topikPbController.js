const TopikPB = require('../models/topik_pb');
const { redis } = require('../helpers/redis');

module.exports = {
    // Read
  findOne: async (req, res, next) => {
    try {
      const foundRedis = await redis.get(req.params.id)
      if (!foundRedis) {
        let found = await TopikPB.findOne({_id: req.params.id})
        if (!found) {
          throw {status: 404, message: 'Data Topik PB Tidak Ditemukan'}
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findByAngkatan: async (req, res, next) => {
    try {
      let found = await TopikPB.find({angkatan_id: req.params.id})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByUpdl: async (req, res, next) => {
    try {
      let found = await TopikPB.find({ updl_id: req.params.id, angkatan_id: req.params.angkatan })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByBidang: async (req, res, next) => {
    try {
      let found = await TopikPB.find({ bidang_id: req.params.id})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByAngkatanAndBidang: async (req, res, next) => {
    try {
      let found = await TopikPB.find({ angkatan_id: req.params.angkatan, bidang_id: req.params.bidang})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newTopikPB = await TopikPB.create(req.body)
      await redis.set(newTopikPB._id, JSON.stringify(newTopikPB))
      const redisFound = await redis.get("topik_pb")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newTopikPB)
        await redis.set('topik_pb', JSON.stringify(parsed))
      }
      res.status(201).json(newTopikPB)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { topik_name, topik_tanggal, topik_deskripsi } = req.body
      let newTopikPB = await TopikPB.findOneAndUpdate({ _id: id }, { topik_name, topik_tanggal, topik_deskripsi }, { rawResult: true, new: true })
      if(newTopikPB.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      await redis.set(id, JSON.stringify(newTopikPB.value))
      res.status(200).json(newTopikPB)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await TopikPB.findOneAndDelete({ _id: id }, { rawResult: true })
      if(target.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      const foundRedis = await redis.get(id)
      if (foundRedis) {
        await redis.del(id)
      }
      await redis.del('topik_pb')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
}
