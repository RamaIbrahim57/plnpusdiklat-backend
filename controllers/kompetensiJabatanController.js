const { redis } = require('../helpers/redis');
const KompetensiJabatan = require('../models/kompetensi_jabatan');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('kompetensi_jabatan')
      if (!redisFound) {
        let found = await KompetensiJabatan.find()
        await redis.set('kompetensi_jabatan', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await KompetensiJabatan.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByJabatan: async (req, res, next) => {
    try {
      const { id } = req.params
      let found = await KompetensiJabatan.find({ jabatan_id: id })
      await redis.set('kompetensi_jabatan', JSON.stringify(found))
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newKompetensiJabatan = await KompetensiJabatan.create(req.body)
      await redis.set(newKompetensiJabatan._id, JSON.stringify(newKompetensiJabatan))
      const redisFound = await redis.get("kompetensi_jabatan")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newKompetensiJabatan)
        await redis.set('kompetensi_jabatan', JSON.stringify(parsed))
      }
      res.status(201).json(newKompetensiJabatan)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await KompetensiJabatan.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('kompetensi_jabatan')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await KompetensiJabatan.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('kompetensi_jabatan')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
