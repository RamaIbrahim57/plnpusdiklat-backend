const { redis } = require('../helpers/redis');
const WawancaraPertanyaan = require('../models/wawancara_pertanyaan');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('wawancara_pertanyaan')
      if (!redisFound) {
        let found = await WawancaraPertanyaan.find()
        await redis.set('wawancara_pertanyaan', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await WawancaraPertanyaan.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByIndikator: async (req, res, next) => {
    try {
      let found = await WawancaraPertanyaan.find({ wawancara_id: req.params.id })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newWawancaraPertanyaan = await WawancaraPertanyaan.create(req.body)
      await redis.set(newWawancaraPertanyaan._id, JSON.stringify(newWawancaraPertanyaan))
      const redisFound = await redis.get("wawancara_pertanyaan")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newWawancaraPertanyaan)
        await redis.set('wawancara_pertanyaan', JSON.stringify(parsed))
      }
      res.status(201).json(newWawancaraPertanyaan)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await WawancaraPertanyaan.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('wawancara_pertanyaan')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await WawancaraPertanyaan.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('wawancara_pertanyaan')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
