const { redis } = require('../helpers/redis');
const Bidang = require('../models/bidang');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('bidang')
      if (!redisFound) {
        let found = await Bidang.find()
        await redis.set('bidang', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Bidang.findOne({_id: req.params.id})
        if (!found) {
          res.status(200).json({})
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newBidang = await Bidang.create(req.body)
      await redis.set(newBidang._id, JSON.stringify(newBidang))
      const redisFound = await redis.get("bidang")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newBidang)
        await redis.set('bidang', JSON.stringify(parsed))
      }
      res.status(201).json(newBidang)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { nama } = req.body
      let { id } = req.params
      let target = await Bidang.findOneAndUpdate({ _id: id }, { nama }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      // await redis.del(target._id)
      await redis.set(target.value._id, JSON.stringify(target.value))
      const redisFound = await redis.get("bidang")
      if (redisFound) {
        await redis.del('bidang')
      }
      // let found = await Bidang.findOne({_id: id})
      // await redis.set(found._id, JSON.stringify(found))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Bidang.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('bidang')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
