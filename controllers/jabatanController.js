const { redis } = require('../helpers/redis');
const Jabatan = require('../models/jabatan');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('jabatan')
      if (!redisFound) {
        let found = await Jabatan.find()
        await redis.set('jabatan', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findByOkupasi: async (req, res, next) => {
    try {
      let found = await Jabatan.find({ okupasi_id: req.params.id })
      if (!found) {
        throw { status: 404, message: 'Data not found' }
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByNamaAndOkupasi: async (req, res, next) => {
    try {
      let found = await Jabatan.findOne({ okupasi_id: req.body.okupasi, nama: req.body.nama })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Jabatan.findOne({_id: req.params.id})
        if (!found) {
          res.status(200).json({})
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newJabatan = await Jabatan.create(req.body)
      await redis.set(newJabatan._id, JSON.stringify(newJabatan))
      const redisFound = await redis.get("jabatan")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newJabatan)
        await redis.set('jabatan', JSON.stringify(parsed))
      }
      res.status(201).json(newJabatan)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Jabatan.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get("jabatan")
      if (redisFound) {
        await redis.del('jabatan')
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Jabatan.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
        await redis.del('jabatan')
        res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
