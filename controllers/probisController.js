const { redis } = require('../helpers/redis');
const Probis = require('../models/probis');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('probis')
      if (!redisFound) {
        let found = await Probis.find()
        await redis.set('probis', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Probis.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByMentor: async (req, res, next) => {
    try {
      let found = await Probis.find({ mentor_id: req.params.id })
      if (!found) {
        res.status(200).json(null)
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findBySiswa: async (req, res, next) => {
    try {
      let found = await Probis.find({ siswa_id: req.params.id })
      if (!found) {
        res.status(200).json(null)
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByHariAndSiswa: async (req, res, next) => {
    try {
      let found = await Probis.find({ siswa_id: req.params.siswa, hari_ojt: req.params.hari })
      console.log(found, "probis awawa")
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newProbis = await Probis.create(req.body)
      await redis.set(newProbis._id, JSON.stringify(newProbis))
      const redisFound = await redis.get("probis")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newProbis)
        await redis.set('probis', JSON.stringify(parsed))
      }
      res.status(201).json(newProbis)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Probis.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('probis')
      await redis.set(id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updateNilai: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Probis.findOneAndUpdate({ _id: id }, { status: req.body.status, feedback: req.body.feedback }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('probis')
      await redis.set(id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Probis.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('probis')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
