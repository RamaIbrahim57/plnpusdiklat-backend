const PbNilai = require('../models/pb_nilai');
const { redis } = require('../helpers/redis');

module.exports = {
    // Read
  findOne: async (req, res, next) => {
    try {
      const foundRedis = await redis.get(req.params.id)
      if (!foundRedis) {
        let found = await PbNilai.findOne({_id: req.params.id})
        if (!found) {
          throw { status: 404, message: 'Data Nilai PB Tidak Ditemukan' }
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findByTopikAndSiswa: async (req, res, next) => {
    try {
      let found = await PbNilai.findOne({ topik_pb: req.params.topik, siswa_id: req.params.siswa })
      if (!found) res.status(200).json({})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByTopikPb: async (req, res, next) => {
    try {
      let found = await PbNilai.find({ topik_pb: req.params.topik })
      if (!found) res.status(200).json({})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findAll: async (req, res, next) => {
    try {
      const foundRedis = await redis.get('pb_nilai')
      if (!foundRedis) {
        let found = await PbNilai.find()
        await redis.set('pb_nilai', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      // const { siswa_id } = req.body
      let newTopikPB = await PbNilai.create(req.body)
      await redis.set(newTopikPB._id, JSON.stringify(newTopikPB))
      const redisFound = await redis.get("pb_nilai")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newTopikPB)
        await redis.set('pb_nilai', JSON.stringify(parsed))
      }
      res.status(201).json(newTopikPB)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { nilai, superadmin_id } = req.body
      let newTopikPB = await PbNilai.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if(newTopikPB.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      await redis.set(newTopikPB.value._id, JSON.stringify(newTopikPB.value))
      await redis.del('pb_nilai')
      res.status(200).json(newTopikPB)
    } catch (err) {
      next(err)
    }
  },
  updateNilai: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { nilai, superadmin_id } = req.body
      let newTopikPB = await PbNilai.findOneAndUpdate({ _id: id }, { nilai, superadmin_id }, { rawResult: true, new: true })
      if(newTopikPB.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      await redis.set(newTopikPB.value._id, JSON.stringify(newTopikPB.value))
      await redis.del('pb_nilai')
      res.status(200).json(newTopikPB)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await PbNilai.findOneAndDelete({ _id: id }, { rawResult: true })
      if(target.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      const foundRedis = await redis.get(id)
      if (foundRedis) {
        await redis.del(id)
      }
      await redis.del('pb_nilai')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }
}
