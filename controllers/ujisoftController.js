const { redis } = require('../helpers/redis');
const Ujisoft = require('../models/ujisoft');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('ujisoft')
      if (!redisFound) {
        let found = await Ujisoft.find()
        await redis.set('ujisoft', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Ujisoft.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByBidang: async (req, res, next) => {
    try {
      let found = await Ujisoft.find({ bidang_id: req.params.id })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newUjisoft = await Ujisoft.create(req.body)
      await redis.set(newUjisoft._id, JSON.stringify(newUjisoft))
      const redisFound = await redis.get("ujisoft")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newUjisoft)
        await redis.set('ujisoft', JSON.stringify(parsed))
      }
      res.status(201).json(newUjisoft)
    } catch (err) {
      next(err)
    }
  },
  updateQuestion: async (req, res, next) => {
    try {
      let { id } = req.params
      let { bidang, jawaban } = req.body
      let target = await Ujisoft.findOneAndUpdate({ _id: id }, { ujisoft: { bidang, jawaban } }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujisoft')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Ujisoft.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujisoft')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Ujisoft.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('ujisoft')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
