const { redis } = require('../helpers/redis');
const UjisoftEviden = require('../models/ujisoft_eviden');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('ujisoft_eviden')
      if (!redisFound) {
        let found = await UjisoftEviden.find()
        await redis.set('ujisoft_eviden', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await UjisoftEviden.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findBySiswa: async (req, res, next) => {
   try {
     let found = await UjisoftEviden.findOne({siswa_id: req.params.id})
     if (!found) {
        res.status(200).json(null)
     }
     res.status(200).json(found)
   } catch (err) {
     next(err)
   }
 },
  create: async (req, res, next) => {
    try {
      let newUjisoftEviden = await UjisoftEviden.create(req.body)
      await redis.set(newUjisoftEviden._id, JSON.stringify(newUjisoftEviden))
      const redisFound = await redis.get("ujisoft_eviden")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newUjisoftEviden)
        await redis.set('ujisoft_eviden', JSON.stringify(parsed))
      }
      res.status(201).json(newUjisoftEviden)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await UjisoftEviden.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujisoft_eviden')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await UjisoftEviden.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('ujisoft_eviden')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
