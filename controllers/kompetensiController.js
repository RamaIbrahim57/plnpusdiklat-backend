const { redis } = require('../helpers/redis');
const Kompetensi = require('../models/kompetensi');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const page = req.query.page
      const limit = req.query.limit
      const startIndex = (page - 1) * limit
      const endIndex = page * limit
      const redisFound = await redis.get('kompetensi')
      if (!redisFound) {
        let found = await Kompetensi.find()
        await redis.set('kompetensi', JSON.stringify(found))
        found = page && limit ? found.slice(startIndex, endIndex) : found
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        found = page && limit ? found.slice(startIndex, endIndex) : found
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  countData: async (req, res, next) => {
    try {
      Kompetensi.countDocuments({}, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  findByTipe: async (req, res, next) => {
    try {
      const redisFound = await redis.get('kompetensi')
      let found = await Kompetensi.find({ tipe: req.params.tipe })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByKodeUnit: async (req, res, next) => {
    try {
      let found = await Kompetensi.findOne({kode_unit: req.params.id})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findAllByKodeUnit: async (req, res, next) => {
    try {
      let found = await Kompetensi.find({kode_unit: { "$regex": req.params.id, "$options": "i" }})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Kompetensi.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newKompetensi = await Kompetensi.create(req.body)
      await redis.set(newKompetensi._id, JSON.stringify(newKompetensi))
      const redisFound = await redis.get("kompetensi")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newKompetensi)
        await redis.set('kompetensi', JSON.stringify(parsed))
      }
      res.status(201).json(newKompetensi)
    } catch (err) {
      next(err)
    }
  },
  findWithoutJabatan: async (req, res, next) => {
    try {
      const redisFound = await redis.get('kompetensi')
      if (!redisFound) {
        let found = await Kompetensi.find({ reserved: false })
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Kompetensi.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('kompetensi')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Kompetensi.findById(id, async function (err, client) {
        if (err) throw { status: 404, message: 'Data Not Found' }
        client.remove()
        const redisFound = await redis.get(id)
        if (redisFound) {
          await redis.del(id)
        }
        await redis.del('kompetensi')
        res.status(200).json(target)
      })
    } catch (err) {
      next(err)
    }
  }

}
