const { redis } = require('../helpers/redis');
const PengajuanGantiMentor = require('../models/pengajuan_ganti_mentor');
const axios = require('axios')

module.exports = {
    // Read
  getCityByProvinces: async (req, res, next) => {
    try {
      const resOngkir = await axios(`http://pro.rajaongkir.com/api/city?province=${req.params.id}`, {
        headers: {
          key: 'bf249ec2271b52cedbcc0a45b3a78acc',
        },
      })
      if (!resOngkir) throw { status: 500, message: 'Internal Server Error' }
      res.status(200).json(resOngkir.data.rajaongkir.results)
    } catch (err) {
      next(err)
    }
  },
  getAllProvinces: async (req, res, next) => {
    try {
      const resOngkir = await axios(`http://pro.rajaongkir.com/api/province`, {
        headers: {
          key: 'bf249ec2271b52cedbcc0a45b3a78acc',
        },
      })
      if (!resOngkir) throw { status: 500, message: 'Internal Server Error' }
      res.status(200).json(resOngkir.data.rajaongkir.results)
    } catch (err) {
      next(err)
    }
  },
}
