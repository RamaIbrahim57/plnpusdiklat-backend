const FkNilai = require('../models/fk_nilai');
const { redis } = require('../helpers/redis');

module.exports = {
    // Read
  findOne: async (req, res, next) => {
    try {
      const foundRedis = await redis.get(req.params.id)
      if (!foundRedis) {
        let found = await FkNilai.findOne({_id: req.params.id})
        if (!found) {
          throw { status: 404, message: 'Data Nilai FK Tidak Ditemukan' }
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findAll: async (req, res, next) => {
    try {
      const foundRedis = await redis.get('fk_nilai')
      if (!foundRedis) {
        let found = await FkNilai.find()
        if (!found) {
          throw {status: 404, message: 'Data Nilai FK Tidak Ditemukan'}
        }
        await redis.set('fk_nilai', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  // findByAngkatan: async (req, res, next) => {
  //   try {
  //     let found = await FkNilai.find({angkatan_id: req.params.id})
  //     res.status(200).json(found)
  //   } catch (err) {
  //     next(err)
  //   }
  // },
  create: async (req, res, next) => {
    try {
      // const { siswa_id } = req.body
      let newTopikFK = await FkNilai.create(req.body)
      await redis.set(newTopikFK._id, JSON.stringify(newTopikFK))
      const redisFound = await redis.get("fk_nilai")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newTopikFK)
        await redis.set('fk_nilai', JSON.stringify(parsed))
      }
      res.status(201).json(newTopikFK)
    } catch (err) {
      next(err)
    }
  },
  updateNilai: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { nilai, superadmin_id } = req.body
      let newFkNilai = await FkNilai.findOneAndUpdate({ _id: id }, { nilai, superadmin_id }, { rawResult: true, new: true })
      if(newFkNilai.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      await redis.set(newFkNilai.value._id,  JSON.stringify(newFkNilai.value))
      await redis.del('fk_nilai')
      res.status(200).json(newFkNilai)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { nilai, superadmin_id } = req.body
      let newFkNilai = await FkNilai.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if(newFkNilai.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      await redis.set(newFkNilai.value._id, JSON.stringify(newFkNilai.value))
      await redis.del('fk_nilai')
      res.status(200).json(newFkNilai)
    } catch (err) {
      next(err)
    }
  },
  findByTopikFkAndSiswa: async (req, res, next) => {
    try {
      let found = await FkNilai.findOne({ topik_fk: req.params.topik, siswa_id: req.params.siswa })
      if (!found) res.status(200).json({})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByTopikFk: async (req, res, next) => {
    try {
      let found = await FkNilai.find({ topik_fk: req.params.topik })
      if (!found) res.status(200).json({})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await FkNilai.findOneAndDelete({ _id: id }, { rawResult: true })
      if(target.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      const foundRedis = await redis.get(id)
      if (foundRedis) {
        await redis.del(id)
      }
      await redis.del('fk_nilai')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }
}
