const { redis } = require('../helpers/redis');
const KompetensiSiswa = require('../models/kompetensi_siswa');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('kompetensi_siswa')
      if (!redisFound) {
        let found = await KompetensiSiswa.find()
        await redis.set('kompetensi_siswa', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await KompetensiSiswa.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findBySiswa: async (req, res, next) => {
    try {
      const { id } = req.params
      let found = await KompetensiSiswa.find({ siswa_id: id })
      await redis.set('kompetensi_siswa', JSON.stringify(found))
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newKompetensiSiswa = await KompetensiSiswa.create(req.body)
      await redis.set(newKompetensiSiswa._id, JSON.stringify(newKompetensiSiswa))
      const redisFound = await redis.get("kompetensi_siswa")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newKompetensiSiswa)
        await redis.set('kompetensi_siswa', JSON.stringify(parsed))
      }
      res.status(201).json(newKompetensiSiswa)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await KompetensiSiswa.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('kompetensi_siswa')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await KompetensiSiswa.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('kompetensi_siswa')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  deleteBySiswa: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await KompetensiSiswa.remove({ siswa_id: id }).exec();
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('kompetensi_siswa')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }
}
