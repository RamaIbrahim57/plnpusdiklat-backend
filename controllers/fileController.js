const File = require('../models/file');
const { compresspdfupload } = require('../helpers/multer_compression');
const archiver = require('archiver');
var fs = require('fs');
const path = require("path");

module.exports = {
  upload: async (req, res, next) => {
    try {
      const file = req.file
      console.log(req.body)
      if (!file) {
        throw { status: 400, message: 'Please upload a file' }
      }
      const newFile = await File.create({ type: file.mimetype , path: file.path })
      res.status(201).json(newFile)
    } catch (err) {
      next (err)
    }
  },
  uploadPdf: async (req, res, next) => {
    try {
      const file = req.file
      if (!file) {
        throw { status: 400, message: 'Please upload a file' }
      }
      if (file.mimetype !== 'application/pdf') {
        throw { status: 400, message: 'Please upload PDF file only' }
      }
      const newFile = await File.create({ type: file.mimetype , path: file.path })
      res.status(201).json(newFile)
    } catch (err) {
      next (err)
    }
  },
  // uploadPdf: async (req, res, next) => {
  //   compresspdfupload(req, res, function (err) {
  //     if (err) {
  //       return res.end("Error uploading file")
  //     }
  //   })
  // },
  compressPdf: async (req, res, next) => {
    const inputFile = req.body.path
    const outputFilePath = Date.now() + "output" + path.extname(req.body.path);
    exec(
      `gs \ -q -dNOPAUSE -dBATCH -dSAFER \ -sDEVICE=pdfwrite \ -dCompatibilityLevel=1.3 \ -dPDFSETTINGS=/ebook \ -dEmbedAllFonts=true \ -dSubsetFonts=true \ -dAutoRotatePages=/None \ -dColorImageDownsampleType=/Bicubic \ -dColorImageResolution=72 \ -dGrayImageDownsampleType=/Bicubic \ -dGrayImageResolution=72 \ -dMonoImageDownsampleType=/Subsample \ -dMonoImageResolution=72 \ -sOutputFile=${outputFilePath} \ ${inputFile}`,
      (err, stdout, stderr) => {
        if (err) {
          res.json({
            error: "some error takes place",
          });
        }
        res.json({
          path: outputFilePath,
        });
      }
    );
  },
  uploadPpt: async (req, res, next) => {
    try {
      const file = req.file
      if (!file) {
        throw { status: 400, message: 'Please upload a file' }
      }
      if (file.mimetype !== 'application/vnd.ms-powerpoint' && file.mimetype !== 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
        console.log(file.mimetype, 'mimetype')
        throw { status: 400, message: 'Please upload PPT file only' }
      }
      const newFile = await File.create({ type: file.mimetype , path: file.path })
      res.status(201).json(newFile)
    } catch (err) {
      next (err)
    }
  },
  download: async (req, res, next) => {
    try {
      const files = await File.findOne({ _id: req.params.id })
      if (!files) {
        throw { status: 404, message: 'File not found' }
      }
      console.log(files.path.substring(6))
      res.download(files.path)
    } catch (err) {
      next (err)
    }
  },
  downloadTemplateKuk: async (req, res, next) => {
    try {
      res.download('files//Template KUK.xlsx')
    } catch (err) {
      next (err)
    }
  },
  downloadTemplateSurvey: async (req, res, next) => {
    try {
      res.download('files//Template Survey.xlsx')
    } catch (err) {
      next (err)
    }
  },
  downloadTemplateCreateSurvey: async (req, res, next) => {
    try {
      res.download('files//Template Add Survey.xlsx')
    } catch (err) {
      next (err)
    }
  },
  downloadTemplateUpdl: async (req, res, next) => {
    try {
      res.download('files//Template UPDL Siswa.xlsx')
    } catch (err) {
      next (err)
    }
  },
  downloadTemplateMasterDataSiswa: async (req, res, next) => {
    try {
      res.download('files//Template Data Siswa.xlsx')
    } catch (err) {
      next (err)
    }
  },
  downloadTemplateNilaiSiswa: async (req, res, next) => {
    try {
      res.download('files//Template Nilai Siswa.xlsx')
    } catch (err) {
      next (err)
    }
  },
  downloadReferensiKUK: async (req, res, next) => {
    try {
      res.download('files//FORMAT KUK 5. PEDOMAN SKTTK PENGOPERASIAN DISTRIBUSI (3).xlsx')
    } catch (err) {
      next (err)
    }
  },
  downloadTemplateLaporan: async (req, res, next) => {
    try {
      res.download('files//00. Laporan harian.docx')
    } catch (err) {
      next (err)
    }
  },
  downloadTemplateOkupasi: async (req, res, next) => {
    try {
      res.download('files//Template Okupasi.xlsx')
    } catch (err) {
      next (err)
    }
  },
  compressToZip: async (req, res, next) => {
    try {
      const { data, filename } = req.body
      var archive = archiver('zip', {
          gzip: true,
          zlib: { level: 9 } // Sets the compression level.
      });
      
      archive.on('error', function(err) {
          throw err;
      });
      const outputDestination = `./files/${filename}.zip`
      const output = fs.createWriteStream(outputDestination);
      archive.pipe(output);
      for (const i in data) {
        const file = await File.findOne({ _id: data[i]._id })
        if (file) {
          archive.file(file.path, {name: file.path.substring(6)});
        }
      }
      archive.finalize()
      const newFile = await File.create({ type: 'application/zip', path: outputDestination })
      // res.download(outputDestination)
      res.status(200).json(newFile._id)
    } catch (err) {
      next(err)
    }
  },
  deleteFile: async (req, res, next) => {
    try {
      const file = await File.findOneAndDelete({ _id: req.params.id })
      res.status(200).json(file)
    } catch (err) {
      next (err)
    }
  },
  downloadByPath: async (req, res, next) => {
    try {
      res.download(req.params.filepath)
    } catch (err) {
      next (err)
    }
  },
}