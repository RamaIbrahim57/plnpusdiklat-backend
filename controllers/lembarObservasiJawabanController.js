const { redis } = require('../helpers/redis');
const LembarObservasiJawaban = require('../models/lembar_observasi_jawaban');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('lembar_observasi_jawaban')
      if (!redisFound) {
        let found = await LembarObservasiJawaban.find()
        await redis.set('lembar_observasi_jawaban', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await LembarObservasiJawaban.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByMentorAndSiswa: async (req, res, next) => {
    try {
      let found = await LembarObservasiJawaban.find({ mentor_id: req.params.mentor, siswa_id: req.params.siswa })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newLembarObservasi = await LembarObservasiJawaban.create(req.body)
      await redis.set(newLembarObservasi._id, JSON.stringify(newLembarObservasi))
      const redisFound = await redis.get("lembar_observasi_jawaban")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newLembarObservasi)
        await redis.set('lembar_observasi_jawaban', JSON.stringify(parsed))
      }
      res.status(201).json(newLembarObservasi)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await LembarObservasiJawaban.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await LembarObservasiJawaban.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('lembar_observasi_jawaban')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
