const { redis } = require('../helpers/redis');
const WorkplanStatus = require('../models/workplan_status');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('workplan_status')
      if (!redisFound) {
        let found = await WorkplanStatus.find()
        await redis.set('workplan_status', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await WorkplanStatus.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newWorkplanStatus = await WorkplanStatus.create(req.body)
      await redis.set(newWorkplanStatus._id, JSON.stringify(newWorkplanStatus))
      res.status(201).json(newWorkplanStatus)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await WorkplanStatus.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await WorkplanStatus.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('workplan_status')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
