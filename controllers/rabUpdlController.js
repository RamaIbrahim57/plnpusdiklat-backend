const { redis } = require('../helpers/redis');
const RabUpdl = require('../models/rab_updl');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('rab_updl')
      if (!redisFound) {
        let found = await RabUpdl.find()
        await redis.set('rab_updl', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await RabUpdl.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newRabUpdl = await RabUpdl.create(req.body)
      await redis.set(newRabUpdl._id, JSON.stringify(newRabUpdl))
      const redisFound = await redis.get("rab_updl")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newRabUpdl)
        await redis.set('rab_updl', JSON.stringify(parsed))
      }
      res.status(201).json(newRabUpdl)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { nama } = req.body
      let { id } = req.params
      let target = await RabUpdl.findOneAndUpdate({ _id: id }, { nama }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      // await redis.del(target._id)
      await redis.set(target.value._id, JSON.stringify(target.value))
      const redisFound = await redis.get("rab_updl")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(target.value)
        await redis.set('rab_updl', JSON.stringify(parsed))
      }
      // await redis.set(found._id, JSON.stringify(found))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await RabUpdl.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('rab_updl')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
