const { redis } = require('../helpers/redis');
const Workplan = require('../models/workplan');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('workplan')
      if (!redisFound) {
        let found = await Workplan.find()
        await redis.set('workplan', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get('workplan' + req.params.id)
      if (!redisFound) {
        let found = await Workplan.findOne({_id: req.params.id})
        if (found) {
          await redis.set('workplan' + req.params.id, JSON.stringify(found))
          res.status(200).json(found)
        } else {
          res.status(200).json(null)
        }
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByAktifitas: async (req, res, next) => {
    try {
      let found = await Workplan.findOne({aktifitas_id: req.params.id, siswa_id: req.params.siswa})
      if (!found) {
        res.status(200).json(null)
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByMentor: async (req, res, next) => {
    try {
      let found = await Workplan.find({ mentor_id: req.params.id })
      if (!found) {
        res.status(200).json(null)
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findBySiswa: async (req, res, next) => {
    try {
      let found = await Workplan.find({ siswa_id: req.params.id })
      if (!found) {
        res.status(200).json({})
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newWorkplan = await Workplan.create(req.body)
      await redis.set(newWorkplan._id, JSON.stringify(newWorkplan))
      const redisFound = await redis.get("workplan")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newWorkplan)
        await redis.set('workplan', JSON.stringify(parsed))
      }
      res.status(201).json(newWorkplan)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Workplan.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set('workplan' + target.value._id, JSON.stringify(target.value))
      await redis.del('workplan')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updateStatusWorkplan: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Workplan.findOneAndUpdate({ _id: id }, { feedback_workplan: req.body.feedback_workplan, status_workplan: req.body.status_workplan }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set('workplan' + target.value._id, JSON.stringify(target.value))
      await redis.del('workplan')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updateStatusRealisasi: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Workplan.findOneAndUpdate({ _id: id }, { feedback_realisasi: req.body.feedback_realisasi, status_realisasi: req.body.status_realisasi }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set('workplan' + target.value._id, JSON.stringify(target.value))
      await redis.del('workplan')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updateRealisasi: async (req, res, next) => {
    try {
      let { id } = req.params
      const realisasi = {
        eviden: req.body.eviden,
        keterangan: req.body.keterangan,
      }
      let target = await Workplan.findOneAndUpdate({ _id: id }, { realisasi }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set('workplan' + target.value._id, JSON.stringify(target.value))
      await redis.del('workplan')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Workplan.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get('workplan' + id)
      if (redisFound) {
        await redis.del('workplan' + id)
      }
      await redis.del('workplan')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  deleteBySiswa: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Workplan.remove({ siswa_id: id }).exec();
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('workplan')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
}
