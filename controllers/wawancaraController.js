const { redis } = require('../helpers/redis');
const Wawancara = require('../models/wawancara');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('wawancara')
      if (!redisFound) {
        let found = await Wawancara.find()
        await redis.set('wawancara', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Wawancara.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByBidang: async (req, res, next) => {
    try {
      let found = await Wawancara.find({ bidang_id: req.params.id })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newWawancara = await Wawancara.create(req.body)
      await redis.set(newWawancara._id, JSON.stringify(newWawancara))
      const redisFound = await redis.get("wawancara")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newWawancara)
        await redis.set('wawancara', JSON.stringify(parsed))
      }
      res.status(201).json(newWawancara)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Wawancara.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('wawancara')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Wawancara.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('wawancara')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
