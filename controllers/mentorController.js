const { redis } = require('../helpers/redis');
const Mentor = require('../models/mentor');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('mentor')
      if (!redisFound) {
        let found = await Mentor.find()
        await redis.set('mentor', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findByKompetensi: async (req, res, next) => {
    try {
      let found = await Mentor.find({ kompetensi_id: req.params.id })
      if (!found) {
        throw { status: 404, message: 'Data not found' }
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByProgram: async (req, res, next) => {
    try {
      const found = await Mentor.find({ program: req.params.program })
      if (!found) {
        res.status(200).json([])
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Mentor.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newMentor = await Mentor.create(req.body)
      await redis.set(newMentor._id, JSON.stringify(newMentor))
      const redisFound = await redis.get("mentor")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newMentor)
        await redis.set('mentor', JSON.stringify(parsed))
      }
      res.status(201).json(newMentor)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Mentor.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('mentor')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Mentor.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('mentor')
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
