const { redis } = require('../helpers/redis');
const SurveyJawaban = require('../models/survey_jawaban');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('survey_jawaban')
      if (!redisFound) {
        let found = await SurveyJawaban.find()
        await redis.set('survey_jawaban', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findBySiswa: async (req, res, next) => {
    try {
      let found = await SurveyJawaban.find({ siswa_id: req.params.id, subject: 'Siswa' })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findBySurvey: async (req, res, next) => {
    try {
      let found = await SurveyJawaban.find({ survey_id: req.params.id })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  deleteBySurvey: async (req, res, next) => {
    try {
      let found = await SurveyJawaban.deleteMany({ survey_id: req.params.id })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  deleteBySiswa: async (req, res, next) => {
    try {
      let found = await SurveyJawaban.deleteMany({ siswa_id: req.params.id })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByMentor: async (req, res, next) => {
    try {
      let found = await SurveyJawaban.find({ mentor_id: req.params.id })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByMentorAndSiswa: async (req, res, next) => {
    try {
      let found = await SurveyJawaban.find({ mentor_id: req.params.mentor, siswa_id: req.params.siswa, subject: 'Mentor'  })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await SurveyJawaban.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newSurvey = await SurveyJawaban.create(req.body)
      await redis.set(newSurvey._id, JSON.stringify(newSurvey))
      const redisFound = await redis.get("survey_jawaban")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newSurvey)
        await redis.set('survey_jawaban', JSON.stringify(parsed))
      }
      res.status(201).json(newSurvey)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await SurveyJawaban.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      // await redis.del(target._id)
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('survey_jawaban')
      // await redis.set(found._id, JSON.stringify(found))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await SurveyJawaban.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('survey_jawaban')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }
}
