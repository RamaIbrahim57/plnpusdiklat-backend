const { redis } = require('../helpers/redis');
const Rab = require('../models/rab');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('rab')
      if (!redisFound) {
        let found = await Rab.find()
        await redis.set('rab', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findByAngkatanAndTahapan: async (req, res, next) => {
    try {
      const found = await Rab.find({ angkatan: req.params.angkatan, tahapan: req.params.tahapan })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByAngkatan: async (req, res, next) => {
    try {
      const found = await Rab.find({ angkatan: req.params.angkatan })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Rab.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newRab = await Rab.create(req.body)
      await redis.set(newRab._id, JSON.stringify(newRab))
      const redisFound = await redis.get("rab")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newRab)
        await redis.set('rab', JSON.stringify(parsed))
      }
      res.status(201).json(newRab)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { nama } = req.body
      let { id } = req.params
      let target = await Rab.findOneAndUpdate({ _id: id }, { nama }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('rab')
      await redis.set(target.value._id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Rab.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('rab')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
