const { redis } = require('../helpers/redis');
const Ujihard = require('../models/ujihard');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('ujihard')
      if (!redisFound) {
        let found = await Ujihard.find()
        await redis.set('ujihard', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Ujihard.findOne({_id: req.params.id})
        if (!found) {
          res.status(200).json(null)
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findBySiswa: async (req, res, next) => {
    try {
      let found = await Ujihard.findOne({ siswa_id: req.params.id })
      if (!found) {
        res.status(200).json(null)
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByPenguji: async (req, res, next) => {
    try {
      let found = await Ujihard.find({ penguji_id: req.params.id })
      if (!found) {
        res.status(200).json(null)
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newUjihard = await Ujihard.create(req.body)
      await redis.set(newUjihard._id, JSON.stringify(newUjihard))
      const redisFound = await redis.get("ujihard")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newUjihard)
        await redis.set('ujihard', JSON.stringify(parsed))
      }
      res.status(201).json(newUjihard)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Ujihard.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujihard')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updateStatus: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Ujihard.findOneAndUpdate({ _id: id }, { status: req.body.status, feedback: req.body.feedback }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujihard')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updateStatusByPenguji: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Ujihard.findOneAndUpdate({ _id: id }, { status: req.body.status, feedback: req.body.feedback }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujihard')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updateUPDL: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Ujihard.findOneAndUpdate({ _id: id },  {$unset: { penguji_status: 1 }, penguji_id: req.body.penguji_id, tanggal_uji: req.body.tanggal_uji }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujihard')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  tolakPenguji: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Ujihard.findOneAndUpdate({ _id: id }, { $unset: { penguji_id: 1, tanggal_uji: 1 }, penguji_status: 'ditolak' }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujihard')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  setujuiPenguji: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Ujihard.findOneAndUpdate({ _id: id }, { penguji_status: 'disetujui' }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujihard')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updatePenilaianSertifikasi: async (req, res, next) => {
    try {
      let { id } = req.params
      const data = {
        sertifikasi_keterangan: req.body.sertifikasi_keterangan,
        nilai_sertifikasi_kategori_1: req.body.nilai_sertifikasi_kategori_1,
        nilai_sertifikasi_kategori_2: req.body.nilai_sertifikasi_kategori_2,
        nilai_sertifikasi_kategori_3: req.body.nilai_sertifikasi_kategori_3,
        status_penilaian_sertifikasi: req.body.status_penilaian_sertifikasi,
      }
      let target = await Ujihard.findOneAndUpdate({ _id: id }, data, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujihard')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updatePenilaianNonSertifikasi: async (req, res, next) => {
    try {
      let { id } = req.params
      const data = {
        nonsertifikasi_keterangan: req.body.nonsertifikasi_keterangan,
        nilai_nonsertifikasi_kategori_1: req.body.nilai_nonsertifikasi_kategori_1,
        nilai_nonsertifikasi_kategori_2: req.body.nilai_nonsertifikasi_kategori_2,
        nilai_nonsertifikasi_kategori_3: req.body.nilai_nonsertifikasi_kategori_3,
        status_penilaian_nonsertifikasi: req.body.status_penilaian_nonsertifikasi,
      }
      let target = await Ujihard.findOneAndUpdate({ _id: id }, data, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujihard')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Ujihard.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('ujihard')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
