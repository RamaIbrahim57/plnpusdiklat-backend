const { redis } = require('../helpers/redis');
const WawancaraJawaban = require('../models/wawancara_jawaban');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('wawancara_jawaban')
      if (!redisFound) {
        let found = await WawancaraJawaban.find()
        await redis.set('wawancara_jawaban', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await WawancaraJawaban.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByPertanyaan: async (req, res, next) => {
    try {
      let found = await WawancaraJawaban.findOne({wawancara_pertanyaan: req.params.id})
      if (!found) {
         res.status(200).json({})
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByPsikolog: async (req, res, next) => {
   try {
     let found = await WawancaraJawaban.find({psikolog_id: req.params.id})
     if (!found) {
        res.status(200).json([])
     }
     res.status(200).json(found)
   } catch (err) {
     next(err)
   }
 },
  create: async (req, res, next) => {
    try {
      let newJawaban = await WawancaraJawaban.create(req.body)
      await redis.set(newJawaban._id, JSON.stringify(newJawaban))
      const redisFound = await redis.get("wawancara_jawaban")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newJawaban)
        await redis.set('wawancara_jawaban', JSON.stringify(parsed))
      }
      res.status(201).json(newJawaban)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await WawancaraJawaban.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('wawancara_jawaban')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await WawancaraJawaban.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('wawancara_jawaban')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
