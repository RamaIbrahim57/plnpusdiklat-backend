const { redis } = require('../helpers/redis');
const LaporanHarian = require('../models/laporan_harian');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('laporan_harian')
      if (!redisFound) {
        let found = await LaporanHarian.find()
        await redis.set('laporan_harian', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await LaporanHarian.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByMentor: async (req, res, next) => {
    try {
      let found = await LaporanHarian.find({ mentor_id: req.params.id })
      if (!found) {
        res.status(200).json(null)
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findBySiswa: async (req, res, next) => {
    try {
      let found = await LaporanHarian.find({ siswa_id: req.params.id })
      if (!found) {
        res.status(200).json(null)
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newLaporanHarian = await LaporanHarian.create(req.body)
      await redis.set(newLaporanHarian._id, JSON.stringify(newLaporanHarian))
      const redisFound = await redis.get("laporan_harian")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newLaporanHarian)
        await redis.set('laporan_harian', JSON.stringify(parsed))
      }
      res.status(201).json(newLaporanHarian)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { nama } = req.body
      let { id } = req.params
      let target = await LaporanHarian.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('laporan_harian')
      await redis.set(target.value._id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updateStatus: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await LaporanHarian.findOneAndUpdate({ _id: id }, { status: req.body.status }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('laporan_harian')
      await redis.set(target.value._id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updateStatusRevisi: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await LaporanHarian.findOneAndUpdate({ _id: id }, { status: req.body.status, feedback: req.body.feedback }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('laporan_harian')
      await redis.set(target.value._id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await LaporanHarian.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('laporan_harian')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
