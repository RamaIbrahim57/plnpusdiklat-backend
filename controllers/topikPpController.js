const TopikPP = require('../models/topik_pp');
const { redis } = require('../helpers/redis');

module.exports = {
    // Read
  findOne: async (req, res, next) => {
    try {
      const foundRedis = await redis.get(req.params.id)
      if (!foundRedis) {
        let found = await TopikPP.findOne({_id: req.params.id})
        if (!found) {
          throw {status: 404, message: 'Data Topik PP Tidak Ditemukan'}
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findByAngkatan: async (req, res, next) => {
    try {
      let found = await TopikPP.find({angkatan_id: req.params.id})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newTopikPP = await TopikPP.create(req.body)
      await redis.set(newTopikPP._id, JSON.stringify(newTopikPP))
      const redisFound = await redis.get("topik_pp")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newTopikPP)
        await redis.set('topik_pp', JSON.stringify(parsed))
      }
      res.status(201).json(newTopikPP)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { topik_name, topik_tanggal, topik_deskripsi } = req.body
      let newTopikPP = await TopikPP.findOneAndUpdate({ _id: req.params.id }, { topik_name, topik_tanggal, topik_deskripsi }, { rawResult: true, new: true })
      if(newTopikPP.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      await redis.set(id, JSON.stringify(newTopikPP.value))
      res.status(200).json(newTopikPP)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await TopikPP.findOneAndDelete({ _id: id }, { rawResult: true })
      if(target.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      const foundRedis = await redis.get(id)
      if (foundRedis) {
        await redis.del(id)
      }
      await redis.del('topik_pp')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
}
