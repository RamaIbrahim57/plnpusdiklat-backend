const { redis } = require('../helpers/redis');
const Aktifitas = require('../models/aktifitas');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('aktifitas')
      if (!redisFound) {
        let found = await Aktifitas.find()
        await redis.set('aktifitas', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findByKompetensi: async (req, res, next) => {
    try {
      const page = req.query.page
      const limit = req.query.limit
      const startIndex = (page - 1) * limit
      const endIndex = page * limit
      let found = await Aktifitas.find({ kompetensi_id: req.params.id })
      if (!found) {
        throw { status: 404, message: 'Data not found' }
      }
      found = page && limit && found ? found.slice(startIndex, endIndex) : found
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByNomorAndKompetensi: async (req, res, next) => {
    try {
      let found = await Aktifitas.find({ kodeunit_no: req.params.nomor, kompetensi_id: req.params.kompetensi })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get('aktifitas' + req.params.id)
      if (!redisFound) {
        let found = await Aktifitas.findOne({_id: req.params.id})
        if (found) {
          await redis.set('aktifitas' + req.params.id, JSON.stringify(found))
          res.status(200).json(found)
        } else {
          res.status(200).json(null)
        }
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newAktifitas = await Aktifitas.create(req.body)
      await redis.set(newAktifitas._id, JSON.stringify(newAktifitas))
      const redisFound = await redis.get("aktifitas")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newAktifitas)
        await redis.set('aktifitas', JSON.stringify(parsed))
      }
      res.status(201).json(newAktifitas)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Aktifitas.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('aktifitas')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Aktifitas.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('aktifitas')
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
