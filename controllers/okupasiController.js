const { redis } = require('../helpers/redis');
const Okupasi = require('../models/okupasi');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('okupasi')
      if (!redisFound) {
        let found = await Okupasi.find()
        await redis.set('okupasi', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Okupasi.findOne({_id: req.params.id})
        if (!found) {
          throw { status: 404, message: 'Data not found' }
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByBidang: async (req, res, next) => {
    try {
      let found = await Okupasi.find({ bidang: req.params.id })
      if (!found) {
        throw { status: 404, message: 'Data not found' }
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByNamaAndBidang: async (req, res, next) => {
    try {
      let found = await Okupasi.findOne({ bidang: req.body.bidang, nama: req.body.nama })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newOkupasi = await Okupasi.create(req.body)
      await redis.set(newOkupasi._id, JSON.stringify(newOkupasi))
      const redisFound = await redis.get("okupasi")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newOkupasi)
        await redis.set('okupasi', JSON.stringify(parsed))
      }
      res.status(201).json(newOkupasi)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { nama, superadmin_id } = req.body
      let { id } = req.params
      let target = await Okupasi.findOneAndUpdate({ _id: id }, { nama, superadmin_id }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      const redisFound = await redis.get("okupasi")
      if (redisFound) {
        await redis.del('okupasi')
      }
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Okupasi.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('okupasi')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
