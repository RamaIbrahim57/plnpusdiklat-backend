const { redis } = require('../helpers/redis');
const PengajuanGantiMentor = require('../models/pengajuan_ganti_mentor');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('pengajuan_ganti_mentor')
      if (!redisFound) {
        let found = await PengajuanGantiMentor.find()
        await redis.set('pengajuan_ganti_mentor', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await PengajuanGantiMentor.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newPengajuan = await PengajuanGantiMentor.create(req.body)
      await redis.set(newPengajuan._id, JSON.stringify(newPengajuan))
      const redisFound = await redis.get("pengajuan_ganti_mentor")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newPengajuan)
        await redis.set('pengajuan_ganti_mentor', JSON.stringify(parsed))
      }
      res.status(201).json(newPengajuan)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { nama } = req.body
      let { id } = req.params
      let target = await PengajuanGantiMentor.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      // await redis.del(target._id)
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del("pengajuan_ganti_mentor")
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await PengajuanGantiMentor.findById(id, async function (err, client) {
        if (err) throw { status: 404, message: 'Data Not Found' }
        client.remove()
        const redisFound = await redis.get(id)
        if (redisFound) {
          await redis.del(id)
        }
        await redis.del('pengajuan_ganti_mentor')
        res.status(200).json(target)
      })
    } catch (err) {
      next(err)
    }
  }

}
