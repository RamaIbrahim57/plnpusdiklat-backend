const PpNilai = require('../models/pp_nilai');
const { redis } = require('../helpers/redis');

module.exports = {
    // Read
  findOne: async (req, res, next) => {
    try {
      const foundRedis = await redis.get(req.params.id)
      if (!foundRedis) {
        let found = await PpNilai.findOne({_id: req.params.id})
        if (!found) {
          throw { status: 404, message: 'Data Nilai PP Tidak Ditemukan' }
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findByTopikPp: async (req, res, next) => {
    try {
      let found = await PpNilai.find({ topik_pp: req.params.topik })
      if (!found) res.status(200).json({})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findAll: async (req, res, next) => {
    try {
      const foundRedis = await redis.get('pp_nilai')
      if (!foundRedis) {
        let found = await PpNilai.find()
        if (!found) {
          throw {status: 404, message: 'Data Nilai PP Tidak Ditemukan'}
        }
        await redis.set('pp_nilai', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      // const { siswa_id } = req.body
      let newTopikPP = await PpNilai.create(req.body)
      await redis.set(newTopikPP._id, JSON.stringify(newTopikPP))
      const redisFound = await redis.get("pp_nilai")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newTopikPP)
        await redis.set('pp_nilai', JSON.stringify(parsed))
      }
      res.status(201).json(newTopikPP)
    } catch (err) {
      next(err)
    }
  },
  updateNilai: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { nilai, superadmin_id } = req.body
      let newTopikPP = await PpNilai.findOneAndUpdate({ _id: id }, { nilai, superadmin_id }, { rawResult: true, new: true })
      if(newTopikPP.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      await redis.set(newTopikPP.value._id, JSON.stringify(newTopikPP.value))
      await redis.del('pp_nilai')
      res.status(200).json(newTopikPP)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      const { id } = req.params;
      const { nilai, superadmin_id } = req.body
      let newTopikPP = await PpNilai.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if(newTopikPP.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      await redis.set(newTopikPP.value._id, JSON.stringify(newTopikPP.value))
      await redis.del('pp_nilai')
      res.status(200).json(newTopikPP)
    } catch (err) {
      next(err)
    }
  },
  findByTopikAndSiswa: async (req, res, next) => {
    try {
      let found = await PpNilai.findOne({ topik_pp: req.params.topik, siswa_id: req.params.siswa })
      if (!found) res.status(200).json({})
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await PpNilai.findOneAndDelete({ _id: id }, { rawResult: true })
      if(target.value === null) {
        throw { status: 404, message: "Data Nilai Not Found"}
      }
      const foundRedis = await redis.get(id)
      if (foundRedis) {
        await redis.del(id)
      }
      await redis.del('pp_nilai')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }
}
