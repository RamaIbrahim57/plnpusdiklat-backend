const { redis } = require('../helpers/redis');
const Siswa = require('../models/siswa');
const Mentor = require('../models/mentor2');

module.exports = {
  // Read
  findSiswaS1: async (req, res, next) => {
    try {
      let { program } = req.query
      let found = await Siswa.countDocuments({ jenjang: { "$regex": "S1", "$options": "i" }, program }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  findSiswaSLTA: async (req, res, next) => {
    try {
      let { program } = req.query
      let found = await Siswa.countDocuments({ jenjang: { "$regex": "SLTA", "$options": "i" }, program }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  findSiswaS2: async (req, res, next) => {
    try {
      let { program } = req.query
      let found = await Siswa.countDocuments({ jenjang: { "$regex": "S2", "$options": "i" }, program }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  findSiswaD3: async (req, res, next) => {
    try {
      let { program } = req.query
      let found = await Siswa.countDocuments({ jenjang: { "$regex": "D-III", "$options": "i" }, program }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  findSiswaD3K: async (req, res, next) => {
    try {
      let { program } = req.query
      let found = await Siswa.countDocuments({ jenjang:  { "$regex": "D3K", "$options": "i" }, program }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  findMentorByProgram: async (req, res, next) => {
    try {
      let { program } = req.query
      const found = await Mentor.countDocuments({ program }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  findSiswaByProgram: async (req, res, next) => {
    try {
      let { program } = req.query
      let found = await Siswa.countDocuments({ program }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  countSiswaDataByProgram: async (req, res, next) => {
    try {
      const { program } = req.params
      Siswa.countDocuments({ program }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  countSiswaLulusInPrajabatan: async (req, res, next) => {
    try {
      Siswa.countDocuments({
        program: 'prajabatan',
        uji_akhir: { $gt: 69 },
        fk: { $gt: 69 },
        pp: { $gt: 69 },
        pb: { $gt: 69 },
      }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  countSiswaTidakLulusInPrajabatan: async (req, res, next) => {
    try {
      Siswa.countDocuments({
        program: 'prajabatan',
        uji_akhir: { $lt: 70 },
        fk: { $lt: 70 },
        pp: { $lt: 70 },
        pb: { $lt: 70 },
      }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  countSiswaTidakLulusByProgram: async (req, res, next) => {
    try {
      const { program } = req.params
      Siswa.countDocuments({
        program,
        uji_akhir: { $lt: 70 },
        pp: { $lt: 70 },
        pb: { $lt: 70 },
      }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  countSiswaLulusByProgram: async (req, res, next) => {
    try {
      const { program } = req.params
      Siswa.countDocuments({
        program,
        uji_akhir: { $gt: 69 },
        pp: { $gt: 69 },
        pb: { $gt: 69 },
      }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  // findSiswaGraduated: async (req, res, next) => {
  //   try {
  //     let { program } = req.query
  //     let found = await Siswa.countDocuments({ fk: }, function (err, count) {
  //       res.status(200).json({ length: count })
  //     });
  //   } catch (err) {
  //     next(err)
  //   }
  // },
}
