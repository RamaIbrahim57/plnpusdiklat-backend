const { redis } = require('../helpers/redis');
const UjisoftJawaban = require('../models/ujisoft_jawaban');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('ujisoft_jawaban')
      if (!redisFound) {
        let found = await UjisoftJawaban.find()
        await redis.set('ujisoft_jawaban', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await UjisoftJawaban.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByUjisoft: async (req, res, next) => {
    try {
      let found = await UjisoftJawaban.findOne({ujisoft_id: req.params.id})
      if (!found) {
         res.status(200).json({})
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findBySiswa: async (req, res, next) => {
   try {
     let found = await UjisoftJawaban.find({siswa_id: req.params.id})
     if (!found) {
        res.status(200).json([])
     }
     res.status(200).json(found)
   } catch (err) {
     next(err)
   }
 },
  create: async (req, res, next) => {
    try {
      let newUjisoftJawaban = await UjisoftJawaban.create(req.body)
      await redis.set(newUjisoftJawaban._id, JSON.stringify(newUjisoftJawaban))
      const redisFound = await redis.get("ujisoft_jawaban")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newUjisoftJawaban)
        await redis.set('ujisoft_jawaban', JSON.stringify(parsed))
      }
      res.status(201).json(newUjisoftJawaban)
    } catch (err) {
      next(err)
    }
  },
//   updateQuestion: async (req, res, next) => {
//     try {
//       let { id } = req.params
//       let { bidang, jawaban } = req.body
//       let target = await Ujisoft.findOneAndUpdate({ _id: id }, { ujisoft: { bidang, jawaban } }, { rawResult: true, new: true })
//       if (target.value === null) {
//         throw { status: 404, message: 'Data Not Found'}
//       }
//       await redis.set(target.value._id, JSON.stringify(target.value))
//       await redis.del('ujisoft')
//       res.status(200).json(target)
//     } catch (err) {
//       next(err)
//     }
//   },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await UjisoftJawaban.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('ujisoft_jawaban')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await UjisoftJawaban.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('ujisoft_jawaban')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
