const { redis } = require('../helpers/redis');
const LembarObservasi = require('../models/lembar_observasi');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('lembar_observasi')
      if (!redisFound) {
        let found = await LembarObservasi.find()
        await redis.set('lembar_observasi', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await LembarObservasi.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newLembarObservasi = await LembarObservasi.create(req.body)
      await redis.set(newLembarObservasi._id, JSON.stringify(newLembarObservasi))
      const redisFound = await redis.get("lembar_observasi")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newLembarObservasi)
        await redis.set('lembar_observasi', JSON.stringify(parsed))
      }
      res.status(201).json(newLembarObservasi)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await LembarObservasi.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('lembar_observasi')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await LembarObservasi.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('lembar_observasi')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
