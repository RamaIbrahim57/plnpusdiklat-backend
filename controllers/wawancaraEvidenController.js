const { redis } = require('../helpers/redis');
const WawancaraEviden = require('../models/wawancara_eviden');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('wawancara_eviden')
      if (!redisFound) {
        let found = await WawancaraEviden.find()
        await redis.set('wawancara_eviden', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await WawancaraEviden.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  findByPsikolog: async (req, res, next) => {
   try {
     let found = await WawancaraEviden.findOne({psikolog_id: req.params.id})
     if (!found) {
        res.status(200).json(null)
     }
     res.status(200).json(found)
   } catch (err) {
     next(err)
   }
 },
  create: async (req, res, next) => {
    try {
      let newEviden = await WawancaraEviden.create(req.body)
      await redis.set(newEviden._id, JSON.stringify(newEviden))
      const redisFound = await redis.get("wawancara_eviden")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newEviden)
        await redis.set('wawancara_eviden', JSON.stringify(parsed))
      }
      res.status(201).json(newEviden)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await WawancaraEviden.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('wawancara_eviden')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await WawancaraEviden.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('wawancara_eviden')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }

}
