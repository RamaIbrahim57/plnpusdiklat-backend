const Siswa = require('../models/siswa');
const Password = require('../models/password');
const Superadmin = require('../models/superadmin');
const Mentor = require('../models/mentor2');
const Psikolog = require('../models/psikolog');
const Updl = require('../models/updl');
const Penguji = require('../models/penguji');
const Talenta = require('../models/talenta');
const AdminCoop = require('../models/admincoop');
const AdminPt = require('../models/adminpt');
const PasswordReset = require('../models/password_reset');
const { transporter } = require('../helpers/nodemailer')
const { registerSiswa, validate, loginSiswa } = require('../validation/auth')
const signInSession = require('../auth/siswa').signIn
const { hashToken, verifyToken, sign } = require('../helpers/jwt')
const generatePassword = require('secure-random-password');
const bcryptjs = require('bcryptjs');
const { redis } = require('../helpers/redis')
require('dotenv').config()

module.exports = {
  currentAccount: async (req, res, next) => {
    try {
      const { accesstoken } = req.headers
      if (accesstoken) {
        const userData = verifyToken(accesstoken)
        userData.accessToken = accesstoken
        res.status(201).json({userData});
      } else {
        throw { status: 401, message: 'Authentication Failed' }
      }
    } catch (err) {
      next(err)
    }
  },
  findAllByName: async (req, res, next) => {
    try {
      let found = await Siswa.find({nama: { "$regex": req.params.id, "$options": "i" }, angkatan_id: req.params.angkatan, program: req.params.program })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  // Siswa
  signUp: async (req, res, next) => {
    try {
      const { nik, notest, asal_rekrutmen, nama, jenjang, email, univ,hp1, gender, address, program, angkatan_id, tanggallahir, tempatlahir, agama, fase } = req.body;
      const foundSiswa = await Siswa.exists({ nik })
      const foundEmail = await Siswa.exists({ email })
      if (foundSiswa) {
        throw { status: 400, message: "Nik Sudah Terdaftar" }
      }
      if (foundEmail) {
        throw { status: 400, message: "Email Sudah Terdaftar" }
      }
      req.body.role = 'siswa'
      const siswa = await Siswa.create(req.body)
      await redis.set(nik, JSON.stringify(siswa))
      const foundRedis = await redis.get('siswa')
      if (foundRedis) {
        const parsed = JSON.parse(foundRedis)
        parsed.push(siswa)
        await redis.set('siswa', JSON.stringify(parsed))
        await redis.del('siswaangkatanprogram' + angkatan_id + program)
      }
      res.status(201).json({ siswa, nik });
    } catch (err) {
      next(err)
    }
  },
  signIn: async (req, res, next) => {
    try {
      await validate(loginSiswa, req.body)
      const { nik, password } = req.body
      let user = await redis.get(nik)
      if (user === 'null' || !user) {
        user = await Siswa.findOne({ nik })
        if (!user) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
        await redis.set(nik, JSON.stringify(user))
      }
      user = typeof user === 'string' ? JSON.parse(user) : user
      const redisFoundPassword = await redis.get(user.password)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: user.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(password, foundPassword.password)
      if (verify) {
        await redis.set(user.password, JSON.stringify(foundPassword))
        await redis.set(user._id + password, JSON.stringify(foundPassword))
        if (user.program === 'coop') {
          user.role = 'siswacoop'
        }
        if (user.program === 'pt') {
          user.role = 'siswapt'
        }
        let access_token = hashToken({
          id: user._id,
          nik: user.nik,
          notest: user.notest,
          role: user.role,
          email: user.email,
          hp1: user.hp1,
          name: user.nama,
          gender: user.gender,
          agama: user.agama,
          address: user.address,
          program: user.program,
          jenjang: user.jenjang,
          univ: user.univ,
          agama: user.agama,
          fase: user.fase,
          mentor2: user.mentor2 ? user.mentor2 : '',
          mentor1: user.mentor1 ? user.mentor1 : '',
          jabatan: user.jabatan ? user.jabatan : '',
          angkatan_id: user.angkatan_id ? user.angkatan_id : '',
        })
        res.status(200).json({access_token})
      } else {
        throw {status: 401, message: 'Incorrect nik or password' }
      }
      res.status(200).json(findPassword)
    } catch (err) {
      next(err)
    }
  },
  resetPassRequest: async (req, res, next) => {
    try {
      console.log(req.body)
      const siswa = await Siswa.findOne({ email: req.body.email })
      if (!siswa) {
        throw { status: 404, message: 'Email tidak ditemukan' }
      }
      const passwordRes = await PasswordReset.create({ siswa_id: siswa._id })
      const output = `
      <div class="mb-5">
      <div
        style="width: 100%; background: #f2f4f8; padding: 50px 20px; color: #514d6a; border-radius: 5px;"
      >
        <div style="max-width: 700px; margin: 0px auto; font-size: 14px">
          <table style="border-collapse: collapse; border: 0; width: 100%; margin-bottom: 20px">
            <tr>
              <td style="vertical-align: top;">
              </td>
              <td style="text-align: right; vertical-align: middle;">
                <span style="color: #a09bb9;">Reset Password</span>
              </td>
            </tr>
          </table>

          <div style="padding: 40px 40px 20px 40px; background: #fff;">
            <table style="border-collapse: collapse; border: 0; width: 100%;">
              <tbody>
                <tr>
                  <td>
                    <h5
                      style="margin-bottom: 20px; color: #24222f; font-weight: 600"
                    >Password Reset</h5>
                    <p>
                      Sebelumnya anda pernah membuat permohonan pergantian password, jika iya maka klik button dibawah untuk mereset password akun anda
                    </p>
                    <div style="text-align: center">
                      <a
                        href="${process.env.RESET_LINK_SISWA}${passwordRes._id}"
                        style="display: inline-block; padding: 11px 30px 6px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #01a8fe; border-radius: 5px"
                      >Reset Password</a>
                    </div>
                    <p>Jika anda tidak pernah membuat permohonan perganti password, harap abaikan email ini</p>
                    <p>
                      Regards,
                      <br />PLN Pusdiklat Web Monitoring
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div style="text-align: center; font-size: 12px; color: #a09bb9; margin-top: 20px">
            <p>
              PLN Pusdiklat Web Monitoring
              <br />2021 Digitala
            </p>
          </div>
        </div>
      </div>
    </div>
      `;
      let mailOptions = {
          from: '_mainaccount@digitala.id', // TODO: email sender
          to: siswa.email, // TODO: email receiver
          subject: 'Reset your PLN Web Monitoring Password',
          text: `<h1>Wooohooo it works!!</h1>`,
          html: `${output}`
      };
      transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
          throw { status: 500, message: 'Internal Server Error' }
        }
      });
      res.status(200).json(passwordRes)
    } catch (err) {
      next(err)
    }
  },
  resetPassVerify: async (req, res, next) => {
    try {
      const request = await PasswordReset.findOne({ _id: req.params.id })
      if (request) {
        let generatedPassword = generatePassword.randomPassword();
        // let generatedPassword = '1234567890'
        const newPassword = await Password.create({password: generatedPassword})
        let passwordQuery = await redis.get('passwords');
        await redis.set(newPassword._id, JSON.stringify(newPassword));
        if (!passwordQuery) {
          passwordQuery = await Password.find()
          await redis.set('passwords', JSON.stringify(passwordQuery))
        }
        let target = await Siswa.findOneAndUpdate({ _id: request.siswa_id }, { password: newPassword._id }, { rawResult: true, new: true })
        if (target.value == null) {
          throw { status: 404, message: 'Siswa not found' }
        }
        await redis.set(target.value.nik, JSON.stringify(target.value))
        await redis.del('siswa')
        await PasswordReset.findOneAndDelete({_id: req.params.id }, { rawResult: true })
        const output = `
          <p>Welcome to PLN Pusdiklat Web Monitoring, please keep your credentials for log in.</p>
          <h3>Contact Details</h3>
          <ul>  
            <li>Nik: ${target.value.nik}</li>
            <li>Password: ${generatedPassword}</li>
          </ul>
        `;
        let mailOptions = {
            from: '_mainaccount@digitala.id', // TODO: email sender
            to: target.value.email, // TODO: email receiver
            subject: 'Reset Password',
            text: `<h1>Wooohooo it works!!</h1>`,
            html: `${output}`
        };
        transporter.sendMail(mailOptions, (err, data) => {
          if (err) {
            throw { status: 500, message: 'Internal Server Error' }
          }
          console.log('Email sent!!!');
        });
        res.status(200).json(request)
      } else {
        throw { status: 400, message: 'Request not valid' }
      }
    } catch (err) {
      next(err)
    }
  },
  sendEmail: async (req, res, next) => {
    try {
      let generatedPassword = generatePassword.randomPassword();
      // let generatedPassword = '1234567890'
      const newPassword = await Password.create({password: generatedPassword})
      let passwordQuery = await redis.get('passwords');
      await redis.set(newPassword._id, JSON.stringify(newPassword));
      if (!passwordQuery) {
        passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
      }
      let target = await Siswa.findOneAndUpdate({ _id: req.params.id }, { password: newPassword._id }, { rawResult: true, new: true })
      if (target.value == null) {
        throw { status: 404, message: 'Siswa not found' }
      }
      await redis.set(target.value.nik, JSON.stringify(target.value))
      await redis.del('siswa')
      // res.status(200).json(target)
      const output = `
      <div
            style="width: 100%; background: #f2f4f8; padding: 50px 20px; color: #514d6a; border-radius: 5px;"
          >
            <div style="max-width: 700px; margin: 0px auto; font-size: 14px">
              <table style="border-collapse: collapse; border: 0; width: 100%; margin-bottom: 20px">
                <tr>
                  <td style="vertical-align: top;">
                    <img
                      src="images/logo-pln.png"
                      alt="PLN Pusdiklat Web Monitoring"
                      style="height: 40px"
                    />
                  </td>
                  <td style="text-align: right; vertical-align: middle;">
                    <span style="color: #a09bb9;">Account Information</span>
                  </td>
                </tr>
              </table>

              <div style="padding: 40px 40px 20px 40px; background: #fff;">
                <table style="border-collapse: collapse; border: 0; width: 100%;">
                  <tbody>
                    <tr>
                      <td>
                        <p>Hi there,</p>
                        <p>
                        Welcome to PLN Pusdiklat Web Monitoring, please keep your credentials for log in.
                        </p>
                        <h3>Contact Details</h3>
                        <ul>  
                        <li>Nik: ${target.value.nik}</li>
                        <li>Password: ${generatedPassword}</li>
                      </ul>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div style="text-align: center; font-size: 12px; color: #a09bb9; margin-top: 20px">
                <p>
                  PLN Pusdiklat Web Monitoring
                  <br />2021 Digitala
                </p>
              </div>
            </div>
          </div>
      `;
      let mailOptions = {
          from: '_mainaccount@digitala.id', // TODO: email sender
          to: target.value.email, // TODO: email receiver
          subject: 'PLN Pusdiklat Login Information',
          text: `<h1>Wooohooo it works!!</h1>`,
          html: `${output}`
      };
      transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
          throw { status: 500, message: 'Internal Server Error' }
        }
        console.log('Email sent!!!');
        res.status(200).json({message: 'success'})
      });
    } catch (err) {
      next (err)
    }
  },
  updatePassword: async (req, res, next) => {
    try {
      const { newPassword, confirmPassword, oldPassword } = req.body
      const { id } = req.params
      if (newPassword !== confirmPassword) {
        throw { status: 400, message: 'Confirm Password does not match' }
      }
      const siswa = await Siswa.findOne({ _id: id })
      if (!siswa) throw { status: 404, message: 'Siswa not found' }
      const redisFoundPassword = await redis.get(siswa.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: siswa.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(oldPassword, foundPassword.password)
      if (verify) {
        console.log(foundPassword.history, 'amasuk')
        let alreadyUsed = false
        foundPassword.history.forEach(x => {
          const wasUsed = bcryptjs.compareSync(newPassword, x.password)
          if (wasUsed) {
            alreadyUsed = true
            throw { status: 400, message: 'Password sudah pernah digunakan sebelumnya' }
          }
        })
        const hashedNewPassword = await bcryptjs.hash(newPassword, 12)
        foundPassword.history.push({password: hashedNewPassword})
        if (foundPassword.history.length > 7) foundPassword.history.splice(1, 0)
        foundPassword.password = hashedNewPassword
        const updatedPassword = await Password.findOneAndUpdate({_id: foundPassword._id}, { password: hashedNewPassword, history: foundPassword.history }, { rawResult: true })
        const passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
        await redis.set(siswa.password, JSON.stringify(foundPassword))
        res.status(200).json(updatedPassword)
      } else {
        throw { status: 401, message: 'Password salah' }
      }
      // const passwords = await redis.get('passwords')
      // const parsed = JSON.parse(passwords)
      // let foundPassword = false
      // parsed.forEach(x => {
      //   const verify = bcryptjs.compareSync(oldPassword, x.password)
      //   if (verify) {
      //     foundPassword = x
      //   }
      // })
      // console.log(foundPassword)
      // if (!foundPassword) {
      //   throw {status: 401, message: 'Password lama yang dimasukan salah' }
      // }
    } catch (err) {
      next(err)
    }
  },
  findByProgram: async (req, res, next) => {
    try {
      const { program } = req.body
      const foundRedis = await redis.get('siswa'+program)
      if (!foundRedis) {
        const found = await Siswa.find({ program })
        if (!found) {
          throw { status: 404, message: 'Data tidak ditemukan' }
        }
        await redis.set('siswa'+program, JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByPenguji: async (req, res, next) => {
    try {
      const { penguji } = req.params
      const foundRedis = await redis.get('siswa'+penguji)
      if (!foundRedis) {
        const found = await Siswa.find({ penguji })
        if (!found) {
          throw { status: 404, message: 'Data tidak ditemukan' }
        }
        await redis.set('siswa'+penguji, JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByAngkatan: async (req, res, next) => {
    try {
      const { angkatan } = req.params
      const foundRedis = await redis.get('siswa'+angkatan)
      if (!foundRedis) {
        const found = await Siswa.find({ angkatan_id: angkatan })
        if (!found) {
          throw { status: 404, message: 'Data tidak ditemukan' }
        }
        await redis.set('siswa'+angkatan, JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByAngkatanAndProgram: async (req, res, next) => {
    try {
      const page = req.query.page
      const limit = req.query.limit
      const startIndex = (page - 1) * limit
      const endIndex = page * limit
      const { angkatan, program } = req.params
      let found = await Siswa.find({ angkatan_id: angkatan, program })
        found = page && limit && found ? found.slice(startIndex, endIndex) : found
        if (!found) {
          res.status(200).json({})
        }
      // if (!foundRedis) {
      // }
      // let found = JSON.parse(foundRedis)
      // found = page && limit && found ? found.slice(startIndex, endIndex) : found
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  countDataByAngkatanAndProgram: async (req, res, next) => {
    try {
      const { angkatan, program } = req.params
      Siswa.countDocuments({ angkatan_id: angkatan, program }, function (err, count) {
        res.status(200).json({ length: count })
      });
    } catch (err) {
      next(err)
    }
  },
  findByAngkatanAndPenguji: async (req, res, next) => {
    try {
      const { angkatan, penguji } = req.params
      const found = await Siswa.find({ angkatan_id: angkatan, penguji })
      if (!found) {
        res.status(200).json({})
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findAll: async (req, res, next) => {
    try {
      const foundRedis = await redis.get('siswa')
      if (!foundRedis) {
        const found = await Siswa.find()
        if (!found) throw { status: 404, message: 'Data tidak ditemukan' }
        await redis.set('siswa', JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByNik: async (req, res, next) => {
    try {
      const { nik } = req.params
      const foundRedis = await redis.get('siswa'+nik)
      if (!foundRedis) {
        const found = await Siswa.findOne({ nik })
        if (!found) {
          throw { status: 404, message: 'Data tidak ditemukan' }
        }
        await redis.set('siswa'+nik, JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findSiswaS1: async (req, res, next) => {
    try {
      let found = await Siswa.find({ jenjang: { "$regex": "S1", "$options": "i" } })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findSiswaSLTA: async (req, res, next) => {
    try {
      const found = await Siswa.find({ jenjang: { "$regex": "SLTA", "$options": "i" } })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findSiswaS2: async (req, res, next) => {
    try {
      const found = await Siswa.find({ jenjang: { "$regex": "S2", "$options": "i" } })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findSiswaD3: async (req, res, next) => {
    try {
      const found = await Siswa.find({ jenjang: { "$regex": "D-III", "$options": "i" } })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findSiswaD3K: async (req, res, next) => {
    try {
      const found = await Siswa.find({ jenjang:  { "$regex": "D3K", "$options": "i" } })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByMentor: async (req, res, next) => {
    try {
      const found = await Siswa.find({ mentor2: req.params.id })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByAngkatanAndMentor: async (req, res, next) => {
    try {
      const found = await Siswa.find({ mentor2: req.params.id, angkatan_id: req.params.angkatan })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findByAngkatanAndUpdl: async (req, res, next) => {
    try {
      const found = await Siswa.find({ updl: req.params.id, angkatan_id: req.params.angkatan })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      console.log('a')
      const found = await Siswa.findOne({ _id: req.params.id })
      if (!found) {
        res.status(200).json({})
      }
        // await redis.set(req.params.id, JSON.stringify(found))
      res.status(200).json(found)
      // const found = JSON.parse(foundRedis)
      // res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      const { id } = req.params
      let target = await Siswa.findById(id, async function (err, client) {
        if (err) throw { status: 404, message: 'Data Not Found' }
        client.remove()
        const redisFound = await redis.get(id)
        if (redisFound) {
          await redis.del(id)
        }
        await redis.del('siswa')
        await redis.del('siswaangkatanprogram' + angkatan + program)
        redis.flushdb( function (err, succeeded) {
            console.log(succeeded); // will be true if successfull
        })
        res.status(200).json(target)
      })
      // if (target.value === null) {
      //   throw { status: 404, message: 'Data Not Found'}
      // }
    } catch (err) {
      next (err)
    }
  },
  addMentor: async (req, res, next) => {
    try {
      let { id } = req.params
      const target = await Siswa.findOneAndUpdate({ nik: id }, { mentor2: req.body.mentor2, mentor1: req.body.mentor1, updl: req.body.updl }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        const found = JSON.parse(redisFound)
        found.mentor2 = req.body.mentor2
        found.mentor1 = req.body.mentor1
        await redis.set(id, JSON.stringify(found))
      }
      await redis.del('siswa')
      await redis.del('siswaangkatanprogram' + angkatan + program)
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  gantiMentor1: async (req, res, next) => {
    try {
      let { id } = req.params
      const target = await Siswa.findOneAndUpdate({ _id: id }, { mentor1: req.body.mentor1 }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(target.value.nik)
      if (redisFound) {
        const found = JSON.parse(redisFound)
        found.mentor1 = req.body.mentor1
        await redis.set(target.value.nik, JSON.stringify(found))
      }
      await redis.del('siswaangkatanprogram' + angkatan + program)
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  gantiMentor2: async (req, res, next) => {
    try {
      let { id } = req.params
      const target = await Siswa.findOneAndUpdate({ _id: id }, { mentor2: req.body.mentor2 }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(target.value.nik)
      if (redisFound) {
        const found = JSON.parse(redisFound)
        found.mentor2 = req.body.mentor2
        await redis.set(target.value.nik, JSON.stringify(found))
      }
      await redis.del('siswaangkatanprogram' + angkatan + program)
      await redis.del('siswa')
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  addPenguji: async (req, res, next) => {
    try {
      let { id } = req.params
      const target = await Siswa.findOneAndUpdate({ _id: id }, { penguji: req.body.penguji }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(target.value.nik)
      if (redisFound) {
        const found = JSON.parse(redisFound)
        found.penguji = req.body.penguji
        await redis.set(target.value.nik, JSON.stringify(found))
      }
      await redis.del('siswaangkatanprogram' + angkatan + program)
      await redis.del('siswa')
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  addJabatan: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Siswa.findOneAndUpdate({ nik: id }, { jabatan: req.body.jabatan }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        const found = JSON.parse(redisFound)
        found.jabatan = req.body.jabatan
        await redis.set(id, JSON.stringify(found))
      }
      await redis.del('siswa')
      await redis.del('siswaangkatanprogram' + angkatan + program)
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  editSiswa: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Siswa.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('siswa')
      await redis.del('siswaangkatanprogram' + req.body.angkatan_id + program)
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  removeJabatan: async (req, res, next) => {
    try {
      let { id } = req.params
      console.log('targetaaa')
      let target = await Siswa.findOneAndUpdate({ _id: id }, { $unset: { jabatan: 1 } }, { rawResult: true, new: true })
      console.log(target, 'targeet')
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del(target.value.nik)
      await redis.del('siswa')
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  updateUjiAkhir: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Siswa.findOneAndUpdate({ _id: id }, { uji_akhir: req.body.uji_akhir }, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.set(target.value.nik, JSON.stringify(target.value))
      await redis.del('siswa')
      await redis.del('siswaangkatanprogram' + angkatan + program)
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  // Superadmin
  signInAdmin: async (req, res, next) => {
    try {
      const { nip, password } = req.body
      let admin = await redis.get('admin' + nip)
      if (!admin || admin == null || admin === 'null') {
        admin = await Superadmin.findOne({ nip })
        if (!admin) {
          throw { status: 401, message: "Incorrect nip or password"}
        }
        await redis.set('admin' + nip, JSON.stringify(admin))
      }
      admin = typeof admin === 'string' ? JSON.parse(admin) : admin
      const redisFoundPassword = await redis.get(admin.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        console.log('redis')
        console.log(redisFoundPassword, 'redisfoud')
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: admin.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(password, foundPassword.password)
      if (verify) {
        console.log(foundPassword, 'found')
        await redis.set(admin.password, JSON.stringify(foundPassword))
        console.log(verify, 'verif')
        await redis.set(admin._id + password, JSON.stringify(foundPassword))
        console.log(admin.name, 'name')
        let access_token = hashToken({
          id: admin._id,
          nip: admin.nip,
          name: admin.name,
          email: admin.email,
          role: 'pusdiklat',
        })
        res.status(200).json({access_token})
      } else {
        throw { status: 401, message: 'Incorrect nik or password'}
      }
      console.log(admin, 'admin')
      res.status(200).json({access_token})
    } catch (err) {
      next (err)
    }
  },
  signUpAdmin: async (req, res, next) => {
    try {
      const { nip, email, password, name } = req.body;
      const foundAdmin = await Superadmin.exists({ nip })
      if (foundAdmin) {
        throw { status: 400, message: "Nip Sudah Terdaftar" }
      }
      const newPassword = await Password.create({password})
      let passwordQuery = await redis.get('passwords')
      await redis.set(newPassword._id, JSON.stringify(newPassword));
      if (!passwordQuery) {
        passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
      }
      const newAdmin = new Superadmin({
        nip,
        email,
        name,
        password: newPassword._id,
        role: 'admin',
      });
      console.log(newPassword)
      const admin = await Superadmin.create(newAdmin)
      await redis.set(admin._id + password, JSON.stringify(newPassword))
      await redis.set('admin' + nip, JSON.stringify(admin))
      const foundRedis = await redis.get('superadmin')
      if (foundRedis) {
        const parsed = JSON.parse(foundRedis)
        parsed.push(admin)
        await redis.set('superadmin', JSON.stringify(parsed))
      }
      res.status(201).json(admin);
    } catch (err) {
      next(err)
    }
  },
  findAllAdmin: async (req, res, next) => {
    try {
      const foundRedis = await redis.get('superadmin')
      if (!foundRedis) {
        const found = await Superadmin.find()
        if (!found) {
          throw { status: 404, message: 'Data tidak ditemukan' }
        }
        await redis.set('superadmin', JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOneAdmin: async (req, res, next) => {
    try {
      const foundRedis = await redis.get(req.params.id)
      if (!foundRedis) {
        const found = await Superadmin.findOne({ _id: req.params.id })
        if (!found) {
          throw { status: 404, message: 'Data tidak ditemukan' }
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  deleteAdmin: async (req, res, next) => {
    try {
      const { id } = req.params
      let target = await Superadmin.findOneAndDelete({ _id: id }, { rawResult: true })
      console.log(target)
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('siswa')
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  updatePasswordAdmin: async (req, res, next) => {
    try {
      console.log(req.body)
      const { newPassword, confirmPassword, oldPassword } = req.body
      const { id } = req.params
      if (newPassword !== confirmPassword) {
        throw { status: 400, message: 'Confirm Password does not match' }
      }
      const superadmin = await Superadmin.findOne({ _id: id })
      if (!superadmin) throw { status: 404, message: 'superadmin not found' }
      const redisFoundPassword = await redis.get(superadmin.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: superadmin.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(oldPassword, foundPassword.password)
      if (verify) {
        console.log(foundPassword.history, 'amasuk')
        let alreadyUsed = false
        foundPassword.history.forEach(x => {
          const wasUsed = bcryptjs.compareSync(newPassword, x.password)
          if (wasUsed) {
            alreadyUsed = true
            throw { status: 400, message: 'Password sudah pernah digunakan sebelumnya' }
          }
        })
        const hashedNewPassword = await bcryptjs.hash(newPassword, 12)
        foundPassword.history.push({password: hashedNewPassword})
        foundPassword.password = hashedNewPassword
        const updatedPassword = await Password.findOneAndUpdate({_id: foundPassword._id}, { password: hashedNewPassword, history: foundPassword.history }, { rawResult: true })
        const passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
        await redis.set(superadmin.password, JSON.stringify(foundPassword))
        res.status(200).json(updatedPassword)
      } else {
        throw { status: 401, message: 'Password salah' }
      }
    } catch (err) {
      next(err)
    }
  },
  // Mentor
  signInMentor: async (req, res, next) => {
    try {
      const { nip, password } = req.body
      let mentor = await redis.get('mentor2' + nip)
      if (!mentor) {
        mentor = await Mentor.findOne({ nip })
        if (!mentor) {
          throw { status: 401, message: 'Incorrect nip or password'}
        }
        await redis.set('mentor2' + nip, JSON.stringify(mentor))
      }
      mentor = typeof mentor === 'string' ? JSON.parse(mentor) : mentor
      // const redisFoundPassword = await redis.get(mentor.password)
      console.log(mentor.password, 'mentor password')
      const foundPassword = await Password.findOne({ _id: mentor.password})
      console.log(foundPassword, 'found password')
      if (!foundPassword) {
        throw { status: 401, message: 'Incorrect nik or password'}
      }
      const verify = bcryptjs.compareSync(password, foundPassword.password)
      console.log(verify, 'verify')
      if (verify) {
        await redis.set(mentor.password, JSON.stringify(foundPassword))
        await redis.set(mentor._id + password, JSON.stringify(foundPassword))
        if (mentor.program === 'coop') {
          mentor.role = 'mentorcoop'
        }
        let access_token = hashToken({
            id: mentor._id,
            nip: mentor.nip,
            name: mentor.name,
            role: mentor.role,
            program: mentor.program,
            email: mentor.email,
          })
        res.status(200).json({access_token})
      } else {
        throw { status: 401, message: 'Incorrect nik or password'}
      }
      res.status(200).json({access_token})
    } catch (err) {
      next (err)
    }
  },
  signUpMentor: async (req, res, next) => {
    try {
      const { nip, email, password, name, hp1, program } = req.body;
      const foundMentor = await Mentor.exists({ nip })
      if (foundMentor) {
        throw { status: 400, message: "Nip Sudah Terdaftar" }
      }
      const newPassword = await Password.create({password})
      await redis.set(newPassword._id, JSON.stringify(newPassword));
      let passwordQuery = await redis.get('passwords')
      if (!passwordQuery) {
        passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
      }
      const newMentor = new Mentor({
        nip,
        email,
        password: newPassword._id,
        role: 'mentor',
        name,
        hp1,
        program,
      });
      const mentor = await Mentor.create(newMentor)
      await redis.set(mentor._id + password, JSON.stringify(newPassword))
      await redis.set('mentor2' + nip, JSON.stringify(mentor))
      const foundRedis = await redis.get('mentor2')
      if (foundRedis) {
        const parsed = JSON.parse(foundRedis)
        parsed.push(mentor)
        await redis.set('mentor2', JSON.stringify(parsed))
      }
      res.status(201).json(mentor);
    } catch (err) {
      next(err)
    }
  },
  resetPassRequestMentor: async (req, res, next) => {
    try {
      const mentor = await Mentor.findOne({ email: req.body.email })
      if (!mentor) {
        throw { status: 404, message: 'Email tidak ditemukan' }
      }
      const passwordRes = await PasswordReset.create({ mentor_id: mentor._id })
      const output = `
        <p>You've recently asked to reset the password for this account:</p>
        <h3>Contact Details</h3>
        <ul>  
          <li>Name: ${mentor.name}</li>
          <li>Email: ${mentor.email}</li>
        </ul>
        <p>To update your password, click the link below: </p>
        ${process.env.RESET_LINK_MENTOR}${passwordRes._id}
      `;
      let mailOptions = {
          from: '_mainaccount@digitala.id', // TODO: email sender
          to: mentor.email, // TODO: email receiver
          subject: 'Reset your PLN Web Monitoring Password',
          text: `<h1>Wooohooo it works!!</h1>`,
          html: `${output}`
      };
      transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
          throw { status: 500, message: 'Internal Server Error' }
        }
      });
      res.status(200).json(passwordRes)
    } catch (err) {
      next(err)
    }
  },
  resetPassVerifyMentor: async (req, res, next) => {
    try {
      const request = await PasswordReset.findOne({ _id: req.params.id })
      if (request) {
        let generatedPassword = generatePassword.randomPassword();
        // let generatedPassword = '1234567890'
        const newPassword = await Password.create({password: generatedPassword})
        let passwordQuery = await redis.get('passwords');
        await redis.set(newPassword._id, JSON.stringify(newPassword));
        if (!passwordQuery) {
          passwordQuery = await Password.find()
          await redis.set('passwords', JSON.stringify(passwordQuery))
        }
        let target = await Mentor.findOneAndUpdate({ _id: request.mentor_id }, { password: newPassword._id }, { rawResult: true, new: true })
        if (target.value == null) {
          throw { status: 404, message: 'Mentor not found' }
        }
        await redis.set('mentor2' + target.value.nip, JSON.stringify(target.value))
        await redis.del('mentor')
        await PasswordReset.findOneAndDelete({_id: req.params.id }, { rawResult: true })
        const output = `
          <p>Welcome to PLN Pusdiklat Web Monitoring, please keep your credentials for log in.</p>
          <h3>Contact Details</h3>
          <ul>  
            <li>Nip: ${target.value.nip}</li>
            <li>Password: ${generatedPassword}</li>
          </ul>
        `;
        let mailOptions = {
            from: '_mainaccount@digitala.id', // TODO: email sender
            to: target.value.email, // TODO: email receiver
            subject: 'Reset Password',
            text: `<h1>Wooohooo it works!!</h1>`,
            html: `${output}`
        };
        transporter.sendMail(mailOptions, (err, data) => {
          if (err) {
            throw { status: 500, message: 'Internal Server Error' }
          }
          console.log('Email sent!!!');
        });
        res.status(200).json(request)
      } else {
        throw { status: 400, message: 'Request not valid' }
      }
    } catch (err) {
      next(err)
    }
  },
  findAllMentor: async (req, res, next) => {
    try {
      const foundRedis = await redis.get('mentor2')
      if (!foundRedis) {
        const found = await Mentor.find()
        if (!found) {
          throw { status: 404, message: 'Data tidak ditemukan' }
        }
        await redis.set('mentor2', JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findMentorByProgram: async (req, res, next) => {
    try {
      const found = await Mentor.find({ program: req.params.program })
      if (!found) {
        res.status(200).json([])
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOneMentor: async (req, res, next) => {
    try {
      const foundRedis = await redis.get(req.params.id)
      if (!foundRedis) {
        const found = await Mentor.findOne({ _id: req.params.id })
        if (!found) {
          throw { status: 404, message: 'Data tidak ditemukan' }
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  deleteMentor: async (req, res, next) => {
    try {
      const { id } = req.params
      let target = await Mentor.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(target.value.nip)
      if (redisFound) {
        await redis.del(target.value.nip)
      }
      await redis.del('mentor2')
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  editMentor: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Mentor.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('mentor2')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updatePasswordMentor: async (req, res, next) => {
    try {
      console.log(req.body)
      const { newPassword, confirmPassword, oldPassword } = req.body
      const { id } = req.params
      if (newPassword !== confirmPassword) {
        throw { status: 400, message: 'Confirm Password does not match' }
      }
      const mentor = await Mentor.findOne({ _id: id })
      if (!mentor) throw { status: 404, message: 'mentor not found' }
      const redisFoundPassword = await redis.get(mentor.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: mentor.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nip or password'}
        }
      }
      const verify = bcryptjs.compareSync(oldPassword, foundPassword.password)
      if (verify) {
        console.log(foundPassword.history, 'amasuk')
        let alreadyUsed = false
        foundPassword.history.forEach(x => {
          const wasUsed = bcryptjs.compareSync(newPassword, x.password)
          if (wasUsed) {
            alreadyUsed = true
            throw { status: 400, message: 'Password sudah pernah digunakan sebelumnya' }
          }
        })
        const hashedNewPassword = await bcryptjs.hash(newPassword, 12)
        foundPassword.history.push({password: hashedNewPassword})
        foundPassword.password = hashedNewPassword
        const updatedPassword = await Password.findOneAndUpdate({_id: foundPassword._id}, { password: hashedNewPassword, history: foundPassword.history }, { rawResult: true })
        const passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
        await redis.set('mentor2' + mentor.nip, JSON.stringify(mentor))
        await redis.del(mentor.password)
        res.status(200).json(updatedPassword)
      } else {
        throw { status: 401, message: 'Password salah' }
      }
    } catch (err) {
      next(err)
    }
  },
  editPasswordMentor: async (req, res, next) => {
    try {
      const { newPassword } = req.body
      const { id } = req.params
      const mentor = await Mentor.findOne({ _id: id })
      if (!mentor) throw { status: 404, message: 'mentor not found' }
      const redisFoundPassword = await redis.get(mentor.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: mentor.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nip or password'}
        }
      }
      console.log(foundPassword.history, 'amasuk')
      let alreadyUsed = false
      foundPassword.history.forEach(x => {
        const wasUsed = bcryptjs.compareSync(newPassword, x.password)
        if (wasUsed) {
          alreadyUsed = true
          throw { status: 400, message: 'Password sudah pernah digunakan sebelumnya' }
        }
      })
      const hashedNewPassword = await bcryptjs.hash(newPassword, 12)
      foundPassword.history.push({password: hashedNewPassword})
      foundPassword.password = hashedNewPassword
      const updatedPassword = await Password.findOneAndUpdate({_id: foundPassword._id}, { password: hashedNewPassword, history: foundPassword.history }, { rawResult: true })
      const passwordQuery = await Password.find()
      await redis.set('passwords', JSON.stringify(passwordQuery))
      await redis.set('mentor2' + mentor.nip, JSON.stringify(mentor))
      await redis.del(mentor.password)
      const output = `
      <div
            style="width: 100%; background: #f2f4f8; padding: 50px 20px; color: #514d6a; border-radius: 5px;"
          >
            <div style="max-width: 700px; margin: 0px auto; font-size: 14px">
              <table style="border-collapse: collapse; border: 0; width: 100%; margin-bottom: 20px">
                <tr>
                  <td style="vertical-align: top;">
                    <img
                      src="images/logo-pln.png"
                      alt="PLN Pusdiklat Web Monitoring"
                      style="height: 40px"
                    />
                  </td>
                  <td style="text-align: right; vertical-align: middle;">
                    <span style="color: #a09bb9;">Account Information</span>
                  </td>
                </tr>
              </table>

              <div style="padding: 40px 40px 20px 40px; background: #fff;">
                <table style="border-collapse: collapse; border: 0; width: 100%;">
                  <tbody>
                    <tr>
                      <td>
                        <p>Hi there,</p>
                        <p>
                        Password akun mentor anda telah diubah
                        </p>
                        <h3>Contact Details</h3>
                        <ul>  
                        <li>NIP: ${mentor.nip}</li>
                        <li>Password: ${newPassword}</li>
                      </ul>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div style="text-align: center; font-size: 12px; color: #a09bb9; margin-top: 20px">
                <p>
                  PLN Pusdiklat Web Monitoring
                  <br />2021 Digitala
                </p>
              </div>
            </div>
          </div>
      `;
      let mailOptions = {
          from: '_mainaccount@digitala.id', // TODO: email sender
          to: mentor.email, // TODO: email receiver
          subject: 'PLN Pusdiklat Login Information',
          text: `<h1>Wooohooo it works!!</h1>`,
          html: `${output}`
      };
      transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
          throw { status: 500, message: 'Internal Server Error' }
        }
        console.log('Email sent!!!');
      });
      res.status(200).json(updatedPassword)
    } catch (err) {
      next(err)
    }
  },
  // Psikolog
  signInPsikolog: async (req, res, next) => {
    try {
      const { nip, password } = req.body
      let psikolog = await redis.get(nip)
      if (!psikolog) {
        console.log('adimin')
        psikolog = await Psikolog.findOne({ nip })
        if (psikolog) {
          await redis.set(nip, JSON.stringify(psikolog))
        } else {
          throw { status: 401, message: 'NIP atau Password salah'}
        }
      }
      psikolog = typeof psikolog === 'string' ? JSON.parse(psikolog) : psikolog
      const redisFoundPassword = await redis.get(psikolog.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        console.log('redis')
        console.log(redisFoundPassword, 'redisfoud')
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: psikolog.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(password, foundPassword.password)
      if (verify) {
        console.log(foundPassword, 'found')
        await redis.set(psikolog.password, JSON.stringify(foundPassword))
        console.log(verify, 'verif')
        await redis.set(psikolog._id + password, JSON.stringify(foundPassword))
        console.log(psikolog, 'temudjin')
        let access_token = hashToken({
          id: psikolog._id,
          nip: psikolog.nip,
          email: psikolog.email,
          name: psikolog.nama,
          role: 'psikolog',
        })
        res.status(200).json({access_token})
      } else {
        throw { status: 401, message: 'Incorrect nik or password'}
      }
    } catch (err) {
      next (err)
    }
  },
  signUpPsikolog: async (req, res, next) => {
    try {
      const { nip, email, password, nama  } = req.body;
      const foundPsikolog = await Psikolog.exists({ nip })
      if (foundPsikolog) {
        throw { status: 400, message: "Nip Sudah Terdaftar" }
      }
      const newPassword = await Password.create({password})
      await redis.set(newPassword._id, JSON.stringify(newPassword));
      await redis.set('pkg' + password, JSON.stringify(newPassword))
      let passwordQuery = await redis.get('passwords')
      if (!passwordQuery) {
        passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
      }
      const newPsikolog = new Psikolog({
        nip,
        email,
        nama,
        password: newPassword._id,
        role: 'psikolog',
      });
      console.log(newPassword)
      const psikolog = await Psikolog.create(newPsikolog)
      await redis.set(psikolog._id + password, JSON.stringify(newPassword))
      await redis.set(psikolog.nip, JSON.stringify(psikolog))
      const foundRedis = await redis.get('psikolog')
      if (foundRedis) {
        const parsed = JSON.parse(foundRedis)
        parsed.push(psikolog)
        await redis.set('psikolog', JSON.stringify(parsed))
      }
      res.status(201).json(psikolog);
    } catch (err) {
      next(err)
    }
  },
  updatePasswordPsikolog: async (req, res, next) => {
    try {
      console.log(req.body)
      const { newPassword, confirmPassword, oldPassword } = req.body
      const { id } = req.params
      if (newPassword !== confirmPassword) {
        throw { status: 400, message: 'Confirm Password does not match' }
      }
      const psikolog = await Psikolog.findOne({ _id: id })
      if (!psikolog) throw { status: 404, message: 'psikolog not found' }
      const redisFoundPassword = await redis.get(psikolog.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: psikolog.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(oldPassword, foundPassword.password)
      if (verify) {
        console.log(foundPassword.history, 'amasuk')
        let alreadyUsed = false
        foundPassword.history.forEach(x => {
          const wasUsed = bcryptjs.compareSync(newPassword, x.password)
          if (wasUsed) {
            alreadyUsed = true
            throw { status: 400, message: 'Password sudah pernah digunakan sebelumnya' }
          }
        })
        const hashedNewPassword = await bcryptjs.hash(newPassword, 12)
        foundPassword.history.push({password: hashedNewPassword})
        foundPassword.password = hashedNewPassword
        const updatedPassword = await Password.findOneAndUpdate({_id: foundPassword._id}, { password: hashedNewPassword, history: foundPassword.history }, { rawResult: true })
        const passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
        await redis.set(psikolog.password, JSON.stringify(foundPassword))
        res.status(200).json(updatedPassword)
      } else {
        throw { status: 401, message: 'Password salah' }
      }
    } catch (err) {
      next(err)
    }
  },
  resetPassRequestPsikolog: async (req, res, next) => {
    try {
      const psikolog = await Psikolog.findOne({ email: req.body.email })
      if (!psikolog) {
        throw { status: 404, message: 'Email tidak ditemukan' }
      }
      const passwordRes = await PasswordReset.create({ psikolog_id: psikolog._id })
      const output = `
        <p>You've recently asked to reset the password for this account:</p>
        <h3>Contact Details</h3>
        <ul>  
          <li>Name: ${psikolog.nama}</li>
          <li>Email: ${psikolog.email}</li>
        </ul>
        <p>To update your password, click the link below: </p>
        ${process.env.RESET_LINK_PSIKOLOG}${passwordRes._id}
      `;
      let mailOptions = {
          from: '_mainaccount@digitala.id', // TODO: email sender
          to: psikolog.email, // TODO: email receiver
          subject: 'Reset your PLN Web Monitoring Password',
          text: `<h1>Wooohooo it works!!</h1>`,
          html: `${output}`
      };
      transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
          throw { status: 500, message: 'Internal Server Error' }
        }
      });
      res.status(200).json(passwordRes)
    } catch (err) {
      next(err)
    }
  },
  resetPassVerifyPsikolog: async (req, res, next) => {
    try {
      const request = await PasswordReset.findOne({ _id: req.params.id })
      if (request) {
        let generatedPassword = generatePassword.randomPassword();
        // let generatedPassword = '1234567890'
        const newPassword = await Password.create({password: generatedPassword})
        let passwordQuery = await redis.get('passwords');
        await redis.set(newPassword._id, JSON.stringify(newPassword));
        if (!passwordQuery) {
          passwordQuery = await Password.find()
          await redis.set('passwords', JSON.stringify(passwordQuery))
        }
        let target = await Psikolog.findOneAndUpdate({ _id: request.psikolog_id }, { password: newPassword._id }, { rawResult: true, new: true })
        if (target.value == null) {
          throw { status: 404, message: 'Psikolog not found' }
        }
        await redis.set(target.value.nip, JSON.stringify(target.value))
        await redis.del('psikolog')
        await PasswordReset.findOneAndDelete({_id: req.params.id }, { rawResult: true })
        const output = `
          <p>Welcome to PLN Pusdiklat Web Monitoring, please keep your credentials for log in.</p>
          <h3>Contact Details</h3>
          <ul>  
            <li>Nip: ${target.value.nip}</li>
            <li>Password: ${generatedPassword}</li>
          </ul>
        `;
        let mailOptions = {
            from: '_mainaccount@digitala.id', // TODO: email sender
            to: target.value.email, // TODO: email receiver
            subject: 'Reset Password',
            text: `<h1>Wooohooo it works!!</h1>`,
            html: `${output}`
        };
        transporter.sendMail(mailOptions, (err, data) => {
          if (err) {
            throw { status: 500, message: 'Internal Server Error' }
          }
          console.log('Email sent!!!');
        });
        res.status(200).json(request)
      } else {
        throw { status: 400, message: 'Request not valid' }
      }
    } catch (err) {
      next(err)
    }
  },
  // UPDL
  signInUpdl: async (req, res, next) => {
    try {
      const { nip, password } = req.body
      let updl = await redis.get(nip)
      if (!updl) {
        updl = await Updl.findOne({ nip })
        if (!updl) {
          throw { status: '401', message: 'NIP atau password salah' }
        }
        await redis.set(nip, JSON.stringify(updl))
      }
      updl = typeof updl === 'string' ? JSON.parse(updl) : updl
      const redisFoundPassword = await redis.get(updl.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        console.log('redis')
        console.log(redisFoundPassword, 'redisfoud')
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: updl.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(password, foundPassword.password)
      if (verify) {
        console.log(foundPassword, 'found')
        await redis.set(updl.password, JSON.stringify(foundPassword))
        console.log(verify, 'verif')
        await redis.set(updl._id + password, JSON.stringify(foundPassword))
        console.log(updl, 'temudjin')
        let access_token = hashToken({
          id: updl._id,
          nip: updl.nip,
          email: updl.email,
          name: updl.name,
          unit: updl.unit,
          role: 'updl',
        })
        res.status(200).json({access_token})
      } else {
        throw { status: 401, message: 'Incorrect nik or password'}
      }
      res.status(200).json({access_token})
    } catch (err) {
      next (err)
    }
  },
  signUpUpdl: async (req, res, next) => {
    try {
      const { nip, email, password, name, unit  } = req.body;
      const foundUpdl = await Updl.exists({ nip })
      const foundUpdlEmail = await Updl.exists({ email })
      if (foundUpdl || foundUpdlEmail) {
        throw { status: 400, message: "Nip Sudah Terdaftar" }
      }
      const newPassword = await Password.create({password})
      await redis.set(newPassword._id, JSON.stringify(newPassword));
      await redis.set('updl' + password, JSON.stringify(newPassword))
      let passwordQuery = await redis.get('passwords')
      if (!passwordQuery) {
        passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
      }
      const newUpdl = new Updl({
        nip,
        email,
        name,
        password: newPassword._id,
        unit,
        role: 'updl',
      });
      console.log(newPassword)
      const updlData = await Updl.create(newUpdl)
      await redis.set(updlData._id + password, JSON.stringify(newPassword))
      await redis.set(nip, JSON.stringify(updlData))
      const foundRedis = await redis.get('updl')
      if (foundRedis) {
        const parsed = JSON.parse(foundRedis)
        parsed.push(updlData)
        await redis.set('updl', JSON.stringify(parsed))
      }
      res.status(201).json(updlData);
    } catch (err) {
      next(err)
    }
  },
  findAllUpdl: async (req, res, next) => {
    try {
      const foundRedis = await redis.get('updl')
      if (!foundRedis) {
        const found = await Updl.find()
        if (!found) {
          throw { status: 404, message: 'Data tidak ditemukan' }
        }
        await redis.set('updl', JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOneUpdl: async (req, res, next) => {
    try {
      const foundRedis = await redis.get(req.params.id)
      if (!foundRedis) {
        const found = await Updl.findOne({ _id: req.params.id })
        if (!found) {
          throw { status: 404, message: 'Data tidak ditemukan' }
        }
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findUpdlByName: async (req, res, next) => {
    try {
      const found = await Updl.findOne({ name: req.params.id })
      if (!found) {
        throw { status: 404, message: 'Data tidak ditemukan' }
      }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  editUpdl: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Updl.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      const getAllUpdl = await Updl.find()
      console.log(getAllUpdl, 'getallupdl')
      if (!getAllUpdl) {
        throw { status: 404, message: 'Data tidak ditemukan' }
      }
      await redis.set('updl', JSON.stringify(getAllUpdl))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  deleteUpdl: async (req, res, next) => {
    try {
      const { id } = req.params
      let target = await Updl.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(target.value.id)
      if (redisFound) {
        await redis.del(target.value.nip)
      }
      const getAllUpdl = await Updl.find()
      if (!getAllUpdl) {
        throw { status: 404, message: 'Data tidak ditemukan' }
      }
      await redis.set('updl', JSON.stringify(getAllUpdl))
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  resetPassRequestUpdl: async (req, res, next) => {
    try {
      const updl = await Updl.findOne({ email: req.body.email })
      if (!updl) {
        throw { status: 404, message: 'Email tidak ditemukan' }
      }
      const passwordRes = await PasswordReset.create({ updl_id: updl._id })
      const output = `
        <p>You've recently asked to reset the password for this account:</p>
        <h3>Contact Details</h3>
        <ul>  
          <li>Name: ${updl.name}</li>
          <li>Email: ${updl.email}</li>
        </ul>
        <p>To update your password, click the link below: </p>
        ${process.env.RESET_LINK_UPDL}${passwordRes._id}
      `;
      let mailOptions = {
          from: '_mainaccount@digitala.id', // TODO: email sender
          to: updl.email, // TODO: email receiver
          subject: 'Reset your PLN Web Monitoring Password',
          text: `<h1>Wooohooo it works!!</h1>`,
          html: `${output}`
      };
      transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
          throw { status: 500, message: 'Internal Server Error' }
        }
      });
      res.status(200).json(passwordRes)
    } catch (err) {
      next(err)
    }
  },
  resetPassVerifyUpdl: async (req, res, next) => {
    try {
      const request = await PasswordReset.findOne({ _id: req.params.id })
      if (request) {
        let generatedPassword = generatePassword.randomPassword();
        // let generatedPassword = '1234567890'
        const newPassword = await Password.create({password: generatedPassword})
        let passwordQuery = await redis.get('passwords');
        await redis.set(newPassword._id, JSON.stringify(newPassword));
        if (!passwordQuery) {
          passwordQuery = await Password.find()
          await redis.set('passwords', JSON.stringify(passwordQuery))
        }
        let target = await Updl.findOneAndUpdate({ _id: request.updl_id }, { password: newPassword._id }, { rawResult: true, new: true })
        if (target.value == null) {
          throw { status: 404, message: 'Updl not found' }
        }
        await redis.set(target.value.nip, JSON.stringify(target.value))
        await redis.del('updl')
        await PasswordReset.findOneAndDelete({_id: req.params.id }, { rawResult: true })
        const output = `
          <p>Welcome to PLN Pusdiklat Web Monitoring, please keep your credentials for log in.</p>
          <h3>Contact Details</h3>
          <ul>  
            <li>Nip: ${target.value.nip}</li>
            <li>Password: ${generatedPassword}</li>
          </ul>
        `;
        let mailOptions = {
            from: '_mainaccount@digitala.id', // TODO: email sender
            to: target.value.email, // TODO: email receiver
            subject: 'Reset Password',
            text: `<h1>Wooohooo it works!!</h1>`,
            html: `${output}`
        };
        transporter.sendMail(mailOptions, (err, data) => {
          if (err) {
            throw { status: 500, message: 'Internal Server Error' }
          }
          console.log('Email sent!!!');
        });
        res.status(200).json(request)
      } else {
        throw { status: 400, message: 'Request not valid' }
      }
    } catch (err) {
      next(err)
    }
  },
  updatePasswordUpdl: async (req, res, next) => {
    try {
      console.log(req.body)
      const { newPassword, confirmPassword, oldPassword } = req.body
      const { id } = req.params
      if (newPassword !== confirmPassword) {
        throw { status: 400, message: 'Confirm Password does not match' }
      }
      const updl = await Updl.findOne({ _id: id })
      if (!updl) throw { status: 404, message: 'updl not found' }
      const redisFoundPassword = await redis.get(updl.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: updl.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(oldPassword, foundPassword.password)
      if (verify) {
        console.log(foundPassword.history, 'amasuk')
        let alreadyUsed = false
        foundPassword.history.forEach(x => {
          const wasUsed = bcryptjs.compareSync(newPassword, x.password)
          if (wasUsed) {
            alreadyUsed = true
            throw { status: 400, message: 'Password sudah pernah digunakan sebelumnya' }
          }
        })
        const hashedNewPassword = await bcryptjs.hash(newPassword, 12)
        foundPassword.history.push({password: hashedNewPassword})
        foundPassword.password = hashedNewPassword
        const updatedPassword = await Password.findOneAndUpdate({_id: foundPassword._id}, { password: hashedNewPassword, history: foundPassword.history }, { rawResult: true })
        const passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
        await redis.set(updl.password, JSON.stringify(foundPassword))
        res.status(200).json(updatedPassword)
      } else {
        throw { status: 401, message: 'Password salah' }
      }
    } catch (err) {
      next(err)
    }
  },
  // Penguji
  signInPenguji: async (req, res, next) => {
    try {
      const { nip, password } = req.body
      let penguji = await redis.get(nip)
      if (!penguji) {
        penguji = await Penguji.findOne({ nip })
        if (!penguji) throw { status: 401, message: 'Incorrect NIP or password' }
        await redis.set(nip, JSON.stringify(penguji))
      }
      penguji = typeof penguji === 'string' ? JSON.parse(penguji) : penguji
      const redisFoundPassword = await redis.get(penguji.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        console.log('redis')
        console.log(redisFoundPassword, 'redisfoud')
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: penguji.password})
        console.log(penguji.password, 'foudpassword')
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(password, foundPassword.password)
      console.log(password)
      if (verify) {
        console.log(foundPassword, 'found')
        await redis.set(penguji.password, JSON.stringify(foundPassword))
        console.log(verify, 'verif')
        await redis.set(penguji._id + password, JSON.stringify(foundPassword))
        console.log(penguji, 'temudjin')
        let access_token = hashToken({
            id: penguji._id,
            nip: penguji.nip,
            email: penguji.email,
            name: penguji.name,
            unit: penguji.unit,
            role: 'penguji',
          })
        res.status(200).json({access_token})
      } else {
        throw { status: 401, message: 'Incorrect nik or password'}
      }
    } catch (err) {
      next (err)
    }
  },
  signUpPenguji: async (req, res, next) => {
    try {
      const { nip, email, password, name, unit, bidang, hp1  } = req.body;
      const foundPenguji = await Penguji.exists({ nip })
      if (foundPenguji) {
        throw { status: 400, message: "Nip Sudah Terdaftar" }
      }
      console.log(name, 'name')
      const newPassword = await Password.create({password})
      await redis.set(newPassword._id, JSON.stringify(newPassword));
      let passwordQuery = await redis.get('passwords')
      if (!passwordQuery) {
        passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
      }
      const newPenguji = new Penguji({
        nip,
        email,
        name,
        password: newPassword._id,
        unit,
        bidang,
        hp1,
        role: 'penguji',
      });
      console.log(newPassword)
      const pengujiData = await Penguji.create(newPenguji)
      await redis.set(pengujiData._id + password, JSON.stringify(newPassword))
      await redis.set(nip, JSON.stringify(pengujiData))
      const foundRedis = await redis.get('penguji')
      if (foundRedis) {
        const parsed = JSON.parse(foundRedis)
        parsed.push(pengujiData)
        await redis.set('penguji', JSON.stringify(parsed))
      }
      res.status(201).json(pengujiData);
    } catch (err) {
      next(err)
    }
  },
  findAllPenguji: async (req, res, next) => {
    try {
      const foundRedis = await redis.get('penguji')
      if (!foundRedis) {
        const found = await Penguji.find()
        if (!found) throw { status: 404, message: 'Data tidak ditemukan' }
        await redis.set('penguji', JSON.stringify(found))
        res.status(200).json(found)
      }
      const found = JSON.parse(foundRedis)
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOnePenguji: async (req, res, next) => {
    try {
      const found = await Penguji.findOne({ _id: req.params.id })
      if (!found) throw { status: 404, message: 'Data tidak ditemukan' }
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  deletePenguji: async (req, res, next) => {
    try {
      const { id } = req.params
      let target = await Penguji.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(target.value.nip)
      if (redisFound) {
        await redis.del(target.value.nip)
      }
      await redis.del('penguji')
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  editPenguji: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Penguji.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del('penguji')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updatePasswordPenguji: async (req, res, next) => {
    try {
      console.log(req.body)
      const { newPassword, confirmPassword, oldPassword } = req.body
      const { id } = req.params
      if (newPassword !== confirmPassword) {
        throw { status: 400, message: 'Confirm Password does not match' }
      }
      const penguji = await Penguji.findOne({ _id: id })
      if (!penguji) throw { status: 404, message: 'penguji not found' }
      const redisFoundPassword = await redis.get(penguji.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: penguji.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(oldPassword, foundPassword.password)
      if (verify) {
        console.log(foundPassword.history, 'amasuk')
        let alreadyUsed = false
        foundPassword.history.forEach(x => {
          const wasUsed = bcryptjs.compareSync(newPassword, x.password)
          if (wasUsed) {
            alreadyUsed = true
            throw { status: 400, message: 'Password sudah pernah digunakan sebelumnya' }
          }
        })
        const hashedNewPassword = await bcryptjs.hash(newPassword, 12)
        foundPassword.history.push({password: hashedNewPassword})
        foundPassword.password = hashedNewPassword
        const updatedPassword = await Password.findOneAndUpdate({_id: foundPassword._id}, { password: hashedNewPassword, history: foundPassword.history }, { rawResult: true })
        const passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
        await redis.set(penguji.password, JSON.stringify(foundPassword))
        res.status(200).json(updatedPassword)
      } else {
        throw { status: 401, message: 'Password salah' }
      }
    } catch (err) {
      next(err)
    }
  },
  editPasswordPenguji: async (req, res, next) => {
    try {
      console.log(req.body)
      const { newPassword } = req.body
      const { id } = req.params
      const penguji = await Penguji.findOne({ _id: id })
      if (!penguji) throw { status: 404, message: 'penguji not found' }
      const redisFoundPassword = await redis.get(penguji.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: penguji.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      console.log(foundPassword.history, 'amasuk')
      let alreadyUsed = false
      foundPassword.history.forEach(x => {
        const wasUsed = bcryptjs.compareSync(newPassword, x.password)
        if (wasUsed) {
          alreadyUsed = true
          throw { status: 400, message: 'Password sudah pernah digunakan sebelumnya' }
        }
      })
      const hashedNewPassword = await bcryptjs.hash(newPassword, 12)
      foundPassword.history.push({password: hashedNewPassword})
      foundPassword.password = hashedNewPassword
      const updatedPassword = await Password.findOneAndUpdate({_id: foundPassword._id}, { password: hashedNewPassword, history: foundPassword.history }, { rawResult: true })
      const passwordQuery = await Password.find()
      await redis.set('passwords', JSON.stringify(passwordQuery))
      await redis.set(penguji.password, JSON.stringify(foundPassword))
      const output = `
      <div
            style="width: 100%; background: #f2f4f8; padding: 50px 20px; color: #514d6a; border-radius: 5px;"
          >
            <div style="max-width: 700px; margin: 0px auto; font-size: 14px">
              <table style="border-collapse: collapse; border: 0; width: 100%; margin-bottom: 20px">
                <tr>
                  <td style="vertical-align: top;">
                    <img
                      src="images/logo-pln.png"
                      alt="PLN Pusdiklat Web Monitoring"
                      style="height: 40px"
                    />
                  </td>
                  <td style="text-align: right; vertical-align: middle;">
                    <span style="color: #a09bb9;">Account Information</span>
                  </td>
                </tr>
              </table>

              <div style="padding: 40px 40px 20px 40px; background: #fff;">
                <table style="border-collapse: collapse; border: 0; width: 100%;">
                  <tbody>
                    <tr>
                      <td>
                        <p>Hi there,</p>
                        <p>
                        Password akun mentor anda telah diubah
                        </p>
                        <h3>Contact Details</h3>
                        <ul>  
                        <li>NIP: ${penguji.nip}</li>
                        <li>Password: ${newPassword}</li>
                      </ul>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div style="text-align: center; font-size: 12px; color: #a09bb9; margin-top: 20px">
                <p>
                  PLN Pusdiklat Web Monitoring
                  <br />2021 Digitala
                </p>
              </div>
            </div>
          </div>
      `;
      let mailOptions = {
          from: '_mainaccount@digitala.id', // TODO: email sender
          to: penguji.email, // TODO: email receiver
          subject: 'PLN Pusdiklat Login Information',
          text: `<h1>Wooohooo it works!!</h1>`,
          html: `${output}`
      };
      transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
          throw { status: 500, message: 'Internal Server Error' }
        }
        console.log('Email sent!!!');
      });
      res.status(200).json(updatedPassword)
    } catch (err) {
      next(err)
    }
  },
  resetPassRequestPenguji: async (req, res, next) => {
    try {
      const penguji = await Penguji.findOne({ email: req.body.email })
      if (!penguji) {
        throw { status: 404, message: 'Email tidak ditemukan' }
      }
      const passwordRes = await PasswordReset.create({ penguji_id: penguji._id })
      const output = `
        <p>You've recently asked to reset the password for this account:</p>
        <h3>Contact Details</h3>
        <ul>  
          <li>Name: ${penguji.name}</li>
          <li>Email: ${penguji.email}</li>
        </ul>
        <p>To update your password, click the link below: </p>
        ${process.env.RESET_LINK_PENGUJI}${passwordRes._id}
      `;
      let mailOptions = {
          from: '_mainaccount@digitala.id', // TODO: email sender
          to: penguji.email, // TODO: email receiver
          subject: 'Reset your PLN Web Monitoring Password',
          text: `<h1>Wooohooo it works!!</h1>`,
          html: `${output}`
      };
      transporter.sendMail(mailOptions, (err, data) => {
        if (err) {
          throw { status: 500, message: 'Internal Server Error' }
        }
      });
      res.status(200).json(passwordRes)
    } catch (err) {
      next(err)
    }
  },
  resetPassVerifyPenguji: async (req, res, next) => {
    try {
      const request = await PasswordReset.findOne({ _id: req.params.id })
      if (request) {
        let generatedPassword = generatePassword.randomPassword();
        // let generatedPassword = '1234567890'
        const newPassword = await Password.create({password: generatedPassword})
        let passwordQuery = await redis.get('passwords');
        await redis.set(newPassword._id, JSON.stringify(newPassword));
        if (!passwordQuery) {
          passwordQuery = await Password.find()
          await redis.set('passwords', JSON.stringify(passwordQuery))
        }
        let target = await Penguji.findOneAndUpdate({ _id: request.penguji_id }, { password: newPassword._id }, { rawResult: true, new: true })
        if (target.value == null) {
          throw { status: 404, message: 'Penguji not found' }
        }
        await redis.set(target.value.nip, JSON.stringify(target.value))
        await redis.del('penguji')
        await PasswordReset.findOneAndDelete({_id: req.params.id }, { rawResult: true })
        const output = `
          <p>Welcome to PLN Pusdiklat Web Monitoring, please keep your credentials for log in.</p>
          <h3>Contact Details</h3>
          <ul>  
            <li>Nip: ${target.value.nip}</li>
            <li>Password: ${generatedPassword}</li>
          </ul>
        `;
        let mailOptions = {
            from: '_mainaccount@digitala.id', // TODO: email sender
            to: target.value.email, // TODO: email receiver
            subject: 'Reset Password',
            text: `<h1>Wooohooo it works!!</h1>`,
            html: `${output}`
        };
        transporter.sendMail(mailOptions, (err, data) => {
          if (err) {
            throw { status: 500, message: 'Internal Server Error' }
          }
          console.log('Email sent!!!');
        });
        res.status(200).json(request)
      } else {
        throw { status: 400, message: 'Request not valid' }
      }
    } catch (err) {
      next(err)
    }
  },
  // Talenta
  signInTalenta: async (req, res, next) => {
    try {
      const { nip, password } = req.body
      let talenta = await redis.get(nip)
      if (!talenta) {
        talenta = await Talenta.findOne({ nip })
        await redis.set(nip, JSON.stringify(talenta))
      }
      talenta = typeof talenta === 'string' ? JSON.parse(talenta) : talenta
      const redisFoundPassword = await redis.get(talenta.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        console.log('redis')
        console.log(redisFoundPassword, 'redisfoud')
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: talenta.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(password, foundPassword.password)
      if (verify) {
        console.log(foundPassword, 'found')
        await redis.set(talenta.password, JSON.stringify(foundPassword))
        console.log(verify, 'verif')
        await redis.set(talenta._id + password, JSON.stringify(foundPassword))
        console.log(talenta, 'temudjin')
        let access_token = hashToken({
            id: talenta._id,
            nip: talenta.nip,
            email: talenta.email,
            name: talenta.name,
            role: 'talenta',
          })
        res.status(200).json({access_token})
      } else {
        throw { status: 401, message: 'Incorrect nik or password'}
      }
    } catch (err) {
      next (err)
    }
  },
  signUpTalenta: async (req, res, next) => {
    try {
      const { nip, email, password, name  } = req.body;
      const foundTalenta = await Talenta.exists({ nip })
      if (foundTalenta) {
        throw { status: 400, message: "Nip Sudah Terdaftar" }
      }
      console.log(name, 'name')
      const newPassword = await Password.create({password})
      await redis.set(newPassword._id, JSON.stringify(newPassword));
      let passwordQuery = await redis.get('passwords')
      if (!passwordQuery) {
        passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
      }
      const newTalenta = new Talenta({
        nip,
        email,
        name,
        password: newPassword._id,
        role: 'talenta',
      });
      console.log(newPassword)
      const talentaData = await Talenta.create(newTalenta)
      await redis.set(newTalenta._id + password, JSON.stringify(newPassword))
      await redis.set(nip, JSON.stringify(newTalenta))
      const foundRedis = await redis.get('talenta')
      if (foundRedis) {
        const parsed = JSON.parse(talentaData)
        parsed.push(talentaData)
        await redis.set('talenta', JSON.stringify(parsed))
      }
      res.status(201).json(talentaData);
    } catch (err) {
      next(err)
    }
  },
  updatePasswordTalenta: async (req, res, next) => {
    try {
      console.log(req.body)
      const { newPassword, confirmPassword, oldPassword } = req.body
      const { id } = req.params
      if (newPassword !== confirmPassword) {
        throw { status: 400, message: 'Confirm Password does not match' }
      }
      const talenta = await Talenta.findOne({ _id: id })
      if (!talenta) throw { status: 404, message: 'talenta not found' }
      const redisFoundPassword = await redis.get(talenta.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: talenta.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(oldPassword, foundPassword.password)
      if (verify) {
        console.log(foundPassword.history, 'amasuk')
        let alreadyUsed = false
        foundPassword.history.forEach(x => {
          const wasUsed = bcryptjs.compareSync(newPassword, x.password)
          if (wasUsed) {
            alreadyUsed = true
            throw { status: 400, message: 'Password sudah pernah digunakan sebelumnya' }
          }
        })
        const hashedNewPassword = await bcryptjs.hash(newPassword, 12)
        foundPassword.history.push({password: hashedNewPassword})
        foundPassword.password = hashedNewPassword
        const updatedPassword = await Password.findOneAndUpdate({_id: foundPassword._id}, { password: hashedNewPassword, history: foundPassword.history }, { rawResult: true })
        const passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
        await redis.set(talenta.password, JSON.stringify(foundPassword))
        res.status(200).json(updatedPassword)
      } else {
        throw { status: 401, message: 'Password salah' }
      }
    } catch (err) {
      next(err)
    }
  },
  // Admin COOP
  signInAdminCoop: async (req, res, next) => {
    try {
      const { nip, password } = req.body
      let admin = await redis.get('admincoop' + nip)
      if (!admin || admin == null || admin === 'null') {
        admin = await AdminCoop.findOne({ nip })
        if (!admin) {
          throw { status: 401, message: "Incorrect nip or password"}
        }
        await redis.set('admincoop' + nip, JSON.stringify(admin))
      }
      admin = typeof admin === 'string' ? JSON.parse(admin) : admin
      const redisFoundPassword = await redis.get(admin.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        console.log('redis')
        console.log(redisFoundPassword, 'redisfoud')
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: admin.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(password, foundPassword.password)
      if (verify) {
        console.log(foundPassword, 'found')
        await redis.set(admin.password, JSON.stringify(foundPassword))
        console.log(verify, 'verif')
        await redis.set(admin._id + password, JSON.stringify(foundPassword))
        console.log(admin.name, 'name')
        let access_token = hashToken({
          id: admin._id,
          nip: admin.nip,
          name: admin.name,
          role: admin.role,
          email: admin.email,
          role: 'admincoop',
        })
        res.status(200).json({access_token})
      } else {
        throw { status: 401, message: 'Incorrect nik or password'}
      }
      console.log(admin, 'admin')
      res.status(200).json({access_token})
    } catch (err) {
      next (err)
    }
  },
  signUpAdminCoop: async (req, res, next) => {
    try {
      const { nip, email, password, name } = req.body;
      const foundAdmin = await AdminCoop.exists({ nip })
      if (foundAdmin) {
        throw { status: 400, message: "Nip Sudah Terdaftar" }
      }
      const newPassword = await Password.create({password})
      let passwordQuery = await redis.get('passwords')
      await redis.set(newPassword._id, JSON.stringify(newPassword));
      if (!passwordQuery) {
        passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
      }
      const newAdmin = new AdminCoop({
        nip,
        email,
        password: newPassword._id,
        name,
        role: 'admincoop',
      });
      console.log(newPassword)
      const admin = await AdminCoop.create(newAdmin)
      await redis.set(admin._id + password, JSON.stringify(newPassword))
      await redis.set('admincoop' + nip, JSON.stringify(admin))
      const foundRedis = await redis.get('admincoop')
      if (foundRedis) {
        const parsed = JSON.parse(foundRedis)
        parsed.push(admin)
        await redis.set('admincoop', JSON.stringify(parsed))
      }
      res.status(201).json(admin);
    } catch (err) {
      next(err)
    }
  },
  updatePasswordAdminCoop: async (req, res, next) => {
    try {
      console.log(req.body)
      const { newPassword, confirmPassword, oldPassword } = req.body
      const { id } = req.params
      if (newPassword !== confirmPassword) {
        throw { status: 400, message: 'Confirm Password does not match' }
      }
      const admincoop = await AdminCoop.findOne({ _id: id })
      if (!admincoop) throw { status: 404, message: 'admincoop not found' }
      const redisFoundPassword = await redis.get(admincoop.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: admincoop.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(oldPassword, foundPassword.password)
      if (verify) {
        console.log(foundPassword.history, 'amasuk')
        let alreadyUsed = false
        foundPassword.history.forEach(x => {
          const wasUsed = bcryptjs.compareSync(newPassword, x.password)
          if (wasUsed) {
            alreadyUsed = true
            throw { status: 400, message: 'Password sudah pernah digunakan sebelumnya' }
          }
        })
        const hashedNewPassword = await bcryptjs.hash(newPassword, 12)
        foundPassword.history.push({password: hashedNewPassword})
        foundPassword.password = hashedNewPassword
        const updatedPassword = await Password.findOneAndUpdate({_id: foundPassword._id}, { password: hashedNewPassword, history: foundPassword.history }, { rawResult: true })
        const passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
        await redis.set(admincoop.password, JSON.stringify(foundPassword))
        res.status(200).json(updatedPassword)
      } else {
        throw { status: 401, message: 'Password salah' }
      }
    } catch (err) {
      next(err)
    }
  },
  // Admin PT
  signInAdminPt: async (req, res, next) => {
    try {
      const { nip, password } = req.body
      let admin = await redis.get('adminpt' + nip)
      if (!admin || admin == null || admin === 'null') {
        admin = await AdminPt.findOne({ nip })
        if (!admin) {
          throw { status: 401, message: "Incorrect nip or password"}
        }
        await redis.set('adminpt' + nip, JSON.stringify(admin))
      }
      admin = typeof admin === 'string' ? JSON.parse(admin) : admin
      const redisFoundPassword = await redis.get(admin.password)
      console.log(redisFoundPassword)
      let foundPassword = {}
      if (redisFoundPassword !== null) {
        console.log('redis')
        console.log(redisFoundPassword, 'redisfoud')
        foundPassword = JSON.parse(redisFoundPassword)
      } else {
        foundPassword = await Password.findOne({ _id: admin.password})
        if (!foundPassword) {
          throw { status: 401, message: 'Incorrect nik or password'}
        }
      }
      const verify = bcryptjs.compareSync(password, foundPassword.password)
      if (verify) {
        console.log(foundPassword, 'found')
        await redis.set(admin.password, JSON.stringify(foundPassword))
        console.log(verify, 'verif')
        await redis.set(admin._id + password, JSON.stringify(foundPassword))
        console.log(admin.name, 'name')
        let access_token = hashToken({
          id: admin._id,
          nip: admin.nip,
          name: admin.name,
          role: admin.role,
          email: admin.email,
          role: 'adminpt',
        })
        res.status(200).json({access_token})
      } else {
        throw { status: 401, message: 'Incorrect nik or password'}
      }
      console.log(admin, 'admin')
      res.status(200).json({access_token})
    } catch (err) {
      next (err)
    }
  },
  signUpAdminPt: async (req, res, next) => {
    try {
      const { nip, email, password, name } = req.body;
      const foundAdmin = await AdminPt.exists({ nip })
      if (foundAdmin) {
        throw { status: 400, message: "Nip Sudah Terdaftar" }
      }
      const newPassword = await Password.create({password})
      let passwordQuery = await redis.get('passwords')
      await redis.set(newPassword._id, JSON.stringify(newPassword));
      if (!passwordQuery) {
        passwordQuery = await Password.find()
        await redis.set('passwords', JSON.stringify(passwordQuery))
      }
      const newAdmin = new AdminPt({
        nip,
        email,
        password: newPassword._id,
        name,
        role: 'adminpt',
      });
      console.log(newPassword)
      const admin = await AdminPt.create(newAdmin)
      await redis.set(admin._id + password, JSON.stringify(newPassword))
      await redis.set('adminpt' + nip, JSON.stringify(admin))
      const foundRedis = await redis.get('adminpt')
      if (foundRedis) {
        const parsed = JSON.parse(foundRedis)
        parsed.push(admin)
        await redis.set('adminpt', JSON.stringify(parsed))
      }
      res.status(201).json(admin);
    } catch (err) {
      next(err)
    }
  },
}