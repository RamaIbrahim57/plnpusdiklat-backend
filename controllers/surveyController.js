const { redis } = require('../helpers/redis');
const Survey = require('../models/survey');

module.exports = {
    // Read
  findAll: async (req, res, next) => {
    try {
      const redisFound = await redis.get('survey')
      if (!redisFound) {
        let found = await Survey.find()
        await redis.set('survey', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(redisFound)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  findByJabatan: async (req, res, next) => {
    try {
      let found = await Survey.find({ jabatan_id: req.params.id })
      res.status(200).json(found)
    } catch (err) {
      next(err)
    }
  },
  findOne: async (req, res, next) => {
    try {
      const redisFound = await redis.get(req.params.id)
      if (!redisFound) {
        let found = await Survey.findOne({_id: req.params.id})
        await redis.set(req.params.id, JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let parsed = JSON.parse(redisFound)
        res.status(200).json(parsed)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newSurvey = await Survey.create(req.body)
      await redis.set(newSurvey._id, JSON.stringify(newSurvey))
      const redisFound = await redis.get("survey")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newSurvey)
        await redis.set('survey', JSON.stringify(parsed))
      }
      res.status(201).json(newSurvey)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Survey.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      // await redis.del(target._id)
      await redis.set(target.value._id, JSON.stringify(target.value))
      await redis.del("survey")
      // await redis.set(found._id, JSON.stringify(found))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Survey.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      const redisFound = await redis.get(id)
      if (redisFound) {
        await redis.del(id)
      }
      await redis.del('survey')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  deleteByJabatan: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Survey.deleteMany({ jabatan_id: id })
      if (target.value === null) {
        throw { status: 404, message: 'Data Not Found'}
      }
      await redis.del('survey')
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  }
}
