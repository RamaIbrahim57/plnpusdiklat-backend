const Angkatan = require('../models/angkatan');
const { redis } = require('../helpers/redis')

module.exports = {
    // Read
  findOne: async (req, res, next) => {
    try {
      const foundRedis = await redis.get(req.params.id)
      if (!foundRedis) {
        console.log('not redis')
        let found = await Angkatan.findOne({_id: req.params.id})
        if (found) {
          await redis.set(req.params.id, JSON.stringify(found))
          res.status(200).json(found)
        } else {
          throw { status: 404, message: 'Angkatan tidak ditemukan' }
        }
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  setAsActive: async (req, res, next) => {
    try {
      const { id, program } = req.params
      let deactivateAll = await Angkatan.updateMany({ status_angkatan: 'aktif', program }, { status_angkatan: 'tidak aktif' })
      let target = await Angkatan.findOneAndUpdate({ _id: id }, { status_angkatan: 'aktif' })
      if (target.value === null) {
        throw { status: 404, message: 'Angkatan Tidak Ditemukan' }
      }
      await redis.del('angkatan')
      res.status(200).json(target)
    } catch (err) {
      next (err)
    }
  },
  findActive: async (req, res, next) => {
    try {
      const { program } = req.params
      let found = await Angkatan.findOne({status_angkatan: 'aktif', program})
      if (found) {
        res.status(200).json(found)
      } else {
        res.status(404).json({ message: 'Angkatan Aktif Not Found' })
      }
    } catch (err) {
      next(err)
    }
  },
  findByProgram: async (req, res, next) => {
    try {
      let found = await Angkatan.find({program: req.params.program})
      if (found) {
        res.status(200).json(found)
      } else {
        res.status(404).json([])
      }
    } catch (err) {
      next(err)
    }
  },
  findAll: async (req, res, next) => {
    try {
      const foundRedis = await redis.get('angkatan')
      if (!foundRedis) {
        let found = await Angkatan.find()
        await redis.set('angkatan', JSON.stringify(found))
        res.status(200).json(found)
      } else {
        let found = JSON.parse(foundRedis)
        res.status(200).json(found)
      }
    } catch (err) {
      next(err)
    }
  },
  create: async (req, res, next) => {
    try {
      let newAngkatan = await Angkatan.create(req.body)
      await redis.set(newAngkatan._id, JSON.stringify(newAngkatan))
      const redisFound = await redis.get("angkatan")
      if (redisFound) {
        const parsed = JSON.parse(redisFound)
        parsed.push(newAngkatan)
        await redis.set('angkatan', JSON.stringify(parsed))
      }
      res.status(201).json(newAngkatan)
    } catch (err) {
      next(err)
    }
  },
  update: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Angkatan.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Angkatan Tidak Ditemukan' }
      }
      await redis.del('angkatan')
      await redis.set(id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  updateUjisoftStatus: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Angkatan.findOneAndUpdate({ _id: id }, req.body, { rawResult: true, new: true })
      if (target.value === null) {
        throw { status: 404, message: 'Angkatan Tidak Ditemukan' }
      }
      await redis.del('angkatan')
      await redis.set(id, JSON.stringify(target.value))
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
  delete: async (req, res, next) => {
    try {
      let { id } = req.params
      let target = await Angkatan.findOneAndDelete({ _id: id }, { rawResult: true })
      if (target.value === null) {
        throw { status: 404, message: 'Angkatan Tidak Ditemukan' }
      }
      const foundRedis = await redis.get(id)
      await redis.del('angkatan')
      if (foundRedis) {
        console.log('foundredis')
        await redis.del(id)
      }
      res.status(200).json(target)
    } catch (err) {
      next(err)
    }
  },
}
