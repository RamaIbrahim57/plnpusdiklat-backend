
const https = require("https")
const app = require('./app.js')
const fs = require("fs");

const PORT = 5000;

https.createServer({
    key: fs.readFileSync('nginx-selfsignedpriv.pem'),
    cert: fs.readFileSync('nginx-selfsigned.pem')
  }, app).listen(PORT, function () {
    console.log(`🚀 Server ready at https://plnstaging.digitala.id/api`)
  })