const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const adminPtSchema = new Schema({
    nip: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true,
    },
    password: {
        type: Schema.Types.ObjectId,
        ref: "Password",
    },
}, {
    timestamps: true,
});

// Model
const AdminPt = mongoose.model('Admin_pt', adminPtSchema);

module.exports = AdminPt;