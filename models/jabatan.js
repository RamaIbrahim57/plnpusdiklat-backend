const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const jabatanSchema = new Schema({
    nama: {
        type: String,
        required: true,
    },
    okupasi_id: {
        type: Schema.Types.ObjectId,
        ref: 'Okupasi',
        required: true,
    },
    jabatan_minimal_pilihan: {
        type: Number,
        required: true,
    },
    superadmin_id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Superadmin',
    },
}, {
    timestamps: true,
});

// Model
const Jabatan = mongoose.model('Jabatan', jabatanSchema);

module.exports = Jabatan;