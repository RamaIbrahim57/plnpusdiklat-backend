const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const ujisoftSchema = new Schema({
    indikator: {
        type: Schema.Types.String,
        required: true,
    },
    judulAspek: {
        type: Schema.Types.String,
    },
    aspek: {
        type: Schema.Types.String,
        required: true,
    },
    bidang_id: {
        type: Schema.Types.ObjectId,
        ref: 'Bidang',
        required: true,
    }
});

// Model
const Ujisoft = mongoose.model('Ujisoft', ujisoftSchema);

module.exports = Ujisoft;