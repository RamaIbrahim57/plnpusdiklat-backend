const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const lembarObservasiSchema = new Schema({
    nomor: {
        type: Schema.Types.Number,
    },
    aspek: {
        type: Schema.Types.String,
        required: true,
    },
    rawData: {
        type: Schema.Types.String,
        required: true,
    },
});

// Model
const LembarObservasi = mongoose.model('Lembar_observasi', lembarObservasiSchema);

module.exports = LembarObservasi;