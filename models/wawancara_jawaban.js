const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const wawancaraJawabanSchema = new Schema({
    jawaban: {
        type: Schema.Types.String,
        required: true,
    },
    efektifitas: {
        type: String,
        required: true,
    },
    wawancara_pertanyaan: {
        type: Schema.Types.ObjectId,
        ref: 'Wawancara_pertanyaan',
        required: true,
    },
    psikolog_id: {
        type: Schema.Types.ObjectId,
        ref: 'Psikolog',
        required: true,
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
        required: true,
    },
});

// Model
const WawancaraJawaban = mongoose.model('Wawancara_jawaban', wawancaraJawabanSchema);

module.exports = WawancaraJawaban;