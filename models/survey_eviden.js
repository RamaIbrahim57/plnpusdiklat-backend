const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const surveyEvidenSchema = new Schema({
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
    eviden: {
        type: Schema.Types.ObjectId,
        ref: 'File'
    },
}, {
    timestamps: true,
});

// Model
const SurveyEviden = mongoose.model('Survey_eviden', surveyEvidenSchema);

module.exports = SurveyEviden;