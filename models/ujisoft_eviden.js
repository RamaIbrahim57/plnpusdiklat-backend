const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const ujisoftEvidenSchema = new Schema({
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
    eviden: {
        type: Schema.Types.ObjectId,
        ref: 'File'
    },
    lembar_observasi_id: {
        type: Schema.Types.ObjectId,
        ref: 'Lembar_observasi_eviden'
    },
    wawancara_id: {
        type: Schema.Types.ObjectId,
        ref: 'Wawancara_eviden'
    },
}, {
    timestamps: true,
});

// Model
const UjisoftEviden = mongoose.model('Ujisoft_eviden', ujisoftEvidenSchema);

module.exports = UjisoftEviden;