const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const updlSchema = new Schema({
    nip: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true,
    },
    unit: {
        type: String,
        required: true,
    },
    password: {
        type: Schema.Types.ObjectId,
        ref: "Password",
    },
}, {
    timestamps: true,
});

// Model
const Updl = mongoose.model('Updl', updlSchema);

module.exports = Updl;