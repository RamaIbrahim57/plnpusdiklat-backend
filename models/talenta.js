const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const talentaSchema = new Schema({
    nip: {
        type: String,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: Schema.Types.ObjectId,
        ref: "Password",
    },
}, {
    timestamps: true,
});

// Model
const Talenta = mongoose.model('Talenta', talentaSchema);

module.exports = Talenta;