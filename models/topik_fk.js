const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const topikFkSchema = new Schema({
    topik_name: {
        type: String,
        required: true,
    },
    topik_tanggal: {
        type: Date,
    },
    topik_deskripsi: {
        type: String,
        required: true,
    },
    angkatan_id: {
        type: Schema.Types.ObjectId,
        ref: 'Angkatan',
        required: true,
    },
}, {
    timestamps: true,
});

// Model
const TopikFK = mongoose.model('Topik_fk', topikFkSchema);

module.exports = TopikFK;