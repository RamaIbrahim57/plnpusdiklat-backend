const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const rabUpdlSchema = new Schema({
    updl_id: {
        type: Schema.Types.ObjectId,
        ref: 'Updl',
        required: true,
    },
    rab_id: {
        type: Schema.Types.ObjectId,
        ref: 'Rab',
        required: true,
    },
}, {
    timestamps: true,
});

// Model
const RabUpdl = mongoose.model('Rab_updl', rabUpdlSchema);

module.exports = RabUpdl