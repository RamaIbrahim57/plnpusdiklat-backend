const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const bidangSchema = new Schema({
    nama: {
        type: String,
        required: true,
    },
    superadmin_id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Superadmin',
    },
}, {
    timestamps: true,
});

// Model
const Bidang = mongoose.model('Bidang', bidangSchema);

module.exports = Bidang;