const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Aktifitas = require('./aktifitas')

// Schema
const kompetensiSchema = new Schema({
    nama_unit: {
        type: String,
        required: true,
    },
    kode_unit: {
        type: String,
        required: true,
    },
    jumlah_aktifitas: {
        type: Number,
    },
    superadmin_id: {
        type: Schema.Types.ObjectId,
        ref: 'Superadmin',
        required: true,
    },
    jabatan_id: {
        type: Schema.Types.ObjectId,
        ref: 'Jabatan',
    },
    reserved: {
        type: Schema.Types.Boolean,
    },
}, {
    timestamps: true,
});

kompetensiSchema.pre('remove', function(next) {
    // 'this' is the client being removed. Provide callbacks here if you want
    // to be notified of the calls' result.
    Aktifitas.remove({kompetensi_id: this._id}).exec();
    next();
});

// Model
const Kompetensi = mongoose.model('Kompetensi', kompetensiSchema);

module.exports = Kompetensi;