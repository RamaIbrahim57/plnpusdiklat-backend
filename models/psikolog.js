const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const psikologSchema = new Schema({
    nip: {
        type: String,
        required: true,
        unique: true,
    },
    nama: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: Schema.Types.ObjectId,
        ref: "Password",
    },
}, {
    timestamps: true,
});

// Model
const Psikolog = mongoose.model('Psikolog', psikologSchema);

module.exports = Psikolog;