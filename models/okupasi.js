const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const okupasiSchema = new Schema({
    nama: {
        type: String,
        required: true,
    },
    bidang: {
        type: Schema.Types.ObjectId,
        ref: 'Bidang',
        required: true,
    },
    superadmin_id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Superadmin',
    },
}, {
    timestamps: true,
});

// Model
const Okupasi = mongoose.model('Okupasi', okupasiSchema);

module.exports = Okupasi;