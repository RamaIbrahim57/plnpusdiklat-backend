const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const lembarObservasiPertanyaanSchema = new Schema({
    nomor: {
        type: Schema.Types.Number,
    },
    pertanyaan: {
        type: Schema.Types.String,
        required: true,
    },
    rawData: {
        type: Schema.Types.String,
        required: true,
    },
    ujisoft_id: {
        type: Schema.Types.ObjectId,
        ref: 'Ujisoft',
        required: true,
    }
});

// Model
const LembarObservasiPertanyaan = mongoose.model('Lembar_observasi_pertanyaan', lembarObservasiPertanyaanSchema);

module.exports = LembarObservasiPertanyaan;