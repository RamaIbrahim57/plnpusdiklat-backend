const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const aktifitasSchema = new Schema({
    kodeunit_no: {
        type: String,
    },
    kompetensi_id: {
        type: Schema.Types.ObjectId,
        ref: 'Kompetensi',
    },
    elemen_kompetensi: {
        type: String,
    },
    kuk: {
        type: String,
    },
    superadmin_id: {
        type: Schema.Types.ObjectId,
        ref: 'Superadmin',
    },
    
}, {
    timestamps: true,
});

// Model
const Aktifitas = mongoose.model('Aktifitas', aktifitasSchema);

module.exports = Aktifitas;