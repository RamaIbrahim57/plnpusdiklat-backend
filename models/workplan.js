const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const realisasiSchema = new Schema({
    eviden: {
        type: Schema.Types.ObjectId,
        ref: 'File',
        required: true,
    },
    keterangan: {
        type: String,
        required: true,
    }
});

const workplanSchema = new Schema({
    aktifitas_id: {
        type: Schema.Types.ObjectId,
        ref: 'Aktifitas',
        required: true,
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
        required: true,
    },
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor2',
        required: true,
    },
    realisasi: {
        type: { realisasiSchema },
    },
    tanggalmulai: {
        type: Schema.Types.Date,
    },
    tanggalakhir: {
        type: Schema.Types.Date,
    },
    status_workplan: {
        type: Schema.Types.String,
    },
    tanggal_realisasi: {
        type: Schema.Types.Date
    },
    status_realisasi: {
        type: Schema.Types.String,
    },
    workplan: {
        type: String,
    },
    feedback_workplan: {
        type: String,
    },
    feedback_realisasi: {
        type: String,
    },
}, {
    timestamps: true,
});

// Model
const Workplan = mongoose.model('Workplan', workplanSchema);

module.exports = Workplan;