const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs');
const Schema = mongoose.Schema;

// Schema
const passwordDataSchema = new Schema({
    password: {
        type: String,
        required: true,
    },
});

// bcrypt
passwordDataSchema.pre('save', async function () {
    if (this.isModified('password')) {
        this.password = await bcryptjs.hash(this.password, 12)
    }
})

passwordDataSchema.methods.matchesPassword = function (password) {
    return bcryptjs.compare(password, this.password)
}

// Model
const Password_Data = mongoose.model('Password_data', passwordDataSchema);

module.exports = Password_Data;