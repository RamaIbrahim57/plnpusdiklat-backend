const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const topikPbSchema = new Schema({
    topik_name: {
        type: String,
        required: true,
    },
    topik_tanggal: {
        type: Date,
    },
    topik_deskripsi: {
        type: String,
        required: true,
    },
    bidang_id: {
        type: Schema.Types.ObjectId,
        ref: 'Bidang',
    },
    updl_id: {
        type: Schema.Types.ObjectId,
        ref: 'Updl',
        required: true,
    },
    angkatan_id: {
        type: Schema.Types.ObjectId,
        ref: 'Angkatan',
        required: true,
    },
}, {
    timestamps: true,
});

// Model
const TopikPB = mongoose.model('Topik_pb', topikPbSchema);

module.exports = TopikPB;