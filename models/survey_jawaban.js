const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const surveyJawabanSchema = new Schema({
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
    choice: {
        type: String,
    },
    jawaban: {
        type: String,
        required: true,
    },
    survey_id: {
        type: Schema.Types.ObjectId,
        ref: 'Survey',
    },
    subject: {
        type: String,
    },
}, {
    timestamps: true,
});

// Model
const SurveyJawaban = mongoose.model('Survey_jawaban', surveyJawabanSchema);

module.exports = SurveyJawaban