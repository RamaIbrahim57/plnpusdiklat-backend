const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const superAdminSchema = new Schema({
    nip: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true,
    },
    role: {
        type: String,
        required: true,
    },
    password: {
        type: Schema.Types.ObjectId,
        ref: "Password",
    },
}, {
    timestamps: true,
});

// Model
const SuperAdmin = mongoose.model('Superadmin', superAdminSchema);

module.exports = SuperAdmin;