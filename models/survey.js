const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const surveySchema = new Schema({
    question: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
    },
    jabatan_id: {
        type: Schema.Types.ObjectId,
        ref: 'Jabatan',
        required: true,
    },
    nomor: {
        type: String,
    },
}, {
    timestamps: true,
});

// Model
const Survey = mongoose.model('Survey', surveySchema);

module.exports = Survey