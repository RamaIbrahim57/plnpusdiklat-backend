const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const angkatanSchema = new Schema({
    angkatan_name: {
        type: String,
        required: true,
        unique: true,
    },
    angkatan_year: {
        type: Number,
        required: true,
    },
    program: {
        type: String,
        required: true,
    },
    timeline_fk: {
        type: String,
        required: true,
    },
    timeline_pp: {
        type: String,
        required: true,
    },
    timeline_pb: {
        type: String,
        required: true,
    },
    timeline_ojt: {
        type: String,
        String: true,
    },
    timeline_ujiakhir: {
        type: String,
        required: true,
    },
    ujisoft_status: {
        type: String,
    },
    ujisoft_mulai: {
        type: Date,
    },
    ujisoft_akhir: {
        type: Date,
    },
    status_angkatan: {
        type: String,
    },
}, {
    timestamps: true,
});

// Model
const Angkatan = mongoose.model('Angkatan', angkatanSchema);

module.exports = Angkatan;