const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const probisStatusSchema = new Schema({
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
        required: true,
    },
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor',
        required: true,
    },
    probis_id: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor',
        required: true,
    },
    status: {
        type: Schema.Types.String,
        required: true,
    },
}, {
    timestamps: true,
});

// Model
const ProbisStatus = mongoose.model('Probis_status', probisStatusSchema);

module.exports = ProbisStatus;