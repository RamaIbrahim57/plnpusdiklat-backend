const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const wawancaraEvidenSchema = new Schema({
    psikolog_id: {
        type: Schema.Types.ObjectId,
        ref: 'Psikolog',
    },
    eviden: {
        type: Schema.Types.ObjectId,
        ref: 'File'
    },
}, {
    timestamps: true,
});

// Model
const WawancaraEviden = mongoose.model('Wawancara_eviden', wawancaraEvidenSchema);

module.exports = WawancaraEviden;