const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const ujisoftStatusSchema = new Schema({
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
    lembarobservasi_id: {
        type: Schema.Types.ObjectId,
        ref: 'Lembar_observasi_jawaban',
    },
    wawancara_id: {
        type: Schema.Types.ObjectId,
        ref: 'Wawancara_jawaban',
    },
    jawaban: {
        type: String,
    },
    // psikolog_id: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'Psikolog',
    // },
    ujisoft_id: {
        type: Schema.Types.ObjectId,
        ref: 'Ujisoft_aspek',
    },
    status: {
        type: String,
    },
    eviden: {
        type: Schema.Types.ObjectId,
        ref: 'File'
    },
}, {
    timestamps: true,
});

// Model
const UjisoftJawaban = mongoose.model('Ujisoft_jawaban', ujisoftStatusSchema);

module.exports = UjisoftJawaban;