const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const workplanStatusSchema = new Schema({
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor',
    },
    workplan_id: {
        type: Schema.Types.ObjectId,
        ref: 'Workplan',
    },
    status_workplan: {
        type: Schema.Types.String,
    },
    status_realisasi: {
        type: Schema.Types.Date,
    },
}, {
    timestamps: true,
});

// Model
const WorkplanStatus = mongoose.model('WorkplanStatus', workplanStatusSchema);

module.exports = WorkplanStatus;