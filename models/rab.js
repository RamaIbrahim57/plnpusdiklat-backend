const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const rabSchema = new Schema({
    rab: {
        type: Number,
        required: true,
    },
    realisasi: {
        type: Number,
        required: true,
    },
    eviden: {
        type: Schema.Types.ObjectId,
        ref: 'File',
        required: true,
    },
    tahapan: {
        type: Schema.Types.String,
        required: true,
    },
    persentase: {
        type: Schema.Types.Number,
        required: true,
    },
    angkatan: {
        type: Schema.Types.ObjectId,
        ref: 'Angkatan',
        required: true,
    },
    updl: {
        type: Schema.Types.String,
    },
}, {
    timestamps: true,
});

// Model
const Rab = mongoose.model('Rab', rabSchema);

module.exports = Rab