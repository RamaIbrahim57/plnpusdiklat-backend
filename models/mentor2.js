const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const mentorSchema = new Schema({
    nip: {
        type: String,
        required: true,
        unique: true,
    },
    program: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    role: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    hp1: {
        type: String,
        required: true,
    },
    hp2: {
        type: String,
    },
    password: {
        type: Schema.Types.ObjectId,
        ref: "Password",
    },
}, {
    timestamps: true,
});

// Model
const Mentor = mongoose.model('Mentor2', mentorSchema);

module.exports = Mentor;