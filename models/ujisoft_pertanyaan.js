const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const ujisoftAspekSchema = new Schema({
    nomor: {
        type: Schema.Types.Number,
    },
    pertanyaan: {
        type: Schema.Types.String,
        required: true,
    },
    rawData: {
        type: Schema.Types.String,
        required: true,
    },
    ujisoft_id: {
        type: Schema.Types.ObjectId,
        ref: 'Ujisoft',
        required: true,
    }
});

// Model
const UjisoftAspek = mongoose.model('Ujisoft_aspek', ujisoftAspekSchema);

module.exports = UjisoftAspek;