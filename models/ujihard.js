const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const ujihardSchema = new Schema({
    eks_summary: {
        type: Schema.Types.ObjectId,
        ref: 'File',
        required: true,
    },
    ppt: {
        type: Schema.Types.ObjectId,
        ref: 'File',
        required: true,
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
        required: true,
    },
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor',
        required: true,
    },
    status: {
        type: Schema.Types.String,
    },
    penguji_id: {
        type: Schema.Types.ObjectId,
        ref: 'Penguji',
    },
    nilai_sertifikasi_kategori_1: {
        type: Schema.Types.Number,
    },
    nilai_sertifikasi_kategori_2: {
        type: Schema.Types.Number,
    },
    nilai_sertifikasi_kategori_3: {
        type: Schema.Types.Number,
    },
    sertifikasi_keterangan: {
        type: String,
    },
    nonsertifikasi_keterangan: {
        type: String,
    },
    status_penilaian_sertifikasi: {
        type: Schema.Types.String,
    },
    nilai_nonsertifikasi_kategori_1: {
        type: Schema.Types.Number,
    },
    nilai_nonsertifikasi_kategori_2: {
        type: Schema.Types.Number,
    },
    nilai_nonsertifikasi_kategori_3: {
        type: Schema.Types.Number,
    },
    status_penilaian_nonsertifikasi: {
        type: Schema.Types.String,
    },
    feedback: { 
        type: Schema.Types.String,
    },
    tanggal_uji: {
        type: Schema.Types.Date,
    },
    penguji_status: {
        type: Schema.Types.String,
    },
    tanggal: {
        type: Schema.Types.Date,
        required: true,
    },
}, {
    timestamps: true,
});

// Model
const Ujihard = mongoose.model('Ujihard', ujihardSchema);

module.exports = Ujihard;