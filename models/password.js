const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs');
const Schema = mongoose.Schema;

// Schema
const passwordDataSchema = new Schema({
    password: {
        type: String,
        required: true,
    },
});

const passwordSchema = new Schema({
    password: {
        type: String,
        required: true,
    },
    history: {
        type: [ passwordDataSchema ],
    },
}, {
    timestamps: true,
});

// bcrypt, middleware
passwordSchema.pre('save', async function () {
    if (this.isModified('password')) {
        this.password = await bcryptjs.hash(this.password, 12)
        this.history.push({password: this.password})
    }
})

passwordSchema.methods.matchesPassword = function (password) {
    return bcryptjs.compare(password, this.password)
}

// Model
const Password = mongoose.model('Password', passwordSchema);

module.exports = Password;