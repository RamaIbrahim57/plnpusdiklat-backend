const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const wawancaraIndikatorSchema = new Schema({
    question: {
        type: String,
        required: true,
    },
    efektifitas: {
        type: String,
        required: true,
    },
    rawData: {
        type: String,
        required: true,
    },
}, {
    timestamps: true,
});

// Model
const WawancaraIndikator = mongoose.model('Wawancara_indikator', wawancaraIndikatorSchema);

module.exports = WawancaraIndikator