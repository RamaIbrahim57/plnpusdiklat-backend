const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const lembarObservasiJawabanSchema = new Schema({
    jawaban: {
        type: Schema.Types.String,
        required: true,
    },
    efektifitas: {
        type: String,
        required: true,
    },
    observasi_pertanyaan: {
        type: Schema.Types.ObjectId,
        ref: 'Lembar_observasi_pertanyaan',
        required: true,
    },
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor2',
        required: true,
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
        required: true,
    },
});

// Model
const LembarObservasiJawaban = mongoose.model('Lembar_observasi_jawaban', lembarObservasiJawabanSchema);

module.exports = LembarObservasiJawaban;