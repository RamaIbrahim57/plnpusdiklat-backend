const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const pengujiSchema = new Schema({
    nip: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true,
    },
    hp1: {
        type: String,
        required: true,
    },
    unit: {
        type: String,
        required: true,
    },
    bidang: {
        type: String,
        required: true,
    },
    password: {
        type: Schema.Types.ObjectId,
        ref: "Password",
    },
}, {
    timestamps: true,
});

// Model
const Penguji = mongoose.model('Penguji', pengujiSchema);

module.exports = Penguji;