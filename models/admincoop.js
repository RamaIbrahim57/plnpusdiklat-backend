const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const adminCoopSchema = new Schema({
    nip: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true,
    },
    password: {
        type: Schema.Types.ObjectId,
        ref: "Password",
    },
}, {
    timestamps: true,
});

// Model
const AdminCoop = mongoose.model('Admin_coop', adminCoopSchema);

module.exports = AdminCoop;