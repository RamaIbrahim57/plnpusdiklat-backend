const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const ujisoftSchema = new Schema({
    nomor: {
        type: Schema.Types.Number,
    },
    aspek: {
        type: Schema.Types.String,
        required: true,
    },
    rawData: {
        type: Schema.Types.String,
        required: true,
    },
});

// Model
const Ujisoft = mongoose.model('Ujisoft', ujisoftSchema);

module.exports = Ujisoft;