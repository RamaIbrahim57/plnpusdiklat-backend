const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const fileSchema = new Schema({
    type: {
        type: String,
        required: true,
    },
    path: {
        type: String,
        required: true,
    },
}, {
    timestamps: true,
});

// Model
const File = mongoose.model('File', fileSchema);

module.exports = File;