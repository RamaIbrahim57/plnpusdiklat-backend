const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const FkNilaiSchema = new Schema({
    nilai: {
        type: String,
    },
    topik_fk: {
        type: Schema.Types.ObjectId,
        ref: 'Topik_fk',
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: "Siswa",
    },
    topik_resume: {
        type: Schema.Types.String,
        required: true,
    },
    superadmin_id: {
        type: Schema.Types.ObjectId,
        ref: "Superadmin",
    },
}, {
    timestamps: true,
});

// Model
const FkNilai = mongoose.model('Fk_nilai', FkNilaiSchema);

module.exports = FkNilai;