const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const laporanHarianSchema = new Schema({
    nama_unit: {
        type: String,
    },
    hari_ojt: {
        type: String,
        required: true,
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor2',
    },
    status: {
        type: String,
    },
    tanggal: {
        type: Schema.Types.Date,
    },
    feedback: {
        type: Schema.Types.String,
    },
    eviden: {
        type: Schema.Types.ObjectId,
        ref: 'File',
        required: true,
    },
}, {
    timestamps: true,
});

// Model
const LaporanHarian = mongoose.model('Laporan_harian', laporanHarianSchema);

module.exports = LaporanHarian;