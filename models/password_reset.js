const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs');
const Schema = mongoose.Schema;

// Schema
const passwordResetSchema = new Schema({
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor',
    },
    penguji_id: {
        type: Schema.Types.ObjectId,
        ref: 'Penguji',
    },
    psikolog_id: {
        type: Schema.Types.ObjectId,
        ref: 'Psikolog',
    },
    updl_id: {
        type: Schema.Types.ObjectId,
        ref: 'Updl',
    },
    superadmin_id: {
        type: Schema.Types.ObjectId,
        ref: 'Superadmin',
    },
});

// Model
const PasswordReset = mongoose.model('Password_reset', passwordResetSchema);

module.exports = PasswordReset;