const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcryptjs = require('bcryptjs')
const PengajuanGantiMentor = require('./pengajuan_ganti_mentor')
const FkNilai = require('./fk_nilai')
const Ujihard = require('./ujihard')

// Schema
const siswaSchema = new Schema({
    nik: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
    },
    password: {
        type: Schema.Types.ObjectId,
        ref: 'Password',
    },
    notest: {
        type: String,
        required: true,
    },
    asal_rekrutmen: {
        type: String,
        required: true,
    },
    mentor2: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor2',
    },
    mentor1: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor1',
    },
    jabatan: {
        type: Schema.Types.ObjectId,
        ref: 'Jabatan'
    },
    nama: {
        type: String,
        required: true,
    },
    jenjang: {
        type: String,
        required: true,
    },
    jurusan: {
        type: String,
    },
    angkatan_id: {
        type: Schema.Types.ObjectId,
        ref: 'Angkatan',
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
    },
    role: {
        type: String,
        required: true,
    },
    univ: {
        type: String,
        required: true,
    },
    hp1: {
        type: String,
        required: true,
    },
    fase: {
        type: String,
        required: true,
    },
    hp2: {
        type: String,
    },
    gender: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    tempatlahir: {
        type: String,
    },
    nem: {
        type: String,
    },
    agama: {
        type: String,
        required: true
    },
    tanggallahir: {
        type: String,
    },
    gelar: {
        type: String,
    },
    tahunmasuk: {
        type: String,
    },
    tahunlulus: {
        type: String,
    },
    asalsd: {
        type: String,
    },
    tahunmasuksd: {
        type: String,
    },
    tahunlulussd: {
        type: String,
    },
    asalsmp: {
        type: String,
    },
    tahunmasuksmp: {
        type: String,
    },
    tahunlulussmp: {
        type: String,
    },
    asalsma: {
        type: String,
    },
    tahunmasuksma: {
        type: String,
    },
    tahunlulussma: {
        type: String,
    },
    ukuranbaju: {
        type: String,
    },
    ukurancelana: {
        type: String,
    },
    ukuransepatu: {
        type: String,
    },
    penempatan: {
        type: String,
    },
    program: {
        type: String,
        required: true,
    },
    fk: {
        type: Number,
    },
    pp: {
        type: Number,
    },
    pb: {
        type: Number,
    },
    uji_akhir: {
        type: String,
    },
    hasil: {
        type: String,
    },
    tahap: {
        type: String,
    },
    updl: {
        type: Schema.Types.ObjectId,
        ref: 'Updl',
    },
    updl_pb: {
        type: Schema.Types.ObjectId,
        ref: 'Updl',
    },
    penguji: {
        type: Schema.Types.ObjectId,
        ref: 'Penguji',
    },
}, {
    timestamps: true,
});

siswaSchema.methods.matchesPassword = function (password) {
    return bcryptjs.compare(password, this.password)
}

siswaSchema.pre('remove', function(next) {
    // 'this' is the client being removed. Provide callbacks here if you want
    // to be notified of the calls' result.
    PengajuanGantiMentor.remove({siswa_id: this._id}).exec();
    FkNilai.remove({siswa_id: this._id}).exec();
    Ujihard.remove({siswa_id: this._id}).exec();
    next();
});

// Model
const Siswa = mongoose.model('Siswa', siswaSchema);

module.exports = Siswa;