const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const mentorSchema = new Schema({
    nip: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    name: {
        type: String,
        required: true,
    },
    program: {
        type: String,
        required: true,
    },
    hp1: {
        type: String,
        required: true,
    },
    hp2: {
        type: String,
    },
}, {
    timestamps: true,
});

// Model
const Mentor = mongoose.model('Mentor1', mentorSchema);

module.exports = Mentor;