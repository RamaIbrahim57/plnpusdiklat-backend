const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const PbNilaiSchema = new Schema({
    nilai: {
        type: String,
    },
    topik_pb: {
        type: Schema.Types.ObjectId,
        ref: 'Topik_pb',
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: "Siswa",
    },
    topik_resume: {
        type: Schema.Types.String,
        required: true,
    },
    superadmin_id: {
        type: Schema.Types.ObjectId,
        ref: "Superadmin",
    },
}, {
    timestamps: true,
});

// Model
const PbNilai = mongoose.model('Pb_nilai', PbNilaiSchema);

module.exports = PbNilai;