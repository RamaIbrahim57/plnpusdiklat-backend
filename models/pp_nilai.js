const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const PpNilaiSchema = new Schema({
    nilai: {
        type: String,
    },
    topik_pp: {
        type: Schema.Types.ObjectId,
        ref: 'Topik_pp',
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: "Siswa",
    },
    topik_resume: {
        type: Schema.Types.String,
        required: true,
    },
    superadmin_id: {
        type: Schema.Types.ObjectId,
        ref: "Superadmin",
    },
}, {
    timestamps: true,
});

// Model
const PpNilai = mongoose.model('Pp_nilai', PpNilaiSchema);

module.exports = PpNilai;