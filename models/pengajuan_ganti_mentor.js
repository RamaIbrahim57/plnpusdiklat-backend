const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const pengajuanGantiMentorSchema = new Schema({
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
    mentor2_lama: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor2',
    },
    mentor1_lama: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor1',
    },
    mentor2_baru: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor2',
    },
    mentor1_baru: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor1',
    },
    eviden: {
        type: Schema.Types.ObjectId,
        ref: 'File',
        required: true,
    },
    tanggal: {
        type: Schema.Types.Date,
    },
    status: {
        type: Schema.Types.String,
    },
}, {
    timestamps: true,
});

// Model
const PengajuanGantiMentor = mongoose.model('Pengajuan_ganti_mentor', pengajuanGantiMentorSchema);

module.exports = PengajuanGantiMentor