const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const lembarObservasiEvidenSchema = new Schema({
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor2',
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
    eviden: {
        type: Schema.Types.ObjectId,
        ref: 'File'
    },
}, {
    timestamps: true,
});

// Model
const LembarObservasiEviden = mongoose.model('Lembar_observasi_eviden', lembarObservasiEvidenSchema);

module.exports = LembarObservasiEviden;