const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const ujihardStatusSchema = new Schema({
    ujihard_id: {
        type: Schema.Types.ObjectId,
        ref: 'Ujihard',
        required: true,
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
        required: true,
    },
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'Mentor',
        required: true,
    },
    status: {
        type: Schema.Types.String,
    },
    tanggal_uji: {
        type: Schema.Types.Date,
    },
    penguji_status: {
        type: Schema.Types.String,
    },
    penguji_id: {
        type: Schema.Types.ObjectId,
        ref: 'Penguji',
    },
}, {
    timestamps: true,
});

// Model
const UjihardStatus = mongoose.model('Ujihard_status', ujihardStatusSchema);

module.exports = UjihardStatus;