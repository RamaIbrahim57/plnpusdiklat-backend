const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const wawancaraPertanyaanSchema = new Schema({
    nomor: {
        type: Schema.Types.Number,
    },
    pertanyaan: {
        type: Schema.Types.String,
        required: true,
    },
    rawData: {
        type: Schema.Types.String,
        required: true,
    },
    wawancara_id: {
        type: Schema.Types.ObjectId,
        ref: 'Wawancara',
        required: true,
    }
});

// Model
const WawancaraPertanyaan = mongoose.model('Wawancara_pertanyaan', wawancaraPertanyaanSchema);

module.exports = WawancaraPertanyaan;