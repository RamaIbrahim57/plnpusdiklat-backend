const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const kompetensiJabatanSchema = new Schema({
    kompetensi_id: {
        type: Schema.Types.ObjectId,
        ref: 'Kompetensi',
    },
    jabatan_id: {
        type: Schema.Types.ObjectId,
        ref: 'Jabatan',
    },
    tipe: {
        type: Schema.Types.String,
    },
}, {
    timestamps: true,
});

// Model
const KompetensiJabatan = mongoose.model('Kompetensi_jabatan', kompetensiJabatanSchema);

module.exports = KompetensiJabatan;