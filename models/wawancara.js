const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const wawancaraSchema = new Schema({
    judulAspek: {
        type: String,
        required: true,
    },
    aspek: {
        type: String,
        required: true,
    },
}, {
    timestamps: true,
});

// Model
const Wawancara = mongoose.model('Wawancara', wawancaraSchema);

module.exports = Wawancara