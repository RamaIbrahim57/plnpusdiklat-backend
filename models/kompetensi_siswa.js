const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const kompetensiSiswaSchema = new Schema({
    kompetensi_id: {
        type: Schema.Types.ObjectId,
        ref: 'Kompetensi',
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'Siswa',
    },
}, {
    timestamps: true,
});

// Model
const KompetensiSiswa = mongoose.model('Kompetensi_siswa', kompetensiSiswaSchema);

module.exports = KompetensiSiswa;