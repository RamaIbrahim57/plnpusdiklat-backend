const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema
const probisSchema = new Schema({
    hari_ojt: {
        type: Schema.Types.Number,
        required: true,
    },
    mulai: {
        type: Schema.Types.Date,
        required: true,
    },
    selesai: {
        type: Schema.Types.Date,
        required: true,
    },
    lokasi: {
        type: String,
        required: true,
    },
    penjelasan: {
        type: String,
        required: true,
    },
    eviden: {
        type: Schema.Types.ObjectId,
        ref: 'File',
        required: true,
    },
    siswa_id: {
        type: Schema.Types.ObjectId,
        ref: 'File',
        required: true,
    },
    mentor_id: {
        type: Schema.Types.ObjectId,
        ref: 'File',
        required: true,
    },
    status: {
        type: Schema.Types.String,
        // required: true,
    },
    feedback: {
        type: Schema.Types.String,
    },
    bidang: {
        type: Schema.Types.String,
    },
}, {
    timestamps: true,
});

// Model
const Probis = mongoose.model('Probis', probisSchema);

module.exports = Probis;