const express = require('express');
const router = express.Router();
const UjiHardController = require('../../controllers/ujiHardController')
const { siswa, mentor, updl, penguji } = require('../../middlewares/authentication')

router.get('/api/ujihard/:id', UjiHardController.findOne)
router.get('/api/ujihard/siswa/:id', UjiHardController.findBySiswa)
router.get('/api/ujihard/penguji/:id', UjiHardController.findByPenguji)
router.get('/api/ujihard', UjiHardController.findAll)
router.post('/api/ujihard/add', siswa, UjiHardController.create)
router.put('/api/ujihard/:id', siswa, UjiHardController.update)
router.put('/api/ujihard/status/:id', mentor, UjiHardController.updateStatus)
router.put('/api/ujihard/penguji/status/:id', penguji, UjiHardController.updateStatusByPenguji)
router.put('/api/ujihard/updl/:id', updl, UjiHardController.updateUPDL)
router.put('/api/ujihard/sertifikasi/:id', penguji, UjiHardController.updatePenilaianSertifikasi)
router.put('/api/ujihard/nonsertifikasi/:id', penguji, UjiHardController.updatePenilaianNonSertifikasi)
router.put('/api/ujihard/tolakpenguji/:id', penguji, UjiHardController.tolakPenguji)
router.put('/api/ujihard/setujuipenguji/:id', penguji, UjiHardController.setujuiPenguji)
router.delete('/api/ujihard/:id', siswa, UjiHardController.delete)

module.exports = router