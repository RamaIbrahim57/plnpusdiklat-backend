const express = require('express');
const router = express.Router();
const KompetensiJabatanController = require('../../controllers/kompetensiJabatanController');
const { siswa, superadmin } = require('../../middlewares/authentication')

router.get('/api/kompetensiJabatan/:id', KompetensiJabatanController.findOne)
router.get('/api/kompetensiJabatan', KompetensiJabatanController.findAll)
router.get('/api/kompetensiJabatan/jabatan/:id', KompetensiJabatanController.findByJabatan)
router.post('/api/kompetensiJabatan/add', superadmin, KompetensiJabatanController.create)
router.put('/api/kompetensiJabatan/:id', superadmin, KompetensiJabatanController.update)
router.delete('/api/kompetensiJabatan/:id', superadmin, KompetensiJabatanController.delete)

module.exports = router