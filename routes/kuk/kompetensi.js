const express = require('express');
const router = express.Router();
const KompetensiController = require('../../controllers/kompetensiController');
const { superadmin, superadminandcoop } = require('../../middlewares/authentication')

router.get('/api/kompetensi/:id', KompetensiController.findOne)
router.get('/api/kompetensi/kodeunit/:id', KompetensiController.findByKodeUnit)
router.get('/api/kompetensi/search/:id', KompetensiController.findAllByKodeUnit)
router.get('/api/kompetensi', KompetensiController.findAll)
router.get('/api/countKompetensi', KompetensiController.countData)
router.get('/api/kompetensiavailable', KompetensiController.findWithoutJabatan)
router.get('/api/kompetensi/tipe/:tipe', KompetensiController.findByTipe)
// router.get('/api/jabatan/okupasi/:id', JabatanController.findByOkupasi)
router.post('/api/kompetensi/add', superadminandcoop, KompetensiController.create)
router.put('/api/kompetensi/:id', superadminandcoop, KompetensiController.update)
router.delete('/api/kompetensi/:id', superadminandcoop, KompetensiController.delete)

module.exports = router