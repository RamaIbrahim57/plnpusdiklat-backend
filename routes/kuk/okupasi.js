const express = require('express');
const router = express.Router();
const OkupasiController = require('../../controllers/okupasiController')
const { superadmin, superadminandcoop } = require('../../middlewares/authentication')

router.get('/api/okupasi/:id', OkupasiController.findOne)
router.get('/api/okupasi/bidang/:id', OkupasiController.findByBidang)
router.post('/api/okupasi/nama', OkupasiController.findByNamaAndBidang)
router.get('/api/okupasi', OkupasiController.findAll)
router.post('/api/okupasi/add', superadminandcoop, OkupasiController.create)
router.put('/api/okupasi/:id', superadminandcoop, OkupasiController.update)
router.delete('/api/okupasi/:id', superadminandcoop, OkupasiController.delete)

module.exports = router