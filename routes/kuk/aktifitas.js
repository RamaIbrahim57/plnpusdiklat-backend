const express = require('express');
const router = express.Router();
const AktifitasController = require('../../controllers/aktifitasController')
const { superadmin, superadminandcoop } = require('../../middlewares/authentication')

router.get('/api/aktifitas/:id', AktifitasController.findOne)
router.get('/api/aktifitas', AktifitasController.findAll)
router.get('/api/aktifitas/kompetensi/:id', AktifitasController.findByKompetensi)
router.get('/api/aktifitas/nomor/:nomor/:kompetensi', AktifitasController.findByNomorAndKompetensi)
router.post('/api/aktifitas/add', superadminandcoop, AktifitasController.create)
router.put('/api/aktifitas/:id', superadminandcoop, AktifitasController.update)
router.delete('/api/aktifitas/:id', superadminandcoop, AktifitasController.delete)

module.exports = router