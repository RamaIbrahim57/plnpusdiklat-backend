const express = require('express');
const router = express.Router();
const WorkplanController = require('../../controllers/workplanController')
const { siswa, superadmin, mentor, superadminandcoop } = require('../../middlewares/authentication')

router.get('/api/workplan/:id', WorkplanController.findOne)
router.get('/api/workplan', WorkplanController.findAll)
router.get('/api/workplan/aktifitas/:id/:siswa', WorkplanController.findByAktifitas)
router.get('/api/workplan/siswa/:id', WorkplanController.findBySiswa)
router.get('/api/workplan/mentor/:id', WorkplanController.findByMentor)
router.put('/api/workplan/realisasi/:id', siswa, WorkplanController.updateRealisasi)
router.put('/api/workplan/status/realisasi/:id', mentor, WorkplanController.updateStatusRealisasi)
router.put('/api/workplan/status/workplan/:id', mentor, WorkplanController.updateStatusWorkplan)
router.post('/api/workplan/add', siswa, WorkplanController.create)
router.put('/api/workplan/:id', siswa, WorkplanController.update)
router.delete('/api/workplan/:id', siswa, WorkplanController.delete)
router.delete('/api/workplan/siswa/:id', superadmin, WorkplanController.deleteBySiswa)

module.exports = router