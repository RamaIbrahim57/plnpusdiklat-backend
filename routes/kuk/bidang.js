const express = require('express');
const router = express.Router();
const BidangController = require('../../controllers/bidangController')
const { superadmin, superadminandcoop } = require('../../middlewares/authentication')

router.get('/api/bidang/:id', BidangController.findOne)
router.get('/api/bidang', BidangController.findAll)
router.post('/api/bidang/add', superadminandcoop, BidangController.create)
router.put('/api/bidang/:id', superadminandcoop, BidangController.update)
router.delete('/api/bidang/:id', superadminandcoop, BidangController.delete)

module.exports = router