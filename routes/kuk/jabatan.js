const express = require('express');
const router = express.Router();
const JabatanController = require('../../controllers/jabatanController')
const { superadmin, superadminandcoop } = require('../../middlewares/authentication')

router.get('/api/jabatan/:id', JabatanController.findOne)
router.get('/api/jabatan', JabatanController.findAll)
router.get('/api/jabatan/okupasi/:id', JabatanController.findByOkupasi)
router.post('/api/jabatan/nama', JabatanController.findByNamaAndOkupasi)
router.post('/api/jabatan/add', superadminandcoop, JabatanController.create)
router.put('/api/jabatan/:id', superadminandcoop, JabatanController.update)
router.delete('/api/jabatan/:id', superadminandcoop, JabatanController.delete)

module.exports = router