const express = require('express');
const router = express.Router();
const UjiSoftEvidenController = require('../../controllers/ujisoftEvidenController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/ujisoft-eviden/:id', UjiSoftEvidenController.findOne)
router.get('/api/ujisoft-eviden/siswa/:id', UjiSoftEvidenController.findBySiswa)
router.get('/api/ujisoft-eviden', UjiSoftEvidenController.findAll)
router.post('/api/ujisoft-eviden/add', siswa, UjiSoftEvidenController.create)
router.put('/api/ujisoft-eviden/:id', UjiSoftEvidenController.update)
router.delete('/api/ujisoft-eviden/:id', siswa, UjiSoftEvidenController.delete)

module.exports = router