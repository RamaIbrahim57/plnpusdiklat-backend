const express = require('express');
const router = express.Router();
const UjisoftPertanyaanController = require('../../controllers/ujisoftPertanyaanController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/ujisoftpertanyaan/:id', UjisoftPertanyaanController.findOne)
router.get('/api/ujisoftpertanyaan', UjisoftPertanyaanController.findAll)
router.get('/api/ujisoftpertanyaan/aspek/:id', UjisoftPertanyaanController.findByAspek)
router.post('/api/ujisoftpertanyaan/add', superadmin, UjisoftPertanyaanController.create)
router.put('/api/ujisoftpertanyaan/:id', superadmin, UjisoftPertanyaanController.update)
router.delete('/api/ujisoftpertanyaan/:id', superadmin, UjisoftPertanyaanController.delete)

module.exports = router