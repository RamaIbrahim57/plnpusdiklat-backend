const express = require('express');
const router = express.Router();
const SurveyJawabanController = require('../../controllers/surveyJawabanController')
const { siswa, mentorandsiswa, mentor, superadmin, superadminandcoopandtalenta } = require('../../middlewares/authentication')

router.get('/api/surveyJawaban/:id', SurveyJawabanController.findOne)
router.get('/api/surveyJawaban', SurveyJawabanController.findAll)
router.get('/api/surveyJawaban/siswa/:id', SurveyJawabanController.findBySiswa)
router.get('/api/surveyJawaban/survey/:id', SurveyJawabanController.findBySurvey)
router.get('/api/surveyJawaban/mentor/:id', SurveyJawabanController.findByMentor)
router.get('/api/surveyJawaban/mentor/:mentor/:siswa', SurveyJawabanController.findByMentorAndSiswa)
router.post('/api/surveyJawaban/add', mentorandsiswa, SurveyJawabanController.create)
router.put('/api/surveyJawaban/:id', siswa, SurveyJawabanController.update)
router.delete('/api/surveyJawaban/:id', siswa, SurveyJawabanController.delete)
router.delete('/api/surveyJawaban/survey/:id', superadminandcoopandtalenta, SurveyJawabanController.deleteBySurvey)
router.delete('/api/surveyJawaban/siswa/:id', superadminandcoopandtalenta, SurveyJawabanController.deleteBySiswa)

module.exports = router