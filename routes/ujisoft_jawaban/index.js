const express = require('express');
const router = express.Router();
const UjiSoftJawabanController = require('../../controllers/ujisoftJawabanController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/ujisoft-jawaban/:id', UjiSoftJawabanController.findOne)
router.get('/api/ujisoft-jawaban/ujisoft/:id', UjiSoftJawabanController.findByUjisoft)
router.get('/api/ujisoft-jawaban/siswa/:id', UjiSoftJawabanController.findBySiswa)
router.get('/api/ujisoft-jawaban', UjiSoftJawabanController.findAll)
router.post('/api/ujisoft-jawaban/add', siswa, UjiSoftJawabanController.create)
router.put('/api/ujisoft-jawaban/:id', siswa, UjiSoftJawabanController.update)
router.delete('/api/ujisoft-jawaban/:id', siswa, UjiSoftJawabanController.delete)

module.exports = router