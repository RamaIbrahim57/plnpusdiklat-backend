const express = require('express');
const router = express.Router();
const WawancaraJawabanController = require('../../controllers/wawancaraJawabanController')
const { psikolog, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/wawancaraJawaban/:id', WawancaraJawabanController.findOne)
router.get('/api/wawancaraJawaban/pertanyaan/:id', WawancaraJawabanController.findByPertanyaan)
router.get('/api/wawancaraJawaban/psikolog/:id', WawancaraJawabanController.findByPsikolog)
router.get('/api/wawancaraJawaban', WawancaraJawabanController.findAll)
router.post('/api/wawancaraJawaban/add', psikolog, WawancaraJawabanController.create)
router.put('/api/wawancaraJawaban/:id', psikolog, WawancaraJawabanController.update)
router.delete('/api/wawancaraJawaban/:id', psikolog, WawancaraJawabanController.delete)

module.exports = router