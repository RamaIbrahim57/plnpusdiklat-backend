const express = require('express');
const router = express.Router();
const RabController = require('../../controllers/rabController')
const { superadmin, updl } = require('../../middlewares/authentication')

router.get('/api/rab/:id', RabController.findOne)
router.get('/api/rab', RabController.findAll)
router.get('/api/rab/angkatan/:angkatan/:tahapan', RabController.findByAngkatanAndTahapan)
router.get('/api/rab/angkatan/:angkatan/', RabController.findByAngkatan)
router.post('/api/rab/add', updl, RabController.create)
router.put('/api/rab/:id', superadmin, RabController.update)
router.delete('/api/rab/:id', superadmin, RabController.delete)

module.exports = router