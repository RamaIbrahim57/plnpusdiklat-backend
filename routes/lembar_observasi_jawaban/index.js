const express = require('express');
const router = express.Router();
const LembarObservasiJawabanController = require('../../controllers/lembarObservasiJawabanController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/observasiJawaban/:id', LembarObservasiJawabanController.findOne)
router.get('/api/observasiJawaban/mentor/siswa/:mentor/:siswa', LembarObservasiJawabanController.findByMentorAndSiswa)
router.get('/api/observasiJawaban', LembarObservasiJawabanController.findAll)
router.post('/api/observasiJawaban/add', mentor, LembarObservasiJawabanController.create)
router.put('/api/observasiJawaban/:id', mentor, LembarObservasiJawabanController.update)
router.delete('/api/observasiJawaban/:id', mentor, LembarObservasiJawabanController.delete)

module.exports = router