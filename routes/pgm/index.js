const express = require('express');
const router = express.Router();
const PgmController = require('../../controllers/pgmController')
const { siswa } = require('../../middlewares/authentication')
const { superadmin, superadminandcoop } = require('../../middlewares/authentication')

router.get('/api/pgm/:id', PgmController.findOne)
router.get('/api/pgm', PgmController.findAll)
router.post('/api/pgm/add', siswa, PgmController.create)
router.put('/api/pgm/:id', superadminandcoop, PgmController.update)
router.delete('/api/pgm/:id', superadminandcoop, PgmController.delete)

module.exports = router