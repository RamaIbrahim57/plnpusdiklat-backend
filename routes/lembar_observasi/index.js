const express = require('express');
const router = express.Router();
const LembarObservasiController = require('../../controllers/lembarObservasiController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/observasi/:id', LembarObservasiController.findOne)
router.get('/api/observasi', LembarObservasiController.findAll)
router.post('/api/observasi/add', superadmin, LembarObservasiController.create)
router.put('/api/observasi/:id', superadmin, LembarObservasiController.update)
router.delete('/api/observasi/:id', superadmin, LembarObservasiController.delete)

module.exports = router