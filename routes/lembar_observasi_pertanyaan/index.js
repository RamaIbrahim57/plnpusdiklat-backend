const express = require('express');
const router = express.Router();
const LembarObservasiPertanyaanController = require('../../controllers/lembarObservasiPertanyaanController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/observasiPertanyaan/:id', LembarObservasiPertanyaanController.findOne)
router.get('/api/observasiPertanyaan/aspek/:id', LembarObservasiPertanyaanController.findByAspek)
router.get('/api/observasiPertanyaan', LembarObservasiPertanyaanController.findAll)
router.post('/api/observasiPertanyaan/add', superadmin, LembarObservasiPertanyaanController.create)
router.put('/api/observasiPertanyaan/:id', superadmin, LembarObservasiPertanyaanController.update)
router.delete('/api/observasiPertanyaan/:id', superadmin, LembarObservasiPertanyaanController.delete)

module.exports = router