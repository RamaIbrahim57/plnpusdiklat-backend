const express = require('express');
const router = express.Router();
const LembarObservasiEvidenController = require('../../controllers/lembarObservasiEvidenController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/observasiEviden/:id', LembarObservasiEvidenController.findOne)
router.get('/api/observasiEviden', LembarObservasiEvidenController.findAll)
router.get('/api/observasiEviden/:mentor/:siswa', LembarObservasiEvidenController.findByMentorAndSiswa)
router.post('/api/observasiEviden/add', mentor, LembarObservasiEvidenController.create)
router.put('/api/observasiEviden/:id', mentor, LembarObservasiEvidenController.update)
router.delete('/api/observasiEviden/:id', mentor, LembarObservasiEvidenController.delete)

module.exports = router