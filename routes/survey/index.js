const express = require('express');
const router = express.Router();
const SurveyController = require('../../controllers/surveyController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/survey/:id', SurveyController.findOne)
router.get('/api/survey', SurveyController.findAll)
router.get('/api/survey/jabatan/:id', SurveyController.findByJabatan)
router.post('/api/survey/add', superadmin, SurveyController.create)
router.put('/api/survey/:id', superadmin, SurveyController.update)
router.delete('/api/survey/:id', superadmin, SurveyController.delete)
router.delete('/api/survey/jabatan/:id', superadmin, SurveyController.deleteByJabatan)

module.exports = router