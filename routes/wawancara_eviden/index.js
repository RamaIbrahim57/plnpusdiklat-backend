const express = require('express');
const router = express.Router();
const WawancaraEvidenController = require('../../controllers/wawancaraEvidenController')
const { siswa, psikolog, superadmin } = require('../../middlewares/authentication')

router.get('/api/wawancaraEviden/:id', WawancaraEvidenController.findOne)
router.get('/api/wawancaraEviden/psikolog/:id', WawancaraEvidenController.findByPsikolog)
router.get('/api/wawancaraEviden', WawancaraEvidenController.findAll)
router.post('/api/wawancaraEviden/add', psikolog, WawancaraEvidenController.create)
router.put('/api/wawancaraEviden/:id', psikolog, WawancaraEvidenController.update)
router.delete('/api/wawancaraEviden/:id', psikolog, WawancaraEvidenController.delete)

module.exports = router