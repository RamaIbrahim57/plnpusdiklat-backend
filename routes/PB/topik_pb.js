const express = require('express');
const router = express.Router();
const TopikPbController = require('../../controllers/topikPbController')
const { superadmin, siswa } = require('../../middlewares/authentication')

router.get('/api/topik_pb/:id', TopikPbController.findOne)
router.put('/api/topik_pb/:id/edit', superadmin, TopikPbController.update)
router.get('/api/topik_pb/angkatan/:id', TopikPbController.findByAngkatan)
router.get('/api/topik_pb/updl/:id/:angkatan', TopikPbController.findByUpdl)
router.get('/api/topik_pb/bidang/:id', TopikPbController.findByBidang)
router.get('/api/topik_pb/angkatan/bidang/:angkatan/:bidang', TopikPbController.findByAngkatanAndBidang)
router.post('/api/topik_pb/add', superadmin, TopikPbController.create)
router.delete('/api/topik_pb/:id', superadmin, TopikPbController.delete)

module.exports = router