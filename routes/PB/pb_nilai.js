const express = require('express');
const router = express.Router();
const PbController = require('../../controllers/pbController')
const { superadmin, siswa } = require('../../middlewares/authentication')

router.get('/api/pb_nilai/:id', PbController.findOne)
router.get('/api/pb_nilai', PbController.findAll)
router.get('/api/pb_nilai/topik/:topik/:siswa', PbController.findByTopikAndSiswa)
router.get('/api/pb_nilai/topik/:topik', PbController.findByTopikPb)
router.put('/api/pb_nilai/:id/edit', siswa, PbController.update)
router.put('/api/pb_nilai/nilai/:id/edit', superadmin, PbController.updateNilai)
router.post('/api/pb_nilai/add', siswa, PbController.create)
router.delete('/api/pb_nilai/:id', siswa, PbController.delete)

module.exports = router