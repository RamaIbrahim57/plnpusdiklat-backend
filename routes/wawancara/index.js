const express = require('express');
const router = express.Router();
const WawancaraController = require('../../controllers/wawancaraController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/wawancara/:id', WawancaraController.findOne)
router.get('/api/wawancara', WawancaraController.findAll)
router.post('/api/wawancara/add', superadmin, WawancaraController.create)
router.put('/api/wawancara/:id', superadmin, WawancaraController.update)
router.delete('/api/wawancara/:id', superadmin, WawancaraController.delete)

module.exports = router