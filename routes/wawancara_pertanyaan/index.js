const express = require('express');
const router = express.Router();
const WawancaraPertanyaanController = require('../../controllers/wawancaraPertanyaanController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/wawancaraPertanyaan/:id', WawancaraPertanyaanController.findOne)
router.get('/api/wawancaraPertanyaan', WawancaraPertanyaanController.findAll)
router.get('/api/wawancaraPertanyaan/indikator/:id', WawancaraPertanyaanController.findByIndikator)
router.post('/api/wawancaraPertanyaan/add', superadmin, WawancaraPertanyaanController.create)
router.put('/api/wawancaraPertanyaan/:id', superadmin, WawancaraPertanyaanController.update)
router.delete('/api/wawancaraPertanyaan/:id', superadmin, WawancaraPertanyaanController.delete)

module.exports = router