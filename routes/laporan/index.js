const express = require('express');
const router = express.Router();
const laporanHarianController = require('../../controllers/laporanHarianController')
const { siswa, mentor } = require('../../middlewares/authentication')

router.get('/api/laporan/:id', laporanHarianController.findOne)
router.get('/api/laporan', laporanHarianController.findAll)
router.get('/api/laporan/mentor/:id', laporanHarianController.findByMentor)
router.get('/api/laporan/siswa/:id', laporanHarianController.findBySiswa)
router.post('/api/laporan/add', siswa, laporanHarianController.create)
router.put('/api/laporan/:id', siswa, laporanHarianController.update)
router.put('/api/laporan/status/:id', mentor, laporanHarianController.updateStatus)
router.put('/api/laporan/revisi/:id', mentor, laporanHarianController.updateStatusRevisi)
router.delete('/api/laporan/:id', siswa, laporanHarianController.delete)

module.exports = router