const express = require('express');
const router = express.Router();
const AngkatanController = require('../../controllers/angkatanController')
const { superadmin, updl, superadminandcoop } = require('../../middlewares/authentication')

router.get('/api/angkatan/:id', AngkatanController.findOne)
router.get('/api/angkatan-aktif/:program', AngkatanController.findActive)
router.get('/api/angkatan/', AngkatanController.findAll)
router.get('/api/angkatan/program/:program', AngkatanController.findByProgram)
router.post('/api/angkatan/add', superadminandcoop, AngkatanController.create)
router.put('/api/angkatan/:id', superadminandcoop, AngkatanController.update)
router.put('/api/angkatan/ujisoft/:id', updl, AngkatanController.updateUjisoftStatus)
router.put('/api/angkatan/aktif/:id/:program', superadminandcoop, AngkatanController.setAsActive)
router.delete('/api/angkatan/:id', superadminandcoop, AngkatanController.delete)

module.exports = router