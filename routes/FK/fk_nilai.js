const express = require('express');
const router = express.Router();
const FkController = require('../../controllers/fkController')
const { siswa, superadmin } = require('../../middlewares/authentication')

router.get('/api/fk_nilai/:id', FkController.findOne)
router.get('/api/fk_nilai/topik/:topik/:siswa', FkController.findByTopikFkAndSiswa)
router.get('/api/fk_nilai/topik/:topik', FkController.findByTopikFk)
router.get('/api/fk_nilai', FkController.findAll)
router.put('/api/fk_nilai/nilai/:id/edit', superadmin, FkController.updateNilai)
router.put('/api/fk_nilai/:id/edit', siswa, FkController.update)
// router.post('/fk_nilai/angkatan/:id', FkController.findByAngkatan)
router.post('/api/fk_nilai/add', siswa, FkController.create)
router.delete('/api/fk_nilai/:id', siswa, FkController.delete)

module.exports = router