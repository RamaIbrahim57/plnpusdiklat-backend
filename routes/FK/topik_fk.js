const express = require('express');
const router = express.Router();
const TopikFkController = require('../../controllers/topikFkController')
const { siswa, superadmin } = require('../../middlewares/authentication')

router.get('/api/topik_fk/:id', TopikFkController.findOne)
router.put('/api/topik_fk/:id/edit', superadmin, TopikFkController.update)
router.get('/api/topik_fk/angkatan/:id', TopikFkController.findByAngkatan)
router.post('/api/topik_fk/add', superadmin, TopikFkController.create)
router.delete('/api/topik_fk/:id', superadmin, TopikFkController.delete)

module.exports = router