const express = require('express');
const router = express.Router();
const UjiSoftController = require('../../controllers/ujisoftController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.get('/api/ujisoft/:id', UjiSoftController.findOne)
router.get('/api/ujisoft', UjiSoftController.findAll)
router.get('/api/ujisoft/bidang/:id', UjiSoftController.findByBidang)
router.post('/api/ujisoft/add', superadmin, UjiSoftController.create)
router.put('/api/ujisoft/:id', superadmin, UjiSoftController.update)
router.delete('/api/ujisoft/:id', superadmin, UjiSoftController.delete)

module.exports = router