const express = require('express');
const router = express.Router();
const RajaOngkirController = require('../../controllers/rajaOngkirController')

router.get('/api/city/provinces/:id', RajaOngkirController.getCityByProvinces)
router.get('/api/provinces', RajaOngkirController.getAllProvinces)

module.exports = router