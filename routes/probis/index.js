const express = require('express');
const router = express.Router();
const ProbisController = require('../../controllers/probisController')
const { mentor, siswa } = require('../../middlewares/authentication')

router.get('/api/probis/:id', ProbisController.findOne)
router.get('/api/probis', ProbisController.findAll)
router.post('/api/probis/add', siswa, ProbisController.create)
router.put('/api/probis/:id', siswa, ProbisController.update)
router.put('/api/probis/status/:id', mentor, ProbisController.updateNilai)
router.get('/api/probis/siswa/:id', ProbisController.findBySiswa)
router.get('/api/probis/siswa/hari/:siswa/:hari', ProbisController.findByHariAndSiswa)
router.get('/api/probis/mentor/:id', ProbisController.findByMentor)
router.delete('/api/probis/:id', siswa, ProbisController.delete)

module.exports = router