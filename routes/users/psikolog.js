const express = require('express');
const router = express.Router();
const UserController = require('../../controllers/userController')
const { psikolog } = require('../../middlewares/authentication')

router.post('/api/psikolog/signup', UserController.signUpPsikolog)
router.post('/api/psikolog/signin', UserController.signInPsikolog)
router.post('/api/psikolog/updatePassword/:id', psikolog, UserController.updatePasswordPsikolog)
router.post('/api/psikolog/requestPassword/', UserController.resetPassRequestPsikolog)
router.get('/api/psikolog/resetPassword/:id', UserController.resetPassVerifyPsikolog)

module.exports = router