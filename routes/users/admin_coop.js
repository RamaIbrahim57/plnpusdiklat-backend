const express = require('express');
const router = express.Router();
const UserController = require('../../controllers/userController')
const { superadmin, superadminandcoop } = require('../../middlewares/authentication')

router.post('/api/admincoop/signup', UserController.signUpAdminCoop)
router.post('/api/admincoop/signin', UserController.signInAdminCoop)
router.post('/api/admincoop/updatePassword/:id', superadminandcoop, UserController.updatePasswordAdminCoop)

module.exports = router