const express = require('express');
const router = express.Router();
const UserController = require('../../controllers/userController')
const { siswa, mentor, superadmin, updl, penguji, superadminandcoopandtalenta } = require('../../middlewares/authentication')

router.post('/api/updl/signup', UserController.signUpUpdl)
router.post('/api/updl/signin', UserController.signInUpdl)

router.get('/api/updl', UserController.findAllUpdl)
router.get('/api/updl/:id', UserController.findOneUpdl)
router.get('/api/updl/name/:id', UserController.findUpdlByName)
router.post('/api/updl/updatePassword/:id', updl, UserController.updatePasswordUpdl)
router.post('/api/updl/requestPassword/', updl, UserController.resetPassRequestUpdl)
router.get('/api/updl/resetPassword/:id', UserController.resetPassVerifyUpdl)
router.get('/api/updl/resetPassword/:id', UserController.resetPassVerifyUpdl)

router.put('/api/updl/:id', superadminandcoopandtalenta, UserController.editUpdl)

router.delete('/api/updl/:id', superadminandcoopandtalenta, UserController.deleteUpdl)

module.exports = router