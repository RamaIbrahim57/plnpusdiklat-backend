const express = require('express');
const router = express.Router();
const KompetensiSiswaController = require('../../controllers/kompetensiSiswaController');
const { siswa, superadmin } = require('../../middlewares/authentication')

router.get('/api/kompetensiSiswa/:id', KompetensiSiswaController.findOne)
router.get('/api/kompetensiSiswa', KompetensiSiswaController.findAll)
router.get('/api/kompetensiSiswa/siswa/:id', KompetensiSiswaController.findBySiswa)
router.post('/api/kompetensiSiswa/add', siswa, KompetensiSiswaController.create)
router.put('/api/kompetensiSiswa/:id', siswa, KompetensiSiswaController.update)
router.delete('/api/kompetensiSiswa/:id', siswa, KompetensiSiswaController.delete)
router.delete('/api/kompetensiSiswa/siswa/:id', superadmin, KompetensiSiswaController.deleteBySiswa)

module.exports = router