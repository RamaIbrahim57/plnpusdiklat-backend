const express = require('express');
const router = express.Router();
const UserController = require('../../controllers/userController')
const { siswa, mentor, superadmin, updl, penguji } = require('../../middlewares/authentication')

router.post('/api/penguji/signup', UserController.signUpPenguji)
router.post('/api/penguji/signin', UserController.signInPenguji)
router.get('/api/penguji', UserController.findAllPenguji)
router.get('/api/penguji/:id', UserController.findOnePenguji)
router.delete('/api/penguji/:id', updl, UserController.deletePenguji)
router.put('/api/penguji/:id', UserController.editPenguji)
router.post('/api/penguji/updatePassword/:id', penguji, UserController.updatePasswordPenguji)
router.post('/api/penguji/editPassword/:id', UserController.editPasswordPenguji)
router.post('/api/penguji/requestPassword/', UserController.resetPassRequestPenguji)
router.get('/api/penguji/resetPassword/:id', UserController.resetPassVerifyPenguji)

// query
module.exports = router