const express = require('express');
const router = express.Router();
const MentorController = require('../../controllers/mentorController')
const { superadmin } = require('../../middlewares/authentication')

router.get('/api/mentor1/:id', MentorController.findOne)
router.get('/api/mentor1', MentorController.findAll)
router.get('/api/mentor1/program/:program', MentorController.findByProgram)
router.post('/api/mentor1/add', superadmin, MentorController.create)
router.put('/api/mentor1/:id', superadmin, MentorController.update)
router.delete('/api/mentor1/:id', superadmin, MentorController.delete)

module.exports = router