const express = require('express');
const router = express.Router();
const UserController = require('../../controllers/userController')
const { siswa, mentor, superadmin } = require('../../middlewares/authentication')

router.post('/api/mentor/signup', UserController.signUpMentor)
router.post('/api/mentor/signin', UserController.signInMentor)
router.get('/api/mentor', UserController.findAllMentor)
router.get('/api/mentor/program/:program', UserController.findMentorByProgram)
router.get('/api/mentor/:id', UserController.findOneMentor)
router.delete('/api/mentor/:id', superadmin, UserController.deleteMentor)
router.post('/api/mentor/requestPassword/', UserController.resetPassRequestMentor)
router.get('/api/mentor/resetPassword/:id', UserController.resetPassVerifyMentor)
router.post('/api/mentor/updatePassword/:id', mentor, UserController.updatePasswordMentor)
router.post('/api/mentor/editPassword/:id', UserController.editPasswordMentor)
router.put('/api/mentor/:id', superadmin, UserController.editMentor)

module.exports = router