const express = require('express');
const router = express.Router();
const UserController = require('../../controllers/userController')
const { siswa, mentor, superadmin, updl, superadminandcoopandtalenta } = require('../../middlewares/authentication')

router.post('/api/talenta/signup', UserController.signUpTalenta)
router.post('/api/talenta/signin', UserController.signInTalenta)
router.post('/api/talenta/updatePassword/:id', superadminandcoopandtalenta, UserController.updatePasswordTalenta)

module.exports = router