const express = require('express');
const router = express.Router();
const UserController = require('../../controllers/userController')
const { superadmin } = require('../../middlewares/authentication')

router.post('/api/adminpt/signup', UserController.signUpAdminPt)
router.post('/api/adminpt/signin', UserController.signInAdminPt)

module.exports = router