const express = require('express');
const router = express.Router();
const UserController = require('../../controllers/userController')
const { superadmin } = require('../../middlewares/authentication')

router.post('/api/superadmin/signup', UserController.signUpAdmin)
router.post('/api/superadmin/signin', UserController.signInAdmin)
router.get('/api/superadmin', UserController.findAllAdmin)
router.get('/api/superadmin/:id', UserController.findOneAdmin)
router.post('/api/superadmin/updatePassword/:id', superadmin, UserController.updatePasswordAdmin)
router.delete('/api/superadmin/:id', superadmin, UserController.deleteAdmin)

module.exports = router