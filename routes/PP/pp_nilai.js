const express = require('express');
const router = express.Router();
const PpController = require('../../controllers/ppController')
const { superadmin, siswa } = require('../../middlewares/authentication')

router.get('/api/pp_nilai/:id', PpController.findOne)
router.get('/api/pp_nilai/topik/:topik/:siswa', PpController.findByTopikAndSiswa)
router.get('/api/pp_nilai/topik/:topik', PpController.findByTopikPp)
router.get('/api/pp_nilai', PpController.findAll)
router.put('/api/pp_nilai/:id/edit', siswa, PpController.update)
router.put('/api/pp_nilai/nilai/:id/edit', superadmin, PpController.updateNilai)
router.post('/api/pp_nilai/add', siswa, PpController.create)
router.delete('/api/pp_nilai/:id', siswa, PpController.delete)

module.exports = router