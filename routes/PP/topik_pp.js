const express = require('express');
const router = express.Router();
const TopikPpController = require('../../controllers/topikPpController')
const { superadmin, siswa } = require('../../middlewares/authentication')

router.get('/api/topik_pp/:id', TopikPpController.findOne)
router.put('/api/topik_pp/:id/edit', superadmin, TopikPpController.update)
router.get('/api/topik_pp/angkatan/:id', TopikPpController.findByAngkatan)
router.post('/api/topik_pp/add', superadmin, TopikPpController.create)
router.delete('/api/topik_pp/:id', superadmin, TopikPpController.delete)

module.exports = router