const express = require('express');
const router = express.Router();
const fileController = require('../../controllers/fileController')
const upload = require('../../middlewares/multer')

router.post('/api/uploadfile', upload.single('myFile'), fileController.upload)
router.post('/api/uploadfilepdf', upload.single('myFile'), fileController.uploadPdf)
router.post('/api/compressPdf', upload.single('myFile'), fileController.compressPdf)
router.post('/api/uploadfileppt', upload.single('myFile'), fileController.uploadPpt)
router.get('/api/downloadfile/:id', upload.single('myFile'), fileController.download)
router.get('/api/downloadKuk', upload.single('myFile'), fileController.downloadTemplateKuk)
router.get('/api/downloadMasterSiswa', upload.single('myFile'), fileController.downloadTemplateMasterDataSiswa)
router.get('/api/downloadNilaiSiswa', upload.single('myFile'), fileController.downloadTemplateNilaiSiswa)
router.get('/api/downloadUpdl', upload.single('myFile'), fileController.downloadTemplateUpdl)
router.get('/api/downloadRefKUK', upload.single('myFile'), fileController.downloadReferensiKUK)
router.get('/api/downloadTemplateOkupasi', upload.single('myFile'), fileController.downloadTemplateOkupasi)
router.get('/api/downloadTemplateSurvey', upload.single('myFile'), fileController.downloadTemplateSurvey)
router.get('/api/downloadTemplateCreateSurvey', upload.single('myFile'), fileController.downloadTemplateCreateSurvey)
router.get('/api/downloadLaporan', upload.single('myFile'), fileController.downloadTemplateLaporan)
router.post('/api/getCompressed', upload.single('myFile'), fileController.compressToZip)
router.get('/api/downloadByPath/:filepath', upload.single('myFile'), fileController.downloadByPath)
router.delete('/api/deleteFile/:id', upload.single('myFile'), fileController.deleteFile)

module.exports = router