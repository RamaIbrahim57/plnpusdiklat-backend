const express = require('express');
const router = express.Router();
const PasswordController = require('../../controllers/passwordController')

router.post('/api/password/create', PasswordController.create)

module.exports = router