const { verifyToken } = require('../helpers/jwt');
const Superadmin = require('../models/superadmin');
const Talenta = require('../models/talenta');
const AdminCoop = require('../models/admincoop');
const Mentor = require('../models/mentor2');
const Siswa = require('../models/siswa');
const Updl = require('../models/updl');
const Penguji = require('../models/penguji');
const Psikolog = require('../models/psikolog');

async function superadmin(req, res, next){
  try {
    let { accesstoken } = req.headers;
    if(accesstoken){
        let decoded = verifyToken(accesstoken);
        console.log(decoded, 'decoded')
        let user = await Superadmin.findOne({ _id: decoded.id });
        if(!user){
            throw {message: 'Authentication failed', status: 401};
        } else {
            req.userLoggedIn = decoded;
            next();
        }
    } else {
        throw {message: 'Authentication failed', status: 401};
    }
  } catch (err) {
      next(err);
  }
}

async function superadminandcoop(req, res, next){
    try {
      let { accesstoken } = req.headers;
      if(accesstoken){
          let decoded = verifyToken(accesstoken);
          console.log(decoded, 'decoded')
          let user = await Superadmin.findOne({ _id: decoded.id });
          if (!user){
              user = await AdminCoop.findOne({ _id: decoded.id })
              if (!user) throw {message: 'Authentication failed', status: 401};
              else {
                req.userLoggedIn = decoded;
                next();
              }
          } else {
              req.userLoggedIn = decoded;
              next();
          }
      } else {
          throw {message: 'Authentication failed', status: 401};
      }
    } catch (err) {
        next(err);
    }
}

  async function superadminandcoopandtalenta(req, res, next){
    try {
      let { accesstoken } = req.headers;
      if(accesstoken){
          let decoded = verifyToken(accesstoken);
          console.log(decoded, 'decoded')
          let user = await Superadmin.findOne({ _id: decoded.id });
          if (!user) {
              user = await AdminCoop.findOne({ _id: decoded.id })
              if (!user) {
                  user = await Talenta.findOne({ _id: decoded.id })
                  if (!user) throw { status: 400, message: 'Authentication failed' }
                  else {
                    req.userLoggedIn = decoded;
                    next();
                  }
                } else {
                    req.userLoggedIn = decoded;
                    next();
                }
          } else {
              req.userLoggedIn = decoded;
              next();
          }
      } else {
          throw {message: 'Authentication failed', status: 401};
      }
    } catch (err) {
        next(err);
    }
}

async function mentor(req, res, next){
    try {
      let { accesstoken } = req.headers;
      if(accesstoken){
          let decoded = verifyToken(accesstoken);
          console.log(decoded, 'decoded')
          let user = await Mentor.findOne({ _id: decoded.id });
          if (!user){
              throw {message: 'Authentication failed', status: 401};
          } else {
              req.userLoggedIn = decoded;
              next();
          }
      } else {
          throw {message: 'Authentication failed', status: 401};
      }
    } catch (err) {
        next(err);
    }
}

async function siswa(req, res, next){
    try {
      let { accesstoken } = req.headers;
      console.log(accesstoken)
      if(accesstoken){
          let decoded = verifyToken(accesstoken);
          console.log(decoded, 'decoded')
          let user = await Siswa.findOne({ _id: decoded.id });
          if(!user){
              throw {message: 'Authentication failed', status: 401};
          } else {
              req.userLoggedIn = decoded;
              next();
          }
      } else {
          throw {message: 'Authentication failed', status: 401};
      }
    } catch (err) {
        next(err);
    }
}

async function mentorandsiswa (req, res, next){
    try {
      let { accesstoken } = req.headers;
      if(accesstoken){
          let decoded = verifyToken(accesstoken);
          console.log(decoded, 'decoded')
          let user = await Siswa.findOne({ _id: decoded.id });
          if (!user) {
            user = await Mentor.findOne({ _id: decoded.id })
            if (!user) throw {message: 'Authentication failed', status: 401};
            else {
              req.userLoggedIn = decoded;
              next();
            }
          } else {
              req.userLoggedIn = decoded;
              next();
          }
      } else {
          throw {message: 'Authentication failed', status: 401};
      }
    } catch (err) {
        next(err);
    }
}

async function updl(req, res, next){
    try {
      let { accesstoken } = req.headers;
      console.log(accesstoken)
      if(accesstoken){
          let decoded = verifyToken(accesstoken);
          console.log(decoded, 'decoded')
          let user = await Updl.findOne({ _id: decoded.id });
          if(!user){
              throw {message: 'Authentication failed', status: 401};
          } else {
              req.userLoggedIn = decoded;
              next();
          }
      } else {
          throw {message: 'Authentication failed', status: 401};
      }
    } catch (err) {
        next(err);
    }
}

async function penguji(req, res, next){
    try {
      let { accesstoken } = req.headers;
      console.log(accesstoken)
      if(accesstoken){
          let decoded = verifyToken(accesstoken);
          console.log(decoded, 'decoded')
          let user = await Penguji.findOne({ _id: decoded.id });
          if(!user){
              throw {message: 'Authentication failed', status: 401};
          } else {
              req.userLoggedIn = decoded;
              next();
          }
      } else {
          throw {message: 'Authentication failed', status: 401};
      }
    } catch (err) {
        next(err);
    }
}

async function psikolog(req, res, next){
    try {
      let { accesstoken } = req.headers;
      console.log(accesstoken)
      if(accesstoken){
          let decoded = verifyToken(accesstoken);
          console.log(decoded, 'decoded')
          let user = await Psikolog.findOne({ _id: decoded.id });
          if(!user){
              throw {message: 'Authentication failed', status: 401};
          } else {
              req.userLoggedIn = decoded;
              next();
          }
      } else {
          throw {message: 'Authentication failed', status: 401};
      }
    } catch (err) {
        next(err);
    }
}

module.exports = { mentorandsiswa, superadmin, mentor, siswa, updl, penguji, superadminandcoop, superadminandcoopandtalenta, psikolog };