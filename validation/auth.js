const Joi = require('joi')
const { isLoggedIn } = require('../auth/siswa')
const nik = Joi.string().min(5).max(254).lowercase().required();
const password = Joi.string().min(8).max(72, 'utf-8').required();
const notest = Joi.string().required();
const asal_rekrutmen = Joi.string().required();
const nama = Joi.string().required();
const jenjang = Joi.string().required();
const email = Joi.string().required();
const univ = Joi.string().required();
const hp1 = Joi.string().required();
const gender = Joi.string().required();
const address = Joi.string().required();

// Schema Siswa
const registerSiswa = Joi.object({
  nik,
  notest,
  asal_rekrutmen,
  nama,
  jenjang,
  email,
  univ,
  hp1,
  gender,
  address,
  angkatan_id: Joi.required(),
  program: Joi.string().required(),
  agama: Joi.string().required(),
  fase: Joi.string().required(),
})

// Schema Password
const registerPassword = Joi.object({
  password,
})

const loginSiswa = Joi.object({
  nik,
  password,
})

const validate = async (schema, payload) => {
  try {
    await schema.validateAsync(payload, { abortEarly: false })
  } catch (e) {
    throw (e)
  }
}

const guest = (req, res, next) => {
  if (isLoggedIn(req)) {
    return next(new Error('You are already logged in'))
  }
  next()
}

module.exports = { registerSiswa, guest, loginSiswa, validate, registerPassword }