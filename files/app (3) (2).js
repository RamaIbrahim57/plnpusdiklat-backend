require("dotenv").config();
const express = require("express");
const helmet = require("helmet");
const cors = require("cors");
const router = require("./routes");
const https = require("https");
const db = require("./models");
const bodyParser = require("body-parser");
const fs = require("fs");
const path = require("path");
const compression = require("compression");
const errorHandler = require("./middlewares/errorHandler");
const app = express();
const port = process.env.PORT || 3000;

global.__basedir = __dirname;

db.sequelize.sync();
app.use(helmet());
app.use(cors());
app.use(compression()); //Compress all routes
app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/api/", router);
app.use(errorHandler);
if (process.env.NODE_ENV == "production") {
  // We are running in production mode
  https.createServer({
    key: fs.readFileSync('server_edutech.key'),
    cert: fs.readFileSync('server_edutech.cert')
  }, app).listen(port, function () {
    console.log(`App running on ${process.env.NODE_ENV} PORT : ${port}! Go to https://localhost:${port}/`)
  })
}else {
  // We are running in development mode
  https.createServer({
    key: fs.readFileSync('server_edutech.key'),
    cert: fs.readFileSync('server_edutech.cert')
  }, app).listen(port, function () {
  // app.listen(port, function () {
    console.log(`App running on ${process.env.NODE_ENV} PORT : ${port}! Go to https://localhost:${port}/`)
  })
}

module.exports = app;