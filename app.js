const express = require('express');
const Redis = require('ioredis');
const router = require('./routes');
const session = require('express-session');
const connectRedis = require('connect-redis');
const REDIS_OPTIONS = require('./config/index').cache
const SESSION_OPTIONS = require('./config/index').session
const ErrorHandler = require('./middlewares/errors');
const compression = require('compression')
const cors = require('cors');
const https = require("https");
const fs = require("fs");
const helmet = require('helmet')

const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);
const app = express();
app.use(helmet())
app.use(compression())

// console.log = function() {}


// Redis Session

const RedisStore = connectRedis(session)
const client = new Redis(REDIS_OPTIONS)

app.use(
  session({
    store: new RedisStore({ client }),
    ...SESSION_OPTIONS,
  })
  )
  
  //db
  mongoose.connect('mongodb://localhost/plnpusdiklat', { useNewUrlParser: true, useUnifiedTopology: true });
  
  // Middlewares
  app.use(express.json());
  app.use(express.urlencoded({extended: true}))
  
  // Routes
app.use(cors())
app.use(router)
app.use(ErrorHandler)

console.log(process.env.NODE_ENV)
app.listen(3000, () => {
  console.log("server started 3000")
})
// module.exports = app
