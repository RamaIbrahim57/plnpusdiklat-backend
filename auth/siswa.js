function signIn (req, userId) {
    req.session.userId = userId
    req.session.createdAt = Date.now()
}
const isLoggedIn = (req) => !!req.session.userId


module.exports = { signIn, isLoggedIn }