const {
    SESSION_SECRET = 'R4ma Ganteng',
    SESSION_NAME = 'PLNAuth',
    SESSION_IDLE_TIMEOUT = 100 * 60 * 30
} = process.env

const SESSION_OPTIONS = {
    secret: SESSION_SECRET,
    name: SESSION_NAME,
    cookie: {
        maxAge: +SESSION_IDLE_TIMEOUT,
        secure: false, // Ganti True di Production
        sameSite: true,
    },
    rolling: true,
    resave: false,
    saveUninitialized: false,
}

module.exports = SESSION_OPTIONS