const RedisOptions = require('ioredis').RedisOptions
const {
    REDIS_PORT = 6379,
    REDIS_HOST = 'localhost',
} = process.env

const REDIS_OPTIONS = {
    port: +REDIS_PORT,
    host: REDIS_HOST,
}

module.exports = REDIS_OPTIONS