const multer = require('multer');
const { exec } = require("child_process");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, "public/uploads");
    },
    filename: function (req, file, cb) {
      cb(
        null,
        file.fieldname + "-" + Date.now() + path.extname(file.originalname)
      )
    },
})

const pdfFilter = function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if (ext !== ".pdf") {
      return callback("This Extension is not supported");
    }
    callback(null, true);
};

const compresspdfupload = multer({
    storage: storage,
    fileFilter: pdfFilter,
  }).single("file");

module.exports = { compresspdfupload }