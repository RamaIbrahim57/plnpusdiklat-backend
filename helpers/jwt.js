  
const jwt = require('jsonwebtoken');

function hashToken(payload){
    let token = jwt.sign(payload, 'secret');
    return token;
}

function verifyToken(token){
    let decoded = jwt.verify(token, 'secret');
    return decoded
}

function sign (userData) {
    return jwt.sign({ id: userData.id }, 'secret', {
        expiresIn: 1 * 24 * 60 * 60 * 1000,
    })
}

module.exports = {hashToken, verifyToken, sign};