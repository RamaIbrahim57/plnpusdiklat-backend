const archiver = require('archiver');

var archive = archiver('zip', {
    gzip: true,
    zlib: { level: 9 } // Sets the compression level.
});

archive.on('error', function(err) {
    throw err;
});

// pipe archive data to the output file

module.exports = { archive }